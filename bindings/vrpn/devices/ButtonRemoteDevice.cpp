#include "../../../libs/vrpn/vrpn_Button.h"
#include "BaseVRPNDeviceBinding.cpp"
#include "../CallbackList.cpp"

class ButtonChangeData {
public:
	vrpn_int32 getButtonIndex () {
		return _buttonIndex;
	}

	vrpn_int32 getState () {
		return _state;
	}

	void setData (vrpn_BUTTONCB data) {
		_buttonIndex = data.button;
		_state = data.state;
	}

private:
	vrpn_int32 _buttonIndex;
	vrpn_int32 _state;
};

class ButtonRemoteDevice : public BaseVRPNDeviceBinding<vrpn_Button_Remote> {
public:
	ButtonRemoteDevice (std::string deviceName, std::string address)
		: BaseVRPNDeviceBinding(deviceName, address) {
		initStates();
		initChange();
	}

	virtual ~ButtonRemoteDevice () {
		LIV_EVENT_RELEASE(this, States);
		LIV_EVENT_RELEASE(this, Change);
	}

	void requestUpdate () {
		if (isUp()) {
			getDevice()->mainloop();	
		}
	}

	void disconnect () {
		LIV_EVENT_CANCEL(this, States);
		LIV_EVENT_CANCEL(this, Change);
		releaseDevice();
	}

	LIV_EVENT(States, std::vector<vrpn_int32>)
	LIV_EVENT(Change, ButtonChangeData*)

protected:
	vrpn_Button_Remote * makeDevice () {
		return new vrpn_Button_Remote(getVRPNDeviceName().c_str());
	}

private:
	vrpn_BUTTONSTATESHANDLER _statesHandler;
	void initStates () {
		_statesHandler = [] (void *userData, const vrpn_BUTTONSTATESCB info) {
			auto $this = (ButtonRemoteDevice *)userData;
			std::vector<vrpn_int32> data(
				info.states, info.states + info.num_buttons
			);
			LIV_EVENT_INVOKE($this, States, data);
		};

		LIV_EVENT_INIT(
			this, States, std::vector<vrpn_int32>,
			[this] () {
				return
					getDevice()->register_states_handler(this, _statesHandler);
			},
			[this] () {
				return
					getDevice()->unregister_states_handler(this, _statesHandler);
			}
		);
	}

	ButtonChangeData _changeData;
	vrpn_BUTTONCHANGEHANDLER _changeHandler;
	void initChange () {
		_changeHandler = [] (void *userData, const vrpn_BUTTONCB info) {
			auto $this = (ButtonRemoteDevice *)userData;
			$this->_changeData.setData(info);
			LIV_EVENT_INVOKE($this, Change, &($this->_changeData));
		};

		LIV_EVENT_INIT(
			this, Change, ButtonChangeData*,
			[this] () {
				return
					getDevice()->register_change_handler(this, _changeHandler);
			},
			[this] () {
				return
					getDevice()->unregister_change_handler(this, _changeHandler);
			}
		);
	}
};

NBIND_CLASS(ButtonChangeData) {
	NBIND_METHOD(getState);
	NBIND_METHOD(getButtonIndex);
}

NBIND_CLASS(ButtonRemoteDevice) {
	NBIND_INHERIT(IVRPNDeviceBinding);
	NBIND_CONSTRUCT<std::string, std::string>();
	LIV_EVENT_BIND(States);
	LIV_EVENT_BIND(Change);
}

LIV_CALLBACKLIST_EXPORT_TYPE(ButtonStates, std::vector<vrpn_int32>);
LIV_CALLBACKLIST_EXPORT_TYPE(ButtonChange, ButtonChangeData*);
