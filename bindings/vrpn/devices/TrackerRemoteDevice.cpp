#include "../../../libs/vrpn/vrpn_Tracker.h"
#include "BaseVRPNDeviceBinding.cpp"
#include "../CallbackList.cpp"

#include <iostream>

class TrackerBaseChangeData {
public:
	vrpn_int32 getSensor () {
		return _sensor;
	}
	std::vector<vrpn_float64> getQuaternion () {
		return _quaternion;
	}
	std::vector<vrpn_float64> getVector3 () {
		return _vector3;
	}
	vrpn_float64 getDelta () {
		return _delta;
	}
	void setData (
		vrpn_int32 sensor,
		vrpn_float64 vector3[3],
		vrpn_float64 quaternion[4],
		vrpn_float64 delta
	) {
		_sensor = sensor;
		_vector3.assign(vector3, vector3 + 3);
		_quaternion.assign(quaternion, quaternion + 4);
	}

private:
	vrpn_int32 _sensor;
	std::vector<vrpn_float64> _quaternion;
	std::vector<vrpn_float64> _vector3;
	vrpn_float64 _delta;
};

class TrackerPositionChangeData : public TrackerBaseChangeData {
public:
	std::vector<vrpn_float64> getPosition () {
		return getVector3();
	}
	void setData (vrpn_TRACKERCB data) {
		TrackerBaseChangeData::setData(data.sensor, data.pos, data.quat, 0);
	}
};

class TrackerVelocityChangeData : public TrackerBaseChangeData {
public:
	std::vector<vrpn_float64> getVelocity () {
		return getVector3();
	}
	std::vector<vrpn_float64> getVelocityQuaternion () {
		return getQuaternion();
	}
	vrpn_float64 getVelocityQuaternionDelta () {
		return getDelta();
	}
	void setData (vrpn_TRACKERVELCB data) {
		TrackerBaseChangeData::setData(
			data.sensor, data.vel, data.vel_quat, data.vel_quat_dt
		);
	}
};

class TrackerAccelerationChangeData : public TrackerBaseChangeData {
public:
	std::vector<vrpn_float64> getAcceleration () {
		return getVector3();
	}
	std::vector<vrpn_float64> getAccelerationQuaternion () {
		return getQuaternion();
	}
	vrpn_float64 getAccelerationQuaternionDelta () {
		return getDelta();
	}
	void setData (vrpn_TRACKERACCCB data) {
		TrackerBaseChangeData::setData(
			data.sensor, data.acc, data.acc_quat, data.acc_quat_dt
		);
	}
};

class TrackerRemoteDevice : public BaseVRPNDeviceBinding<vrpn_Tracker_Remote> {
public:
	TrackerRemoteDevice (std::string deviceName, std::string address)
		: BaseVRPNDeviceBinding(deviceName, address) {
		initPositionChange();
		initVelocityChange();
		initAccelerationChange();
	}

	virtual ~TrackerRemoteDevice () {
		LIV_EVENT_RELEASE(this, PositionChange);
		LIV_EVENT_RELEASE(this, VelocityChange);
		LIV_EVENT_RELEASE(this, AccelerationChange);
	}

	void requestUpdate () {
		if (isUp()) {
			getDevice()->mainloop();	
		}
	}

	void disconnect () {
		LIV_EVENT_CANCEL(this, PositionChange);
		LIV_EVENT_CANCEL(this, VelocityChange);
		LIV_EVENT_CANCEL(this, AccelerationChange);
		releaseDevice();
	}

	LIV_EVENT(PositionChange, TrackerPositionChangeData*)
	LIV_EVENT(VelocityChange, TrackerVelocityChangeData*)
	LIV_EVENT(AccelerationChange, TrackerAccelerationChangeData*)

protected:
	vrpn_Tracker_Remote * makeDevice () {
		return new vrpn_Tracker_Remote(getVRPNDeviceName().c_str());
	}

private:
	TrackerPositionChangeData _positionData;
	vrpn_TRACKERCHANGEHANDLER _positionHandler;
	void initPositionChange () {
		_positionHandler = [] (
			void *userData, const vrpn_TRACKERCB info
		) {
			std::cout << "position\n";
			auto $this = (TrackerRemoteDevice *)userData;
			$this->_positionData.setData(info);
			LIV_EVENT_INVOKE(
				$this, PositionChange, &($this->_positionData)
			);
		};

		LIV_EVENT_INIT(
			this, PositionChange, TrackerPositionChangeData*,
			[this] () {
				std::cout << "IN position\n";
				return getDevice()
					->register_change_handler(this, _positionHandler);
			},
			[this] () {
				return getDevice()
					->unregister_change_handler(this, _positionHandler);
			}
		);
	}

	TrackerVelocityChangeData _velocityData;
	vrpn_TRACKERVELCHANGEHANDLER _velocityHandler;
	void initVelocityChange () {
		_velocityHandler = [] (
			void *userData, const vrpn_TRACKERVELCB info
		) {
			std::cout << "velocity\n";
			auto $this = (TrackerRemoteDevice *)userData;
			$this->_velocityData.setData(info);
			LIV_EVENT_INVOKE(
				$this, VelocityChange, &($this->_velocityData)
			);
		};

		LIV_EVENT_INIT(
			this, VelocityChange, TrackerVelocityChangeData*,
			[this] () {
			std::cout << "IN velocity\n";
				return getDevice()
					->register_change_handler(this, _velocityHandler);
			},
			[this] () {
				return getDevice()
					->unregister_change_handler(this, _velocityHandler);
			}
		);
	}

	TrackerAccelerationChangeData _accelerationData;
	vrpn_TRACKERACCCHANGEHANDLER _accelerationHandler;
	void initAccelerationChange () {
		_accelerationHandler = [] (
			void *userData, const vrpn_TRACKERACCCB info
		) {
			std::cout << "aceeleration\n";
			auto $this = (TrackerRemoteDevice *)userData;
			$this->_accelerationData.setData(info);
			LIV_EVENT_INVOKE(
				$this, AccelerationChange, &($this->_accelerationData)
			);
		};

		LIV_EVENT_INIT(
			this, AccelerationChange, TrackerAccelerationChangeData*,
			[this] () {
				return getDevice()
					->register_change_handler(this, _accelerationHandler);
			},
			[this] () {
				return getDevice()
					->unregister_change_handler(this, _accelerationHandler);
			}
		);
	}
};

NBIND_CLASS(TrackerBaseChangeData) {
	NBIND_METHOD(getSensor);
	NBIND_METHOD(getVector3);
	NBIND_METHOD(getQuaternion);
	NBIND_METHOD(getDelta);
}

NBIND_CLASS(TrackerPositionChangeData) {
	NBIND_INHERIT(TrackerBaseChangeData);
	NBIND_METHOD(getPosition);
}

NBIND_CLASS(TrackerVelocityChangeData) {
	NBIND_INHERIT(TrackerBaseChangeData);
	NBIND_METHOD(getVelocity);
	NBIND_METHOD(getVelocityQuaternion);
	NBIND_METHOD(getVelocityQuaternionDelta);
}

NBIND_CLASS(TrackerAccelerationChangeData) {
	NBIND_INHERIT(TrackerBaseChangeData);
	NBIND_METHOD(getAcceleration);
	NBIND_METHOD(getAccelerationQuaternion);
	NBIND_METHOD(getAccelerationQuaternionDelta);
}

NBIND_CLASS(TrackerRemoteDevice) {
	NBIND_INHERIT(IVRPNDeviceBinding);
	NBIND_CONSTRUCT<std::string, std::string>();
	LIV_EVENT_BIND(PositionChange);
	LIV_EVENT_BIND(VelocityChange);
	LIV_EVENT_BIND(AccelerationChange);
}

LIV_CALLBACKLIST_EXPORT_TYPE(TrackerPositionChange, TrackerPositionChangeData*);
LIV_CALLBACKLIST_EXPORT_TYPE(TrackerVelocityChange, TrackerVelocityChangeData*);
LIV_CALLBACKLIST_EXPORT_TYPE(
	TrackerAccelerationChange, TrackerAccelerationChangeData*
);
