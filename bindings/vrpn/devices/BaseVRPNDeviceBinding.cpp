#include "../IVRPNDeviceBinding.cpp"

template <class TDevice> 
class BaseVRPNDeviceBinding : public IVRPNDeviceBinding {
public:
	BaseVRPNDeviceBinding (std::string deviceName, std::string address)
		: _deviceName(deviceName), _deviceAddress(address) {}
	
	virtual ~BaseVRPNDeviceBinding () {
		releaseDevice();
	}
	
	std::string getDeviceName () {
		return _deviceName;
	};
	
	std::string getDeviceAddress () {
		return _deviceAddress;
	};

protected:
	std::string getVRPNDeviceName () {
		return _deviceName + "@" + _deviceAddress;
	}

	TDevice * getDevice () {
		if (_vrpnDevice == NULL) {
			_vrpnDevice = makeDevice();
		}
		return _vrpnDevice;
	}

	virtual TDevice * makeDevice () = 0;

	void releaseDevice () {
		if (_vrpnDevice != NULL) {
			delete _vrpnDevice;
			_vrpnDevice = NULL;	
		}
	}

	bool isUp () {
		return _vrpnDevice != NULL;
	}

private:
	std::string _deviceName;
	std::string _deviceAddress;
	TDevice *_vrpnDevice = NULL;
};
