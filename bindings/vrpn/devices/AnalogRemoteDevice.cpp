#include "../../../libs/vrpn/vrpn_Analog.h"
#include "BaseVRPNDeviceBinding.cpp"
#include "../CallbackList.cpp"

class AnalogRemoteDevice : public BaseVRPNDeviceBinding<vrpn_Analog_Remote> {
public:
	AnalogRemoteDevice (std::string deviceName, std::string address)
		: BaseVRPNDeviceBinding(deviceName, address) {

		_handler = [] (void *userData, const vrpn_ANALOGCB info) {
			auto $this = (AnalogRemoteDevice *)userData;
			std::vector<vrpn_float64> data(
				info.channel, info.channel + info.num_channel
			);
			LIV_EVENT_INVOKE($this, Change, data);
		};

		LIV_EVENT_INIT(
			this, Change, std::vector<vrpn_float64>,
			[this] () {
				return getDevice()->register_change_handler(this, _handler);
			},
			[this] () {
				return getDevice()->unregister_change_handler(this, _handler);
			}
		);
	}

	virtual ~AnalogRemoteDevice () {
		LIV_EVENT_RELEASE(this, Change);
	}

	void requestUpdate () {
		if (isUp()) {
			getDevice()->mainloop();	
		}
	}

	void disconnect () {
		LIV_EVENT_CANCEL(this, Change);
		releaseDevice();
	}

	LIV_EVENT(Change, std::vector<vrpn_float64>)

protected:
	vrpn_Analog_Remote * makeDevice () {
		return new vrpn_Analog_Remote(getVRPNDeviceName().c_str());
	}

private:
	vrpn_ANALOGCHANGEHANDLER _handler;
};

NBIND_CLASS(AnalogRemoteDevice) {
	NBIND_INHERIT(IVRPNDeviceBinding);
	NBIND_CONSTRUCT<std::string, std::string>();
	LIV_EVENT_BIND(Change);
}

LIV_CALLBACKLIST_EXPORT_TYPE(AnalogChange, std::vector<vrpn_float64>);
