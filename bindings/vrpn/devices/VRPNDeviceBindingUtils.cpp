#include <thread>
#include <chrono>
#include "../IVRPNDeviceBinding.cpp"

void VRPNDeviceBindingSafelyDisconnect (IVRPNDeviceBinding *device) {
	std::thread disconnectionThread([device] () {
		std::this_thread::sleep_for(std::chrono::seconds(1));
		device->disconnect();
	});

	disconnectionThread.detach();
}

NBIND_GLOBAL() {
  NBIND_FUNCTION(VRPNDeviceBindingSafelyDisconnect);
}
