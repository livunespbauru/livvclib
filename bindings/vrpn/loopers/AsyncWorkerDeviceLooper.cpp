#include "BaseVRPNDeviceLooper.cpp"

class AsyncWorkerDeviceLooper : public BaseVRPNDeviceLooper {
public:
	AsyncWorkerDeviceLooper (IVRPNDeviceBinding *device)
		: BaseVRPNDeviceLooper(device) {}

	~AsyncWorkerDeviceLooper () {}

	bool setup () {
		enqueueWorker();
		return true;
	}

protected:
	void callBack () {
		if (isRunning()) {
			enqueueWorker();
		}
	}

	void enqueueWorker () {
		auto currentWorker = new VRPNTrackerAsyncWorker(this);
		Nan::AsyncQueueWorker(currentWorker);
	}

	class VRPNTrackerAsyncWorker : public Nan::AsyncWorker {
	public:
		VRPNTrackerAsyncWorker (AsyncWorkerDeviceLooper *tracker)
			: _looper(tracker), AsyncWorker(NULL) {
		}
		~VRPNTrackerAsyncWorker () {
			_looper = NULL;
		}
		void Execute () {
			// lol
		}
		void HandleOKCallback () {
			if (_looper->isRunning()) {
				_looper->getDeviceBinding()->requestUpdate();	
			}
			_looper->callBack();
		}
	private:
		AsyncWorkerDeviceLooper *_looper;
	};
};

NBIND_CLASS(AsyncWorkerDeviceLooper) {
	NBIND_INHERIT(BaseVRPNDeviceLooper);
	NBIND_CONSTRUCT<IVRPNDeviceBinding *>();
}
