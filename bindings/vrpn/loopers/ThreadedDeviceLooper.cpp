#include "BaseVRPNDeviceLooper.cpp"
#include <thread> 

class ThreadedDeviceLooper : public BaseVRPNDeviceLooper {
public:
	ThreadedDeviceLooper (IVRPNDeviceBinding *device)
		: BaseVRPNDeviceLooper(device) {}

	~ThreadedDeviceLooper () {
		unsetup();
	}

private:
	std::thread *_thread;

protected:
	bool setup () {
		_thread = new std::thread(
			[this] {
				while (this->isRunning()) {
					this->getDeviceBinding()->requestUpdate();
				}
			}
		);
		_thread->join();
		return true;
	}

	void unsetup () {
		std::cout << "C: unsetup\n";
		if(_thread != NULL) {
			_thread->join();
			delete _thread;
			_thread = NULL;
		}
	}
};

NBIND_CLASS(ThreadedDeviceLooper) {
	NBIND_INHERIT(BaseVRPNDeviceLooper);
	NBIND_CONSTRUCT<IVRPNDeviceBinding *>();
}
