#include "../IVRPNDeviceLooper.cpp"

class BaseVRPNDeviceLooper : public IVRPNDeviceLooper {
public:
	BaseVRPNDeviceLooper (IVRPNDeviceBinding *device)
		: _device(device) {}

	virtual ~BaseVRPNDeviceLooper () {
		_device = NULL;
	}

	bool start () {
		if (isRunning()) {
			return false;
		}

		_running = true;
		return _running = setup();
	}

	virtual void stop () {
		_running = false;
		unsetup();
	}

	IVRPNDeviceBinding * getDeviceBinding () {
		return _device;
	}

	bool isRunning () {
		return _running;
	}

private:
	IVRPNDeviceBinding *_device = NULL;
	bool _running = false;

protected:
	virtual bool setup () = 0;
	virtual void unsetup () {}
};

NBIND_CLASS(BaseVRPNDeviceLooper) {
	NBIND_INHERIT(IVRPNDeviceLooper);
}
