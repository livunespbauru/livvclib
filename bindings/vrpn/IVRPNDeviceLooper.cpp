#include "IVRPNDeviceBinding.cpp"
#include "nbind/noconflict.h"

class IVRPNDeviceLooper {
public:
	virtual ~IVRPNDeviceLooper () {}
	virtual bool start () = 0;
	virtual bool isRunning() = 0;
	virtual void stop () = 0;
	virtual IVRPNDeviceBinding * getDeviceBinding() = 0;
};

NBIND_CLASS(IVRPNDeviceLooper) {
	NBIND_METHOD(start);
	NBIND_METHOD(stop);
	NBIND_METHOD(getDeviceBinding);
	NBIND_METHOD(isRunning);
}
