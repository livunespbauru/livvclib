#include <map>
#include <string>
#include <functional>
#include "nbind/noconflict.h"

#include <iostream>

typedef std::function<int()> procedure;

#define LIV_EVENT_VAR(name) _on ## name
#define LIV_EVENT_GETTER(name) getOn ## name

#define LIV_EVENT(name, type)\
TypedCallbackList<type> * LIV_EVENT_VAR(name);\
TypedCallbackList<type> * LIV_EVENT_GETTER(name) () {\
	return this->LIV_EVENT_VAR(name);\
}

#define LIV_EVENT_INIT(caller, name, type, start, stop)\
caller->LIV_EVENT_VAR(name) = new TypedCallbackList<type>(start, stop);

#define LIV_EVENT_INVOKE(caller, name, data)\
caller->LIV_EVENT_GETTER(name)()->invoke(data)

#define LIV_EVENT_RELEASE(caller, name) delete caller->LIV_EVENT_VAR(name);\
caller->LIV_EVENT_VAR(name) = NULL

#define LIV_EVENT_CANCEL(caller, name) caller->LIV_EVENT_VAR(name)->cancel()

#define LIV_EVENT_BIND(name) NBIND_METHOD(getOn ## name)

#define LIV_CALLBACKLIST_EXPORT_TYPE(prefix, type) \
NBIND_CLASS(TypedCallbackList<type>, prefix ## CallbackList) {\
	NBIND_INHERIT(CallbackList);\
}

class CallbackList {
public:
	CallbackList (procedure start, procedure stop)
		: _start(start), _stop(stop) {}

	virtual ~CallbackList () {
		cancel();
	}

	int registerCallback (std::string key, nbind::cbFunction callback) {
		int unregistered = 0;
		if ((unregistered = unregisterCallback(key)) != 0) {
			return unregistered;
		}

		int started = 0;
		if (size() > 0 || (started = _start()) == 0) {
			_callbacks[key]  = new nbind::cbFunction(callback);
			return 0;
		}

		return started;
	}

	int unregisterCallback (std::string key) {
		auto existing = _callbacks.find(key);

		if (existing != _callbacks.end()) {
			int stopped = 0;
			if (size() > 1 || (stopped = _stop()) == 0) {
				_callbacks.erase(existing);
				delete existing->second;
				return 0;
			}

			return stopped;
		}

		return 0;
	}

	void cancel () {
		auto copy = _callbacks;

		for (auto iterator = copy.begin(); iterator != copy.end(); ++iterator) {
			unregisterCallback(iterator->first);
		}
	}

	int size () {
		return _callbacks.size();
	}

	void invoke () {
		auto copy = _callbacks;
		for (auto iterator = copy.begin(); iterator != copy.end(); ++iterator) {
			iterator->second->call<void>();
		}
	}

protected:
	std::map<std::string, nbind::cbFunction *> _callbacks;
	procedure _start, _stop;
};

NBIND_CLASS(CallbackList) {
	NBIND_METHOD(registerCallback);
	NBIND_METHOD(unregisterCallback);
}

template <typename T> class TypedCallbackList : public CallbackList {
public:
	TypedCallbackList (procedure start, procedure stop)
		: CallbackList(start, stop) {}

	void invoke (T data) {
		auto copy = _callbacks;
		for (auto iterator = copy.begin(); iterator != copy.end(); ++iterator) {
			iterator->second->call<void>(data);
		}
	}
};
