#include <cstdlib>
#include "nbind/noconflict.h"

#include <iostream>

class IVRPNDeviceBinding {
public:
	virtual ~IVRPNDeviceBinding () {}
	virtual std::string getDeviceName () = 0;
	virtual std::string getDeviceAddress () = 0;
	virtual void requestUpdate () = 0;
	virtual void disconnect() = 0;
};

NBIND_CLASS(IVRPNDeviceBinding) {
	NBIND_METHOD(getDeviceName);
	NBIND_METHOD(getDeviceAddress);
	NBIND_METHOD(requestUpdate);
	NBIND_METHOD(disconnect);
}