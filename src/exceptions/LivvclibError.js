/**
 * @author luizssb
 */
function LivvclibError (message, code, previous, doNotLog) {
	var tmp = Error.apply(this, arguments);
	tmp.name = this.name = 'LivvclibError';

	this.message = tmp.message;
	this.code = code;
	this.previous = previous;
	this.stack = tmp.stack;

	if (!doNotLog) {
		console.error(this);
	}
	return this;
}

LivvclibError.prototype = Object.assign(
	Object.create(Error.prototype),
	{ constructor: LivvclibError }
);

module.exports = LivvclibError;
