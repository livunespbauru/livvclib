try {
	window.WatchJS = require('melanke-watchjs');
	window.THREE = require('three');
	window.Signal = require('signals');
	window.io = require('socket.io-client');
} catch (e) {}

module.exports = {
	Screen: require('./cameras/Screen'),
	DistributedCamera: require('./cameras/DistributedCamera'),

	MasterSlaveCluster: require('./cluster/MasterSlaveCluster'),
	ClusterJoiningMode: require('../constants/ClusterJoiningMode'),
	BufferedMasterSlaveCluster: require('./cluster/BufferedMasterSlaveCluster'),
	BufferMode: require('./cluster/BufferMode'),
	ClusterDataEventManager: require('./cluster/ClusterDataEventManager'),
	Sync: require('./cluster/Sync'),
	RotationSyncSetter: require('./cluster/RotationSyncSetter'),

	NodeType: require('../constants/NodeType'),
	NodeConnectionMethod: require('../constants/NodeConnectionMethod'),

	NodeConnection: require('./connections/node/NodeConnection'),
	WebRTCNodeConnection: require('./connections/node/WebRTCNodeConnection'),
	SignalingServerNodeConnection:
		require('./connections/node/SignalingServerNodeConnection'),
	NodeConnectionFactory: require('./connections/node/NodeConnectionFactory'),

	RoomEvents: require('../constants/RoomEvents'),

	ServerConnection: require('./connections/server/ServerConnection'),
	SocketIOServerConnection:
		require('./connections/server/SocketIOServerConnection'),

	ObjectEx: require('../utils/ObjectEx'),
	RandomUtils: require('../utils/RandomUtils'),
	computeTilt: require('../utils/computeTilt'),
	toDegrees: require('../utils/toDegrees'),

	DeviceControlAccess: require('./devices/DeviceControlAccess'),
	DeviceSimpleAccess: require('./devices/DeviceSimpleAccess'),
	NewDevicesListener: require('./devices/NewDevicesListener'),
	DeviceInterface: require('../constants/DeviceInterface'),

	DeviceInterfaceTracker:
		require('./devices/trackers/DeviceInterfaceTracker'),
	AnalogTracker: require('./devices/trackers/AnalogTracker'),
	ButtonTracker: require('./devices/trackers/ButtonTracker'),

	AnalogEvents: require('../constants/AnalogEvents'),
	ButtonEvents: require('../constants/ButtonEvents')
};
