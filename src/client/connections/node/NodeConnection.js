const ObjectEx = require('../../../utils/ObjectEx');

/**
 * @author luizssb
 */
function NodeConnection (otherNode) {
	this._otherNode = otherNode;

	ObjectEx.defineEvents(
		this, [
			/**
			 * Called when it creates an offer that must be sent to another node.
			 * @param {NodeConnection} conn The node connection instance dispatching the event.
			 * @param {Object} offer Offer data.
			 */
			'offerCreated',

			/**
			 * Called when it fails to create an offer.
			 * @param {NodeConnection} conn The node connection instance dispatching the event.
			 * @param {Object} error Error data.
			 */
			'offerCreatedError',

			/**
			 * Called when it creates an answer that must be sent to another node.
			 * @param {NodeConnection} conn The node connection instance dispatching the event.
			 * @param {Object} answer Answer data.
			 */
			'answerCreated',

			/**
			 * Called when it fails to create an answer.
			 * @param {NodeConnection} conn The node connection instance dispatching the event.
			 * @param {Object} error Error Data.
			 */
			'answerCreatedError',

			/**
			 * Called when it has additional connection data that must be sent.
			 * @param {NodeConnection} conn The node connection instance dispatching the event.
			 * @param {Object} candidateData Candidate's data.
			 */
			'candidateDataReady',

			/**
			 * Called when there is a status change in one in the send or receive channel.
			 * @param {NodeConnection} conn The node connection instance dispatching the event.
			 * @param {boolean} canSend True if send channel is open, false otherwise
			 * @param {boolean} canReceive True if receive channel is open, false otherwise
			 */
			'dataChannelStatusChanged',

			/**
			 * Called when it receives data that is an object.
			 * @param {NodeConnection} conn The node connection instance dispatching the event.
			 * @param {Object} data The object that was received.
			 */
			'objectReceived',

			/**
			 * Called when it receives data that is a primitive type, such as string or int.
			 * @param {NodeConnection} conn The node connection instance dispatching the event.
			 * @param {string|boolean|number} data The primitiva data that was received.
			 */
			'primitiveDataReceived'
		]
	);
}

ObjectEx.defineProperties(NodeConnection.prototype, {
	otherNode: ObjectEx.readOnly(function () { return this._otherNode; }),
	ready: ObjectEx.readOnly(ObjectEx.abstractMethod)
});

/**
 * Callback/boolean to determine what should happen in case connect() is invoked but the connection is already established.
 * @param {NodeConnection} conn The node connection instance trying to reconnect.
 * @return {boolean} True if the instance should disconnect and the connect again,
 *                 false otherwise
 */
NodeConnection.prototype.shouldReconnect = true;

/**
 * Starts the process which should end up creating a connection between this node and the 'otherNode'.
 * @return {NodeConnection} Itself.
 */
NodeConnection.prototype.connect = ObjectEx.abstractMethod;

/**
 * Handles a connection offer sent by a remote node.
 * @param {Object} offer The offer's data.
 * @return {NodeConnection} Itself.
 */
NodeConnection.prototype.handleOffer = ObjectEx.abstractMethod;

/**
 * Handles the connection answer sent by the remote node.
 * @param {Object} answer The answer's data.
 * @return {NodeConnection} Itself.
 */
NodeConnection.prototype.handleAnswer = ObjectEx.abstractMethod;

/**
 * Handles a failure of the remote node to generate an answer to the offer.
 * @param {Object} error The error's data.
 * @return NodeConnection Itself.
 */
NodeConnection.prototype.handleAnswerFailed = ObjectEx.abstractMethod;

/**
 * Handles candidate date sent by the remote  node.
 * @param {Object} candidateData The candidadate's data.
 * @return NodeConnection Itself.
 */
NodeConnection.prototype.handleCandidate = ObjectEx.abstractMethod;

/**
 * Sends a data "package" to the remote node.
 * This must should be an object with the following attributes:
 *     - id: an unique identifier for the package.
 *     - data: the real data to be sent.
 *     - recipient: the id of the receiver (optional; if not included, it means
 *                  the package is intended for all nodes)
 * Although possible, it is NOT recommended to use a timestamp for the id, as
 * timestamps created with Javascript are in seconds, therefore making it
 * possible to create duplicate ids should data be sent in rapid sucession
 * (e.g. in a loop).
 * When the other node receives the dataPackage, it must call one of two events:
 *     - on.objectReceived, if the data field is an object.
 *     - on.primitiveDataReceived, if the data field is a primitive value.
 * @param {Object} dataPackage A package wrapping the data to be sent.
 * @return {NodeConnection} Itself.
 */
NodeConnection.prototype.sendDataPackage = ObjectEx.abstractMethod;

/**
 * Breaks the connection with the other node.
 * @return {NodeConnection} Itself.
 */
NodeConnection.prototype.disconnect = ObjectEx.abstractMethod;

module.exports = NodeConnection;
