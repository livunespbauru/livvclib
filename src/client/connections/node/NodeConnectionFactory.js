const WebRTCNodeConnection = require('./WebRTCNodeConnection');
const SignalingServerNodeConnection = require('./SignalingServerNodeConnection');
const NodeConnectionMethod = require('../../../constants/NodeConnectionMethod');
const LivvclibError = require('../../../exceptions/LivvclibError');
const ObjectEx = require('../../../utils/ObjectEx');

/**
 * @author luizssb
 */
function NodeConnectionFactory (preferredConnectionMethod) {
	if (preferredConnectionMethod !== undefined) {
		this.preferredConnectionMethod = preferredConnectionMethod;
	}
}

NodeConnectionFactory.prototype.makeConnection = function (
	forPeer, fromServerConnection
) {
	var thisPreferredConnectionMethod =
		ObjectEx.evaluateDelegate(this, this.preferredConnectionMethod, forPeer);

	if (!RTCPeerConnection && // eslint-disable-line no-undef
		thisPreferredConnectionMethod === NodeConnectionMethod.WEBRTC) {
		thisPreferredConnectionMethod = NodeConnectionMethod.SERVER;
		console.warn('Livvclib: preferred connection method change to SERVER, as WebRTC is not available in this browser.');
	}

	if (forPeer.connectionMethod === NodeConnectionMethod.SERVER ||
		thisPreferredConnectionMethod === NodeConnectionMethod.SERVER) {
		return new SignalingServerNodeConnection(
			forPeer, fromServerConnection
		);
	} else if (forPeer.connectionMethod === NodeConnectionMethod.WEBRTC) {
		return new WebRTCNodeConnection(forPeer);
	}

	throw new LivvclibError('unknown connection method', 33);
};

/**
 * Callback/NodeConnectionMethod that determines the preferred node connection method for the current node.
 * @param NodeConnectionFactory sender Instance creating the NodeConnection.
 * @param {Object} otherNode Other node data.
 * @return {NodeConnectionMethod} THe preferred connection method.
 */
NodeConnectionFactory.prototype.preferredConnectionMethod =
	NodeConnectionMethod.WEBRTC;

module.exports = NodeConnectionFactory;
