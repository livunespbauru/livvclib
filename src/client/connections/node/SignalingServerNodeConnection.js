const NodeConnection = require('./NodeConnection');
const LivvclibError = require('../../../exceptions/LivvclibError');
const ObjectEx = require('../../../utils/ObjectEx');

// eslint-disable-next-line camelcase
const SignalingServerNodeConnection_GetManager =
	require('./SignalingServerNodeConnectionManager');

/**
 * @author luizssb
 */
function SignalingServerNodeConnection (otherNode, serverConnection) {
	NodeConnection.call(this, otherNode);

	this.setServerConnection(serverConnection);
}

SignalingServerNodeConnection.prototype = Object.assign(
	Object.create(NodeConnection.prototype),
	{ constructor: SignalingServerNodeConnection }
);

// Override
Object.defineProperty(
	SignalingServerNodeConnection.prototype, 'ready',
	ObjectEx.readOnly(function () { return !!this._ready; })
);

SignalingServerNodeConnection.prototype.setServerConnection = function (
	serverConnection
) {
	if (this.ready) {
		throw new LivvclibError(
			'cannot change server connection when connected', 87
		);
	}

	this._manager = SignalingServerNodeConnection_GetManager(serverConnection);
};

// Override
SignalingServerNodeConnection.prototype.connect = function () {
	if (!this._manager.ready) {
		this.on.offerCreatedError
			.dispatch(this, {message: 'server connection not ready'});
		return;
	}

	if (this.ready) {
		if (!ObjectEx.evaluateDelegate(this, this.shouldReconnect)) {
			return;
		}

		this.disconnect();
	}

	this.on.offerCreated.dispatch(this, {});

	return this;
};

// Override
SignalingServerNodeConnection.prototype.handleOffer = function (offer) {
	this.on.answerCreated.dispatch(this, {});
	this.on.candidateDataReady.dispatch(this, {});

	return this;
};

// Override
SignalingServerNodeConnection.prototype.handleAnswer = function (answer) {
	this.on.candidateDataReady.dispatch(this, {});

	return this;
};

// Override
SignalingServerNodeConnection.prototype.handleAnswerFailed = function (error) {
	throw new LivvclibError('not supposed to happen', 81, error);
};

// Override
SignalingServerNodeConnection.prototype.handleCandidate = function (candidate) {
	this._manager.manage(this);
	this._ready = true;
	this.on.dataChannelStatusChanged.dispatch(this, true, true);

	return this;
};

// Override
SignalingServerNodeConnection.prototype.sendDataPackage = function (
	dataPackage
) {
	if (this.ready) {
		this._manager.sendDataPackage(dataPackage);
	}

	return this;
};

// Override
SignalingServerNodeConnection.prototype.disconnect = function () {
	if (this._manager.isManaging(this)) {
		this._manager.unmanage(this);
		this._ready = false;

		this.on.dataChannelStatusChanged.dispatch(this, false, false);
	}

	return this;
};

module.exports = SignalingServerNodeConnection;
