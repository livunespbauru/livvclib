const NodeEvents = require('../../../constants/NodeEvents');
const ObjectEx = require('../../../utils/ObjectEx');

/**
 * @author luizssb
 */
const _managers = {};

function getManager (serverConnection) {
	if (!_managers[serverConnection.id]) {
		_managers[serverConnection.id] =
			new SignalingServerNodeConnectionManager(serverConnection);
	}

	return _managers[serverConnection.id];
};

function releaseManager (id) {
	if (_managers[id]) {
		const connections = _managers[id]._nodeConnections;
		for (var connectionID in connections) {
			connections[connectionID].disconnect();
		}
		delete _managers[id];
	}
}

function SignalingServerNodeConnectionManager (serverConnection) {
	this._serverConnection = serverConnection
		.on(NodeEvents.FromServer.NODE_GONE, this._handleNodeGone.bind(this))
		.on(
			NodeEvents.FromServer.RECEIVED_DATA,
			this._handleDataPackage.bind(this)
		);

	this._serverConnection.on.willDisconnect.add(this._disconnect, this);
	this._serverConnection.on.disconnected.add(this._disconnect, this);

	this._nodeConnections = {};
}

Object.defineProperty(
	SignalingServerNodeConnectionManager.prototype, 'ready', ObjectEx.readOnly(
		function () { return !!this._serverConnection.connected; }
	)
);

SignalingServerNodeConnectionManager.prototype.manage = function (
	nodeConnection
) {
	this._nodeConnections[nodeConnection.otherNode.id] = nodeConnection;
};

SignalingServerNodeConnectionManager.prototype.unmanage = function (
	nodeConnection
) {
	if (nodeConnection.otherNode.id === this._serverConnection.id) {
		this._serverConnection.emit(NodeEvents.ToServer.QUIT);
	}

	delete this._nodeConnections[nodeConnection.otherNode.id];
};

SignalingServerNodeConnectionManager.prototype.isManaging = function (
	nodeConnection
) {
	return this._nodeConnections[nodeConnection.otherNode.id] !== undefined;
};

SignalingServerNodeConnectionManager.prototype.sendDataPackage = function (
	dataPackage
) {
	if (dataPackage.id !== this._lastData) {
		this._serverConnection.emit(NodeEvents.ToServer.SEND_DATA, dataPackage);
	}

	this._lastData = dataPackage.id;
};

SignalingServerNodeConnectionManager.prototype._handleDataPackage = function (
	senderData, dataPackage
) {
	const iteration = function (event) {
		if (dataPackage.recipient) {
			this._nodeConnections[senderData.sender].on[event].dispatch(
				this._nodeConnections[senderData.sender], dataPackage.data
			);
		} else {
			for (var idObject in this._nodeConnections) {
				this._nodeConnections[idObject].on[event]
					.dispatch(this._nodeConnections[idObject], dataPackage.data);
			}
		}
	}.bind(this);

	if (ObjectEx.isObject(dataPackage.data)) {
		iteration('objectReceived');
	} else {
		iteration('primitiveDataReceived');
	}
};

SignalingServerNodeConnectionManager.prototype._handleNodeGone = function (
	nodeId
) {
	for (var id in this._nodeConnections) {
		if (id === nodeId) {
			this._nodeConnections[id].disconnect();
			delete this._nodeConnections[id];
			break;
		}
	}
};

SignalingServerNodeConnectionManager.prototype._disconnect = function (
	sender
) {
	this._serverConnection
		.off(NodeEvents.FromServer.DATA_RECEIVED)
		.off(NodeEvents.FromServer.NODE_GONE);

	this._serverConnection.on.willDisconnect.remove(this._disconnect, this);
	this._serverConnection.on.disconnected.remove(this._disconnect, this);

	releaseManager(this._serverConnection.id);
};

module.exports = getManager;
