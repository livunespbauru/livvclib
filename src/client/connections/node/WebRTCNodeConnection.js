const NodeConnection = require('./NodeConnection');
const ObjectEx = require('../../../utils/ObjectEx');

/**
 * @author luizssb
 */
function WebRTCNodeConnection (otherNode) {
	NodeConnection.call(this, otherNode);

	this._peer = null;
}

WebRTCNodeConnection.prototype = Object.assign(
	Object.create(NodeConnection.prototype),
	{ constructor: WebRTCNodeConnection }
);

ObjectEx.defineProperties(WebRTCNodeConnection.prototype, {
	peer: ObjectEx.readOnly(function () { return this._peer; }),

	// Override
	ready: ObjectEx.readOnly(function () {
		return this.peer &&
		this.peer.sendChannel &&
		this.peer.sendChannel.readyState === 'open' &&
		this.peer.receiveChannel &&
		this.peer.receiveChannel.readyState === 'open';
	})
});

// Override
WebRTCNodeConnection.prototype.connect = function () {
	if (this.ready) {
		if (!ObjectEx.evaluateDelegate(this, this.shouldReconnect)) {
			return;
		}

		this.disconnect();
	}

	this._peer = this._makePeerConnection();
	this.peer.connection.createOffer(
		this._handleOfferCreated.bind(this),
		this._handleOfferCreationError.bind(this)
	);

	return this;
};

// Override
WebRTCNodeConnection.prototype.sendDataPackage = function (dataPackage) {
	var serializedPackage = JSON.stringify(dataPackage);

	if (this.ready) {
		this.peer.sendChannel.send(serializedPackage);
	}

	return this;
};

// Override
WebRTCNodeConnection.prototype.disconnect = function () {
	if (this.peer) {
		if (this.peer.sendChannel) {
			this.peer.sendChannel.onopen = this.peer.sendChannel.onclose =
				undefined;
			this.peer.sendChannel.close();
		}

		if (this.peer.receiveChannel) {
			this.peer.receiveChannel.onopen = this.peer.sendChannel.onclose =
				undefined;
			this.peer.receiveChannel.close();
		}

		if (this.peer.connection) {
			this.peer.connection.close();
		}
	}

	this._peer = null;

	return this;
};

// Override
WebRTCNodeConnection.prototype.handleOffer = function (offer) {
	this._peer = this._makePeerConnection();

	// eslint-disable-next-line no-undef
	this.peer.connection.setRemoteDescription(new RTCSessionDescription(offer));
	this.peer.connection.createAnswer().then(
		this._handleAnswerCreated.bind(this),
		this._handleAnswerCreationError.bind(this)
	);

	return this;
};

// Override
WebRTCNodeConnection.prototype.handleAnswer = function (answer) {
	this.peer.connection.setRemoteDescription(
		new RTCSessionDescription(answer) // eslint-disable-line no-undef
	);

	return this;
};

// Override
WebRTCNodeConnection.prototype.handleAnswerFailed = function (errorData) {
	this.disconnect();

	return this;
};

// Override
WebRTCNodeConnection.prototype.handleCandidate = function (candidateData) {
	if (!this.peer) return;

	// eslint-disable-next-line no-undef
	const candidate = new RTCIceCandidate({
		sdpMLineIndex: candidateData.label,
		candidate: candidateData.candidate
	});

	this.peer.connection.addIceCandidate(candidate);

	return this;
};

// luizssb: peer creation
WebRTCNodeConnection.prototype._makePeerConnection = function () {
	const peer = {
		connection: new RTCPeerConnection(null) // eslint-disable-line no-undef
	};

	peer.connection.onicecandidate = this._handleICEData.bind(this);
	peer.connection.ondatachannel = this._handleDataChannel.bind(this);

	peer.sendChannel =
		peer.connection.createDataChannel('sendDataChannel', null);
	peer.sendChannel.onopen = peer.sendChannel.onclose =
		this._handleDataChannelsStatus.bind(this);

	return peer;
};

WebRTCNodeConnection.prototype._handleICEData = function (event) {
	if (event.candidate) {
		const iceData = {
			type: 'candidate',
			label: event.candidate.sdpMLineIndex,
			id: event.candidate.sdpMid,
			candidate: event.candidate.candidate
		};

		this.on.candidateDataReady.dispatch(this, iceData);
	}
};

WebRTCNodeConnection.prototype._handleDataChannel = function (event) {
	this.peer.receiveChannel = event.channel;
	this.peer.receiveChannel.onmessage = this._handleData.bind(this);
	this.peer.receiveChannel.onopen = this.peer.receiveChannel.onclose =
			this._handleDataChannelsStatus.bind(this);
};

WebRTCNodeConnection.prototype._handleDataChannelsStatus = function () {
	if (this.peer) {
		this.on.dataChannelStatusChanged.dispatch(this,
			this.peer.sendChannel &&
				this.peer.sendChannel.readyState === 'open',
			this.peer.receiveChannel &&
				this.peer.receiveChannel.readyState === 'open'
		);
	} else {
		this.disconnect();
	}
};

WebRTCNodeConnection.prototype._handleData = function (event) {
	const dataPackage = JSON.parse(event.data);

	if (ObjectEx.isObject(dataPackage.data)) {
		this.on.objectReceived.dispatch(this, dataPackage.data);
	} else {
		this.on.primitiveDataReceived.dispatch(this, dataPackage.data);
	}
};

// luizssb: webrtc handlers
WebRTCNodeConnection.prototype._handleOfferCreated = function (
	sessionDescription
) {
	this.peer.connection.setLocalDescription(sessionDescription);
	this.on.offerCreated.dispatch(this, sessionDescription);
};

WebRTCNodeConnection.prototype._handleOfferCreationError = function (event) {
	this.on.offerCreatedError.dispatch(this, event);
};

WebRTCNodeConnection.prototype._handleAnswerCreated = function (
	sessionDescription
) {
	this.peer.connection.setLocalDescription(sessionDescription);
	this.on.answerCreated.dispatch(this, sessionDescription);
};

WebRTCNodeConnection.prototype._handleAnswerCreationError = function (event) {
	this.on.answerCreatedError.dispatch(this, event);
};

module.exports = WebRTCNodeConnection;
