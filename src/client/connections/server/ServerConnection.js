const ObjectEx = require('../../../utils/ObjectEx');

/**
 * @author luizssb
 */
function ServerConnection (serverAddress) {
	if (serverAddress.indexOf('http') !== 0) {
		serverAddress = 'https://' + serverAddress;
	}
	this._serverAddress = serverAddress;

	this.on = this.on.bind(this);
	ObjectEx.defineEvents(
		this, [
			/**
			 * Called when it establishes connection with the server.
			 */
			'connected',

			/**
			 * Called when it fails to connect to the the server.
			 * @param {Object} error Error data.
			 */
			'connectedError',

			/**
			 * Called when a request will be made and the default parameters must be customized.
			 * @param {Object} data The default parameters that can be customized.
			 */
			'willEmit',

			/**
			 * Called when this node disconnects will from the server.
			 */
			'willDisconnect',

			/**
			 * Called when the server goes away.
			 */
			'disconnected'
		]
	);
}

ObjectEx.defineProperties(ServerConnection.prototype, {
	connected: ObjectEx.readOnly(ObjectEx.abstractMethod),
	id: ObjectEx.readOnly(ObjectEx.abstractMethod),
	serverAddress: ObjectEx.readOnly(function () { return this._serverAddress; })
});

/**
 * Callback/boolean to determine what should happen in case connect() is invoked
 * but the connection is already established.
 * @param {ServerConnection} sender The instance trying to reconnect.
 * @return {boolean} True if the instance should disconnect and the connect again,
 *                 false otherwise
 */
ServerConnection.prototype.shouldReconnect = true;

/**
 * Connects to the server.
 */
ServerConnection.prototype.connect = ObjectEx.abstractMethod;

/**
 * Subscribes a callback function to an event of the server.
 * @param {string} event Event name.
 * @param {Function} callback Callback to be called when the event happens.
 */
ServerConnection.prototype.on = ObjectEx.abstractMethod;

/**
 * Unsubscribes a callback function from an event of the server.
 * @param {string} event Event name.
 */
ServerConnection.prototype.off = ObjectEx.abstractMethod;

ServerConnection.prototype.emit = function (event) {
	const defaultParams = {};
	this.on.willEmit.dispatch(defaultParams);
	this._emit.apply(
		this,
		[event, defaultParams].concat(Array.prototype.slice.call(arguments, 1))
	);
};

/**
 * Triggers an event in the server.
 * @param {string} event Event name
 * @param {Object} [data] Event data
 */
ServerConnection.prototype._emit = ObjectEx.abstractMethod;

/**
 * Disconnects from the server
 */
ServerConnection.prototype.disconnect = ObjectEx.abstractMethod;

module.exports = ServerConnection;
