const ServerConnection = require('./ServerConnection');
const LivvclibError = require('../../../exceptions/LivvclibError');
const ObjectEx = require('../../../utils/ObjectEx');
const sio = require('socket.io-client');

/**
 * @author luizssb
 */
function SocketIOServerConnection (serverAddress) {
	ServerConnection.call(this, serverAddress);
	this._socket = null;
	this.reconnection = true;
}

SocketIOServerConnection.prototype = Object.assign(
	Object.create(ServerConnection.prototype),
	{ constructor: SocketIOServerConnection }
);

ObjectEx.defineProperties(SocketIOServerConnection.prototype, {
	socket: ObjectEx.readOnly(function () { return this._socket; }),

	// Override
	connected: ObjectEx.readOnly(
		function () { return this.socket && this.socket.connected; }
	),

	// Override
	id: ObjectEx.readOnly(
		function () { return this._socket ? this._socket.id : undefined; }
	)
});

// Override
SocketIOServerConnection.prototype.connect = function () {
	if (this.connected) {
		if (!ObjectEx.evaluateDelegate(this, this.shouldReconnect)) {
			return;
		}

		this.disconnect();
	}

	const onError = function (error) {
		this.on.connectedError.dispatch(error);
	}.bind(this);

	this._socket = sio
		.connect(this._serverAddress, { reconnection: this.reconnection })
		.on('connect', function () { this.on.connected.dispatch(); }.bind(this))
		.on('connect_error', onError)
		.on('connect_timeout', onError)
		.on('disconnect', this._handleServerWillDisconnect.bind(this));
};

SocketIOServerConnection.prototype._ensureConnected = function () {
	if (!this.connected) {
		throw new LivvclibError(
			'SocketIOServerConnection not connected', 90
		);
	}
};

// Override
SocketIOServerConnection.prototype.disconnect = function () {
	if (this.connected) {
		this.on.willDisconnect.dispatch();
		this.socket.disconnect();
		this._socket = null;
	}
};

// Override
SocketIOServerConnection.prototype.on = function (event, callback) {
	this._ensureConnected();
	this.socket.on(event, callback);
	return this;
};

// Override
SocketIOServerConnection.prototype.off = function (event) {
	if (this.connected) {
		this.socket.off(event);
	}
	return this;
};

// Override
SocketIOServerConnection.prototype._emit = function (event) {
	this._ensureConnected();
	this.socket.emit.apply(
		this.socket, Array.prototype.slice.call(arguments, 0)
	);
	return this;
};

// luizssb: server events handlers
SocketIOServerConnection.prototype._handleServerWillDisconnect = function () {
	this.socket = null;
	this.on.disconnected.dispatch();
};

module.exports = SocketIOServerConnection;
