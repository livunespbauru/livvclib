/**
 * @author luizssb
 */
module.exports = {
	ATTRIBUTE_CHANGE: 'attributeChange',
	METHOD_INVOCATION: 'methodInvocation'
};
