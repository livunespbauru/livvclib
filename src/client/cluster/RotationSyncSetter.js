/**
 * @author luizssb
 */
module.exports = function (obj, attr, newValue) {
	obj[attr.replace('_', '')] = newValue;
};
