const ClusterNodeConnector = require('./ClusterNodeConnector');
const NodeType = require('../../constants/NodeType');
const ClusterEvents = require('../../constants/ClusterEvents');
const ClusterJoiningMode = require('../../constants/ClusterJoiningMode');
const LivvclibError = require('../../exceptions/LivvclibError');
const ObjectEx = require('../../utils/ObjectEx');

/**
 * @author luizssb
 */
function MasterSlaveCluster (serverConnection, room, nodeConnFactory) {
	this._nodes = {};
	this._packageId = 0;
	this._room = room;

	this._serverConnection = serverConnection;
	this._nodeConnector =
		new ClusterNodeConnector(serverConnection, nodeConnFactory);
	this._nodeConnector.on.nodeConnected.add(this._handleNodeConnect, this);
	this._nodeConnector.on.nodeConnectedError.add(this._handleNodeConnectError, this);

	ObjectEx.defineEvents(
		this, [
			/**
			 * Called when it joins the cluster in the server.
			 * @param {int} participants Number of nodes in the cluster.
			 * @param {bool} master Data about the node. May be undefined for slaves waiting for the master.
			 */
			'joinedCluster',

			/**
			 * Called by slave nodes when the master joins the cluster.
			 * @param {object} masterNode Data about the master.
			 */
			'masterJoined',

			/**
			 * Called when it fails to connect to the the server.
			 * @param {Object} error Error data.
			 */
			'joinedClusterError',

			/**
			 * Called when it is the master node or finished connecting to it
			 */
			'readyInCluster',

			/**
			 * Called when it leaves the cluster.
			 */
			'leftCluster',

			/**
			 * Called when it successfully establishes connection with another node.
			 * @param {Object} node Data about the node.
			 */
			'nodeConnected',

			/**
			 * Called when it fails to establish connection with another node.
			 * @param {Object} node Node's data.
			 * @param {Object} error Error data.
			 */
			'nodeConnectedError',

			/**
			 * Called when it receives data that is an object
			 * @param {Object} data The object that was received.
			 */
			'objectReceived',

			/**
			 * Called when it receives data that is a primitive type, such as string or int.
			 * @param {string|boolean|number} data The primitiva data that was received.
			 */
			'primitiveDataReceived'
		]
	);
}

ObjectEx.defineProperties(MasterSlaveCluster.prototype, {
	nodeType: ObjectEx.readOnly(function () { return this._nodeType; }),
	room: ObjectEx.readOnly(function () { return this._room; }),
	preferredConnectionMethod: ObjectEx.readOnly(function () {
		return this._nodeConnector.connectionFactory.preferredConnectionMethod;
	}),
	joined: ObjectEx.readOnly(
		function () { return this._nodeType !== undefined; }
	),
	allowsSending: ObjectEx.readOnly(
		function () { return this.nodeType === NodeType.MASTER; }
	),
	allowsReceiving: ObjectEx.readOnly(true),
	numberOfPeers: ObjectEx.readOnly(function () {
		return Object.keys(this._nodes).length;
	})
});

MasterSlaveCluster.prototype.peek = function (callback) {
	this.serverConnection
		.on(ClusterEvents.FromServer.PEEKED, function (data, clusterData) {
			this.serverConnection.off(ClusterEvents.FromServer.PEEKED);
			callback(clusterData);
		}.bind(this))
		.emit(ClusterEvents.ToServer.PEEK, this.room);
};

MasterSlaveCluster.prototype.joinCluster = function (joiningMode) {
	if (this.joined) {
		if (!ObjectEx.evaluateDelegate(this, this.shouldRejoinCluster)) {
			return this;
		}

		this.leaveCluster();
	}

	const serverConn = this._serverConnection;

	const _join = function () {
		const FromServer = ClusterEvents.FromServer;
		serverConn
			.off(FromServer.JOINED_CLUSTER)
			.off(FromServer.JOIN_FAILED)
			.off(FromServer.PEER_GONE)
			.off(FromServer.MASTER_JOINED)
			.on(FromServer.JOINED_CLUSTER, this._handleJoinedCluster.bind(this))
			.on(FromServer.JOIN_FAILED, this._handleJoinErrorCluster.bind(this))
			.on(FromServer.PEER_GONE, this._handleNodeGone.bind(this))
			.on(FromServer.MASTER_JOINED, this._handleMasterJoined.bind(this));

		serverConn.on.willDisconnect
			.add(this._handleServerWillDisconnect, this);

		serverConn.emit(
			ClusterEvents.ToServer.JOIN_CLUSTER,
			this.room,
			this.preferredConnectionMethod,
			joiningMode || ClusterJoiningMode.ANY
		);
	}.bind(this);

	if (this._serverConnection.connected) {
		_join();
	} else {
		const onConnect = function (error) {
			serverConn.on.connected.remove(onConnect, this);
			serverConn.on.connectedError.remove(onConnect, this);

			if (error) {
				this.on.joinedClusterError.dispatch(error);
			} else {
				_join();
			}
		};

		serverConn.on.connected.add(onConnect, this);
		serverConn.on.connectedError.add(onConnect, this);
		serverConn.connect();
	}

	return this;
};

MasterSlaveCluster.prototype.sendData = function (data, options) {
	options = options || {};

	if (!this.joined) {
		throw new LivvclibError('not part of the cluster', 21);
	}

	if (!options.force && !this.allowsSending) {
		throw new LivvclibError('node not allowed to send data', 22);
	}

	const dataPackage = {
		id: this._packageId++,
		data: data,
		sender: this._serverConnection.id
	};

	if (options.recipient) {
		dataPackage.recipient = options.recipient;
		this._nodes[options.recipient].sendDataPackage(dataPackage);
	} else {
		const sendFunction = this.nodeType === NodeType.SLAVE
			? function (node) {
				dataPackage.recipient = node.otherNode.id;
				node.sendDataPackage(dataPackage);
			}
			: function (node) {
				node.sendDataPackage(dataPackage);
			};

		for (var nodeId in this._nodes) {
			sendFunction(this._nodes[nodeId]);
		}
	}

	return this;
};

MasterSlaveCluster.prototype.leaveCluster = function (dontSignal) {
	if (!this.joined) {
		return;
	}

	this._nodeConnector.unregisterFromConnectionEvents();

	const FromServer = ClusterEvents.FromServer;
	this._serverConnection
		.off(FromServer.JOINED_CLUSTER)
		.off(FromServer.JOIN_FAILED)
		.off(FromServer.PEER_GONE);
	this._serverConnection.on.willDisconnect
		.remove(this._handleServerWillDisconnect, this);

	for (var nodeId in this._nodes) {
		this._disconnectNode(nodeId);
	}

	if (!dontSignal) {
		this._serverConnection.emit(ClusterEvents.ToServer.QUIT_CLUSTER);
	}

	this._nodeType = undefined;

	this.on.leftCluster.dispatch();

	return this;
};

MasterSlaveCluster.prototype._disconnectNode = function (nodeId) {
	const connection = this._nodes[nodeId];
	if (connection) {
		const on = connection.disconnect().on;
		on.objectReceived.remove(this._handleObjectData, this);
		on.primitiveDataReceived
			.remove(this._handlePrimitiveData, this);
		on.dataChannelStatusChanged
			.remove(this._handleDataChannelsStatus, this);

		delete this._nodes[nodeId];
	}
};

// luizssb: server events handlers
MasterSlaveCluster.prototype._handleJoinedCluster = function (
	data, masterNode
) {
	if (masterNode && masterNode.id === data.sender) { // luizssb: i.e. i am the master
		this._nodeType = NodeType.MASTER;
		this._nodeConnector.registerForConnectionEvents();
		this.on.joinedCluster.dispatch(data.numberOfParticipants, masterNode);
		this.on.readyInCluster.dispatch();
	} else {
		this._nodeType = NodeType.SLAVE;
		this.on.joinedCluster.dispatch(data.numberOfParticipants, masterNode);

		if (masterNode) {
			this._nodeConnector.connectToNode(masterNode);
		}
	}
};

MasterSlaveCluster.prototype._handleMasterJoined = function (
	data, masterNode
) {
	this.on.masterJoined.dispatch(masterNode);
	this._nodeConnector.connectToNode(masterNode);
};

MasterSlaveCluster.prototype._handleJoinErrorCluster = function (data, error) {
	this.on.joinedClusterError.dispatch(error);
};

MasterSlaveCluster.prototype._handleNodeGone = function (data, didCloseRoom) {
	if (didCloseRoom) {
		this.leaveCluster(true);
	}
};

MasterSlaveCluster.prototype._handleServerWillDisconnect = function (sender) {
	this.leaveCluster();
};

// luizssb: node related handlers
MasterSlaveCluster.prototype._handleNodeConnect = function (connection) {
	this._nodes[connection.otherNode.id] = connection;

	const on = connection.on;
	on.dataChannelStatusChanged.add(this._handleDataChannelsStatus, this);
	on.objectReceived.add(this._handleObjectData, this);
	on.primitiveDataReceived.add(this._handlePrimitiveData, this);

	if (this.nodeType === NodeType.SLAVE) {
		this.on.readyInCluster.dispatch();
	}

	this.on.nodeConnected.dispatch(connection.otherNode);
};

MasterSlaveCluster.prototype._handleNodeConnectError = function (node, error) {
	this.on.nodeConnectedError.dispatch(node, error);
};

MasterSlaveCluster.prototype._handleDataChannelsStatus = function (
	conn, canSend, canReceive
) {
	if (!conn.ready) {
		this._disconnectNode(conn.otherNode.id);
		this.on.nodeConnectedError.dispatch(conn.otherNode);
	}
};

MasterSlaveCluster.prototype._handleObjectData = function (conn, data) {
	this.on.objectReceived.dispatch(data);
};

MasterSlaveCluster.prototype._handlePrimitiveData = function (conn, data) {
	this.on.primitiveDataReceived.dispatch(data);
};

/**
 * Callback/boolean to determine what should happen in case joinCluster() is invoked but this instance is already part of the cluster.
 * @param {MasterSlaveCluster} sender The instance that is trying to connect.
 * @return {boolean} True if the instance should leave the cluster and join again, false otherwise.
 */
MasterSlaveCluster.prototype.shouldRejoinCluster = true;

module.exports = MasterSlaveCluster;
