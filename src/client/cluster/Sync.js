const ClusterDataEventManager = require('./ClusterDataEventManager');
const SyncType = require('./SyncType');
const ObjectEx = require('../../utils/ObjectEx');
const WatchJS = require('melanke-watchjs');

/**
 * @author luizssb
 */
function Sync (clusterAccess, name) {
	this._name = name;
	this._trackedObjects = {};

	this._eventManager = new ClusterDataEventManager(clusterAccess);

	if (this._eventManager.canListen) {
		this._eventManager
			.on(
				SyncType.ATTRIBUTE_CHANGE,
				this._handleRemoteAttributeChange.bind(this)
			)
			.on(
				SyncType.METHOD_INVOCATION,
				this._handleRemoteMethodInvocation.bind(this)
			);
	}
}

Sync.prototype.trackAllAttributes = function (obj, setter) {
	if (!ObjectEx.isFunction(setter)) {
		setter = obj[setter] || null;
	}

	const key = this._newKey();

	this._trackedObjects[key] = {
		object: obj,
		setter: setter
	};

	this._start();

	return this;
};

Sync.prototype.trackAttribute = function (obj, attributeName, setter) {
	if (!ObjectEx.isFunction(setter)) {
		setter = obj[setter] || null;
	}

	const key = attributeName;

	this._trackedObjects[key] = {
		object: obj,
		attributeName: attributeName,
		setter: setter
	};

	this._start();

	return this;
};

Sync.prototype._trackAttribute = function (key, obj, attributeName) {
	if (!this._trackedObjects[key].watcher) {
		this._trackedObjects[key].watcher = function (
			attr, action, newValue, oldValue
		) {
			const syncData = {
				name: this._name,
				key: key,
				attributeName: attr,
				newValue: newValue,
				oldValue: oldValue
			};
			this._eventManager.forceEmit(SyncType.ATTRIBUTE_CHANGE, syncData);
		}.bind(this);

		if (attributeName) {
			WatchJS.watch(obj, attributeName, this._trackedObjects[key].watcher);
		} else {
			WatchJS.watch(obj, this._trackedObjects[key].watcher);
		}
	}
};

Sync.prototype.trackMethod = function (obj, methodName, setter) {
	const key = methodName;
	this._trackedObjects[key] = {
		object: obj,
		methodName: methodName,
		setter: setter
	};

	this._start();

	return this;
};

Sync.prototype._trackMethod = function (key, obj, methodName) {
	const $this = this;
	const replacementName = '__' + methodName;

	if (!obj[replacementName]) {
		obj[replacementName] =
			this._trackedObjects[key].watcher = obj[methodName];

		obj[methodName] = function () {
			const syncData = {
				name: $this._name,
				key: key,
				arguments: Array.prototype.slice.call(arguments)
			};
			$this._eventManager.forceEmit(SyncType.METHOD_INVOCATION, syncData);
			this[replacementName].apply(this, arguments);
		};
	}
};

Sync.prototype._start = function () {
	this.setStatus(true, this._eventManager.canEmit);
};

Sync.prototype.setStatus = function (listens, emits) {
	this[listens ? 'resumeListening' : 'pauseListening']();
	this[emits ? 'resumeEmitting' : 'pauseEmitting']();
};

Sync.prototype.resumeListening = function () {
	this._eventManager.resumeListening();
	return this;
};

Sync.prototype.pauseListening = function () {
	this._eventManager.pauseListening();
	return this;
};

Sync.prototype.resumeEmitting = function () {
	for (var key in this._trackedObjects) {
		const element = this._trackedObjects[key];

		if (element.methodName) {
			this._trackMethod(key, element.object, element.methodName);
		} else {
			this._trackAttribute(key, element.object, element.attributeName);
		}
	}

	return this;
};

Sync.prototype.pauseEmitting = function () {
	for (var key in this._trackedObjects) {
		const element = this._trackedObjects[key];

		if (element.methodName) {
			this._stopTrackingMethod(key, element.object, element.methodName);
		} else {
			this._stopTrackingAttribute(
				key, element.object, element.attributeName
			);
		}
	}

	return this;
};

Sync.prototype.stopTrackingAttribute = function (obj, attributeName, _key) {
	if (!_key) {
		for (var key in this._trackedObjects) {
			const element = this._trackedObjects[key];
			if (element.object === obj &&
				element.attributeName === attributeName) {
				_key = key;
				break;
			}
		}
	}

	if (_key) {
		this._stopTrackingAttribute(_key, obj, attributeName);
		delete this._trackedObjects[key];
	}

	this._stopIfOk();

	return this;
};

Sync.prototype.stopTrackingAllAttributes = function (obj, _key) {
	if (!_key) {
		for (var key in this._trackedObjects) {
			if (this._trackedObjects[key].object === obj) {
				_key = key;
				break;
			}
		}
	}

	if (_key) {
		this._stopTrackingAttribute(_key, obj);
		delete this._trackedObjects[key];
	}

	this._stopIfOk();

	return this;
};

Sync.prototype._stopTrackingAttribute = function (key, obj, attributeName) {
	if (this._trackedObjects[key].watcher) {
		try {
			if (attributeName) {
				WatchJS.unwatch(
					obj, attributeName, this._trackedObjects[key].watcher
				);
			} else {
				WatchJS.unwatch(obj, this._trackedObjects[key].watcher);
			}
		} catch (e) {}

		delete this._trackedObjects[key].watcher;
	}
};

Sync.prototype.stopTrackingMethod = function (obj, methodName, _key) {
	if (!_key) {
		for (var key in this._trackedObjects) {
			const element = this._trackedObjects[key];
			if (element.object === obj &&
				element.methodName === methodName) {
				_key = key;
				break;
			}
		}
	}

	if (_key) {
		this._stopTrackingMethod(key, obj, methodName);
		delete this._trackedObjects[_key];
	}

	this._stopIfOk();

	return this;
};

Sync.prototype._stopTrackingMethod = function (key, obj, methodName) {
	const replacementName = '__' + methodName;

	if (obj[replacementName]) {
		obj[methodName] = obj[replacementName];
		delete obj[replacementName];
	}
};

Sync.prototype.stopTracking = function (obj) {
	for (var key in this._trackedObjects) {
		const element = this._trackedObjects[key];

		if (element.object === obj) {
			if (element.methodName) {
				this.stopTrackingMethod(obj, element.methodName, key);
			} else if (element.attributeName) {
				this.stopTrackingAttribute(obj, element.attributeName, key);
			} else {
				this.stopTrackingAllAttributes(obj, key);
			}
		}
	}

	return this;
};

Sync.prototype.stopAllTracking = function () {
	for (var key in this._trackedObjects) {
		this.stopTracking(this._trackedObjects[key].object);
	}

	return this;
};

Sync.prototype._stopIfOk = function () {
	if (Object.keys(this._trackedObjects).length === 0) {
		this._eventManager.pauseListening();
	}
};

Sync.prototype._handleRemoteAttributeChange = function (data) {
	if (data.name === this._name && this._trackedObjects[data.key]) {
		const trackingData = this._trackedObjects[data.key];
		if (trackingData.setter) {
			trackingData.setter.call(
				null,
				trackingData.object,
				data.attributeName,
				data.newValue,
				data.oldValue
			);
		} else {
			trackingData.object[data.attributeName] = data.newValue;
		}
	}
};

Sync.prototype._handleRemoteMethodInvocation = function (data) {
	if (data.name === this._name && this._trackedObjects[data.key]) {
		const trackingData = this._trackedObjects[data.key];
		if (trackingData.setter) {
			trackingData.setter.call(
				null,
				trackingData.object,
				trackingData.methodName,
				data.arguments
			);
		} else {
			trackingData.object[trackingData.methodName].apply(
				trackingData.object,
				data.arguments
			);
		}
	}
};

Sync.prototype._keyIndex = 0;
Sync.prototype._newKey = function () {
	return this._keyIndex++;
};

module.exports = Sync;
