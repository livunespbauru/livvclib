const ObjectEx = require('../../utils/ObjectEx');

/**
 * @author luizssb
 */
function ClusterDataEventManager (clusterAccess) {
	this._clusterAccess = clusterAccess;
	this._events = {};
};

ObjectEx.defineProperties(ClusterDataEventManager.prototype, {
	canEmit: ObjectEx.readOnly(
		function () { return this._clusterAccess.allowsSending; }
	),
	canListen: ObjectEx.readOnly(
		function () { return this._clusterAccess.allowsReceiving; }
	)
});

ClusterDataEventManager.prototype.resumeListening = function () {
	if (!this._binding) {
		this._binding = this._clusterAccess.on.objectReceived
			.add(this._handleClusterData, this);
	}

	return this;
};

ClusterDataEventManager.prototype.pauseListening = function () {
	if (this._binding) {
		this._binding.detach();
		delete this._binding;
	}

	return this;
};

ClusterDataEventManager.prototype.on = function (dataEvent, handler) {
	return this.canListen ? this.forceOn(dataEvent, handler) : this;
};

ClusterDataEventManager.prototype.forceOn = function (dataEvent, handler) {
	this._events[dataEvent] = handler;
	return this;
};

ClusterDataEventManager.prototype.off = function (dataEvent) {
	delete this._events[dataEvent];
	return this;
};

ClusterDataEventManager.prototype.emit = function (eventName, data, recipient) {
	return this.canEmit
		? this._emit(eventName, data, {recipient: recipient})
		: this;
};

ClusterDataEventManager.prototype.forceEmit = function (eventName, data) {
	return this._emit(eventName, data, { force: true });
};

ClusterDataEventManager.prototype._emit = function (eventName, data, options) {
	const clusterData = {
		event: eventName,
		data: data
	};
	this._clusterAccess.sendData(clusterData, options);
	return this;
};

ClusterDataEventManager.prototype._handleClusterData = function (data) {
	if (data.event && this._events[data.event]) {
		this._events[data.event](data.data);
	}
};

module.exports = ClusterDataEventManager;
