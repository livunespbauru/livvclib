const NodeConnectionEvents = require('../../constants/NodeConnectionEvents');
const ObjectEx = require('../../utils/ObjectEx');

/**
 * @author luizssb
 */
function ClusterNodeConnector (serverConnection, nodeConnFactory) {
	this._serverConnection = serverConnection;
	this.connectionFactory = nodeConnFactory;

	this._connectionsOven = {};

	ObjectEx.defineEvents(
		this, [

			/**
			 * Called when it successfully connects to another node.
			 * @param {NodeConnection} conn Connection to the node.
			 */
			'nodeConnected',

			/**
			 * Called when it fails to connect to another node.
			 * @param {Object} node Node's data.
			 * @param {Object} error Error data.
			 */
			'nodeConnectedError'
		]
	);
}

ClusterNodeConnector.prototype.registerForConnectionEvents = function () {
	const FromServer = NodeConnectionEvents.FromServer;
	this._serverConnection
		.on(FromServer.RECEIVED_OFFER, this._handleOffer.bind(this))
		.on(FromServer.RECEIVED_ANSWER, this._handleAnswer.bind(this))
		.on(FromServer.ANSWER_FAILED, this._handleAnswerFailed.bind(this))
		.on(FromServer.RECEIVED_CANDIDATE, this._handleCandidate.bind(this));
};

ClusterNodeConnector.prototype.unregisterFromConnectionEvents = function () {
	const FromServer = NodeConnectionEvents.FromServer;
	this._serverConnection
		.off(FromServer.RECEIVED_OFFER)
		.off(FromServer.RECEIVED_ANSWER)
		.off(FromServer.ANSWER_FAILED)
		.off(FromServer.RECEIVED_CANDIDATE);
};

ClusterNodeConnector.prototype.connectToNode = function (node) {
	this.registerForConnectionEvents();

	this._connectionsOven[node.id] = this._newNodeConnection(node).connect();
};

ClusterNodeConnector.prototype._newNodeConnection = function (node) {
	const nodeConn = this.connectionFactory
		.makeConnection(node, this._serverConnection);

	const on = nodeConn.on;
	on.offerCreated.add(this._handleOfferCreated, this);
	on.offerCreatedError.add(this._handleOfferCreationError, this);
	on.answerCreated.add(this._handleAnswerCreated, this);
	on.answerCreatedError.add(this._handleAnswerCreationError, this);
	on.candidateDataReady.add(this._handleCandidateDataReady, this);
	on.dataChannelStatusChanged.add(this._handleDataChannelsStatus, this);

	return nodeConn;
};

// luizssb: node connection server events handlers
ClusterNodeConnector.prototype._handleOffer = function (data, offer) {
	this._connectionsOven[data.sender] =
		this._newNodeConnection(data.nodeData).handleOffer(offer);
};

ClusterNodeConnector.prototype._handleAnswer = function (data, answer) {
	this._connectionsOven[data.sender].handleAnswer(answer);
};

ClusterNodeConnector.prototype._handleAnswerFailed = function (data, error) {
	this._connectionsOven[data.sender].handleAnswerFailed(error);

	this.on.nodeConnectedError.dispatch(
		this._extractConnectionFromOven(data.sender).otherNode, error
	);
};

ClusterNodeConnector.prototype._handleCandidate = function (
	data, candidateData
) {
	if (this._connectionsOven[data.sender]) {
		this._connectionsOven[data.sender].handleCandidate(candidateData);
	}
};

// luizssb: NodeConnection's events handlers
ClusterNodeConnector.prototype._handleOfferCreated = function (conn, offer) {
	this._serverConnection.emit(
		NodeConnectionEvents.ToServer.OFFER_CONNECTION,
		conn.otherNode.id, offer
	);
};

ClusterNodeConnector.prototype._handleOfferCreationError = function (
	conn, error
) {
	this.on.nodeConnectedError.dispatch(
		this._extractConnectionFromOven(conn.otherNode.id).otherNode, error
	);
};

ClusterNodeConnector.prototype._handleAnswerCreated = function (conn, answer) {
	this._serverConnection.emit(
		NodeConnectionEvents.ToServer.ANSWER_OFFER,
		conn.otherNode.id, answer
	);
};

ClusterNodeConnector.prototype._handleAnswerCreationError = function (
	conn, error
) {
	this._serverConnection.emit(
		NodeConnectionEvents.ToServer.FAIL_ANSWER,
		conn.otherNode.id, error
	);

	this.on.nodeConnectedError.dispatch(
		this._extractConnectionFromOven(conn.otherNode.id).otherNode, error
	);
};

ClusterNodeConnector.prototype._handleCandidateDataReady = function (
	conn, candidateData
) {
	this._serverConnection.emit(
		NodeConnectionEvents.ToServer.CANDIDATE_PEER,
		conn.otherNode.id, candidateData
	);
};

ClusterNodeConnector.prototype._handleDataChannelsStatus = function (
	conn, canSend, canReceive
) {
	if (conn.ready) {
		this.on.nodeConnected
			.dispatch(this._extractConnectionFromOven(conn.otherNode.id));
	}
};

ClusterNodeConnector.prototype._extractConnectionFromOven = function (nodeId) {
	const node = this._connectionsOven[nodeId];

	const on = node.on;
	on.offerCreated.remove(this._handleOfferCreated, this);
	on.offerCreatedError.remove(this._handleOfferCreationError, this);
	on.answerCreated.remove(this._handleAnswerCreated, this);
	on.answerCreatedError.remove(this._handleAnswerCreationError, this);
	on.candidateDataReady.remove(this._handleCandidateDataReady, this);
	on.dataChannelStatusChanged
		.remove(this._handleDataChannelsStatus, this);

	delete this._connectionsOven[nodeId];

	return node;
};

module.exports = ClusterNodeConnector;
