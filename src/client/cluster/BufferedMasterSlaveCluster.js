const MasterSlaveCluster = require('./MasterSlaveCluster');
const BufferMode = require('./BufferMode');
const NodeType = require('../../constants/NodeType');
const LivvclibError = require('../../exceptions/LivvclibError');
const ObjectEx = require('../../utils/ObjectEx');
const RandomUtils = require('../../utils/RandomUtils');

const SYNC_BUFFER = '$syncBuffer$';

/**
 * @author luizssb
 */
function BufferedMasterSlaveCluster (serverConnection, room, nodeConnFactory) {
	MasterSlaveCluster.call(this, serverConnection, room, nodeConnFactory);

	this._bufferedData = {};

	/**
	 * Called when the instance finishes synchronizig buffered data.
	 */
	ObjectEx.defineEvents(this, ['bufferDataSynced']);
}

BufferedMasterSlaveCluster.prototype = Object.assign(
	Object.create(MasterSlaveCluster.prototype),
	{ constructor: BufferedMasterSlaveCluster }
);

BufferedMasterSlaveCluster.prototype.synchronizeBufferedData = function () {
	if (!this.joined) {
		throw new LivvclibError('not part of the cluster', 21);
	}

	if (this.nodeType === NodeType.MASTER) {
		throw new LivvclibError('master node does not need to synchronize', 23);
	}

	for (var id in this._nodes) {
		this._nodes[id].sendDataPackage({
			id: RandomUtils.string(),
			data: SYNC_BUFFER,
			recipient: id,
			sender: this._serverConnection.id
		});
		break;
	}
};

// Override
BufferedMasterSlaveCluster.prototype.sendData = function (data, options) {
	options = options || {};

	if (options.bufferMode === BufferMode.SINGLE && !options.bufferKey) {
		throw new LivvclibError('bufferKey parameter empty for BufferMode.SINGLE', 9);
	}

	MasterSlaveCluster.prototype.sendData.call(this, data, options);

	if (options.bufferMode) {
		if (!options.bufferKey) {
			options.bufferKey = RandomUtils.string();
		}

		this._bufferedData[options.bufferKey] = {
			data: data, time: new Date()
		};
	}
};

// Override
BufferedMasterSlaveCluster.prototype.leaveCluster = function (dontSignal) {
	this._bufferedData = {};
	MasterSlaveCluster.prototype.leaveCluster.call(this, dontSignal);
};

// Override
BufferedMasterSlaveCluster.prototype._handleObjectData = function (conn, data) {
	if (data.type === SYNC_BUFFER) {
		data.buffered.forEach(function (element, index) {
			if (ObjectEx.isObject(element)) {
				this.on.objectReceived.dispatch(element);
			} else {
				this.on.primitiveDataReceived.dispatch(element);
			}
		}.bind(this));

		this.on.bufferDataSynced.dispatch();
	}

	MasterSlaveCluster.prototype._handleObjectData.call(this, conn, data);
};

// Override
BufferedMasterSlaveCluster.prototype._handlePrimitiveData = function (
	conn, data
) {
	if (data === SYNC_BUFFER) {
		const bufferedData = Object.values(this._bufferedData)
			.sort(function (a, b) {
				return a.time - b.time;
			})
			.map(function (e) {
				return e.data;
			});

		this._nodes[conn.otherNode.id].sendDataPackage({
			id: RandomUtils.string(),
			data: { type: SYNC_BUFFER, buffered: bufferedData },
			recipient: conn.otherNode.id
		});
	}

	MasterSlaveCluster.prototype._handlePrimitiveData.call(this, conn, data);
};

module.exports = BufferedMasterSlaveCluster;
