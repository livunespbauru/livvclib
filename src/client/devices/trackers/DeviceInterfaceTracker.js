const Signal = require('Signals');

const LivvclibError = require('../../../exceptions/LivvclibError');
const ObjectEx = require('../../../utils/ObjectEx');

/**
 * @author luizssb
 */
function DeviceInterfaceTracker (deviceAccess, deviceEventsValidators) {
	this._deviceAccess = deviceAccess;
	if (this._deviceAccess.deviceInterface !== this.type) {
		throw LivvclibError(
			'device access must be for ' + this.type + ' device', 64
		);
	}
	this._deviceAccess.on.interfaceEvent.add(this._handleDeviceEvent, this);

	if (this.canSubscribe) {
		deviceAccess.on.subscribed.add(this._handleSubscribe, this);
		deviceAccess.on.subscribedError.add(this._handleSubscribeError, this);
	}

	this.broadcastsDataByDefault = true;
	this._deviceEventsValidators = deviceEventsValidators || {};

	this.on = {
		interface: makeEventRepo(this),
		subscribed: makeEventRepo(this),
		subscribedError: makeEventRepo(this)
	};

	function makeEventRepo (tracker) {
		return function (eventName) {
			tracker._getValidator(eventName);

			if (!this[eventName]) {
				this[eventName] = new Signal();
			}

			return this[eventName];
		}.bind({});
	}
}

ObjectEx.defineProperties(DeviceInterfaceTracker.prototype, {
	deviceAccess: ObjectEx.readOnly(function () { return this._deviceAccess; }),
	canSubscribe: ObjectEx.readOnly(function () {
		return ObjectEx.isFunction(this._deviceAccess.subscribeToInterfaceEvent);
	}),
	type: ObjectEx.readOnly(function () {
		return this._deviceAccess.deviceInterface;
	})
});

DeviceInterfaceTracker.prototype.subscribeToInterfaceEvent = function (
	eventName, broadcastsData
) {
	if (!this.canSubscribe) {
		this.on.subscribedError(eventName)
			.dispatch({message: 'device cannot subscribe to events'});
		return;
	}

	this._deviceAccess.subscribeToInterfaceEvent(
		eventName,
		ObjectEx.isBoolean(broadcastsData)
			? broadcastsData : this.broadcastsDataByDefault
	);
};

DeviceInterfaceTracker.prototype.unsubscribeFromInterfaceEvent = function (
	eventName
) {
	if (!this.canSubscribe) {
		throw new LivvclibError('device cannot subscribe to events', 52);
	}

	this._deviceAccess.unsubscribeFromInterfaceEvent(eventName);
};

DeviceInterfaceTracker.prototype._getValidator = function (eventName) {
	const validator = this._deviceEventsValidators[eventName];

	if (!validator) {
		throw new LivvclibError(
			'Tracker ' + this.type + ' has no validator for event ' + eventName,
			60
		);
	}

	return validator;
};

DeviceInterfaceTracker.prototype._handleDeviceEvent = function (
	eventName, eventData
) {
	const validator = this._getValidator(eventName);

	if (!validator(eventData)) {
		throw new LivvclibError(
			'Tracker ' + this.type + ' can not handle event ' + eventName +
				' with data ' + eventData,
			65
		);
	}

	this.on.interface(eventName).dispatch(eventData);
};

DeviceInterfaceTracker.prototype._handleSubscribe = function (eventName) {
	this.on.subscribed(eventName).dispatch();
};

DeviceInterfaceTracker.prototype._handleSubscribeError = function (
	eventName, error
) {
	this.on.subscribedError(eventName).dispatch();
};

module.exports = DeviceInterfaceTracker;
