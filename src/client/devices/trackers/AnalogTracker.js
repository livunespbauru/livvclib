const DeviceInterfaceTracker = require('./DeviceInterfaceTracker');
const AnalogEvents = require('../../../constants/AnalogEvents');
const DeviceInterface = require('../../../constants/DeviceInterface');
const ObjectEx = require('../../../utils/ObjectEx');

/**
 * @author luizssb
 */
function AnalogTracker (deviceAccess) {
	const validators = {};
	validators[AnalogEvents.CHANGE] = function (data) {
		return Array.isArray(data);
	};
	DeviceInterfaceTracker.call(this, deviceAccess, validators);
}

AnalogTracker.prototype = Object.assign(
	Object.create(DeviceInterfaceTracker.prototype),
	{ constructor: AnalogTracker }
);

Object.defineProperty(
	AnalogTracker.prototype, 'type', ObjectEx.readOnly(DeviceInterface.ANALOG)
);

module.exports = AnalogTracker;
