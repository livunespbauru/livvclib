const DeviceInterfaceTracker = require('./DeviceInterfaceTracker');
const ButtonEvents = require('../../../constants/ButtonEvents');
const DeviceInterface = require('../../../constants/DeviceInterface');
const ObjectEx = require('../../../utils/ObjectEx');

/**
 * @author luizssb
 */
function ButtonTracker (deviceAccess) {
	const validators = {};
	validators[ButtonEvents.CHANGE] = function (data) {
		return data &&
			data.buttonIndex !== undefined && data.state !== undefined;
	};
	validators[ButtonEvents.STATES] = Array.isArray;
	DeviceInterfaceTracker.call(this, deviceAccess, validators);
}

ButtonTracker.prototype = Object.assign(
	Object.create(DeviceInterfaceTracker.prototype),
	{ constructor: ButtonTracker }
);

Object.defineProperty(
	ButtonTracker.prototype, 'type', ObjectEx.readOnly(DeviceInterface.BUTTON)
);

module.exports = ButtonTracker;
