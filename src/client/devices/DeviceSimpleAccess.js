const DeviceEvents = require('../../constants/DeviceEvents');
const ObjectEx = require('../../utils/ObjectEx');

/**
 * @author luizssb
 */
function DeviceSimpleAccess (
	serverConnection, deviceName, deviceAddress, deviceInterface
) {
	this._deviceName = deviceName;
	this._deviceAddress = deviceAddress;
	this._deviceInterface = deviceInterface;
	this._events =
		DeviceEvents.forDevice(deviceName, deviceAddress, deviceInterface);
	this._serverConnection = serverConnection;

	this._connected = true;

	ObjectEx.defineEvents(
		this, [
			/**
			 * Called when access to the device was lost, presumably because the node that originally established the connection is now gone.
			 */
			'accessLost',

			/**
			 * Called when an event of the device's interface is triggered in the server.
			 * @param {string} event Event name.
			 * @param {Object|Array} data Event data.
			 */
			'interfaceEvent'
		]
	);
}

ObjectEx.defineProperties(DeviceSimpleAccess.prototype, {
	deviceName: ObjectEx.readOnly(function () { return this._deviceName; }),
	deviceAddress:
		ObjectEx.readOnly(function () { return this._deviceAddress; }),
	deviceInterface:
		ObjectEx.readOnly(function () { return this._deviceInterface; }),
	serverConnection:
		ObjectEx.readOnly(function () { return this._serverConnection; }),
	connected: ObjectEx.readOnly(function () { return this._connected; })
});

DeviceSimpleAccess.prototype.subscribeToServer = function () {
	this.serverConnection.on.connected.add(this._handleServerConnected, this);
	this.serverConnection.on.disconnected
		.add(this._handleServerDisconnected, this);

	const $this = this;

	this.serverConnection
		.on(
			this._events.FromServer.DEVICE_EVENT,
			function (data, deviceEvent, eventData) {
				$this.on.interfaceEvent.dispatch(deviceEvent, eventData);
			}
		)
		.on(
			this._events.FromServer.LOST,
			function (data) {
				$this.on.accessLost.dispatch();
			}
		);

	return this;
};

DeviceSimpleAccess.prototype.unsubscribeFromServer = function () {
	this.serverConnection
		.off(this._events.FromServer.DEVICE_EVENT)
		.off(this._events.FromServer.LOST);

	return this;
};

DeviceSimpleAccess.prototype._handleServerConnected = function () {
	this._connected = true;
};

DeviceSimpleAccess.prototype._handleServerDisconnected = function () {
	this.on.accessLost.dispatch();
	this._connected = false;
};

module.exports = DeviceSimpleAccess;
