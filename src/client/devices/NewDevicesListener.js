const DefaultInterfaceTrackerFactory =
	require('./DefaultInterfaceTrackerFactory');
const DeviceSimpleAccess = require('./DeviceSimpleAccess');
const DeviceEvents = require('../../constants/DeviceEvents');
const ObjectEx = require('../../utils/ObjectEx');

/**
 * @author luizssb
 */
function NewDevicesListener (serverConnection, interfaceTrackerFactory) {
	this._serverConnection = serverConnection;
	this._interfaceTrackerFactory = interfaceTrackerFactory ||
		DefaultInterfaceTrackerFactory;

	ObjectEx.defineEvents(
		this, [
			/**
			 * Called when a new device is accessed by the someone in the server.
			 * @param {DeviceSimpleAccess} deviceAccess Access to the new device. Call subscribeToServer() to receive data from it.
			 * @param {DeviceInterfaceTracker|undefined} tracker Abstraction of the device's interface for easier consumption. Returns undefined if the device's interface is unknown to the factory.
			 */
			'deviceAccessed'
		]
	);
}

NewDevicesListener.prototype.startListening = function () {
	const handleNewDevice = function (name, address, type) {
		const access = new DeviceSimpleAccess(
			this._serverConnection, name, address, type
		)
			.subscribeToServer();
		this.on.deviceAccessed
			.dispatch(access, this._interfaceTrackerFactory(access));
	}.bind(this);

	this._serverConnection
		.on(
			DeviceEvents.FromServer.NEW_DEVICE,
			function (data, name, address, type) {
				handleNewDevice(name, address, type);
			}
		)
		.on(
			DeviceEvents.FromServer.DEVICES_QUERY,
			function (data, devices) {
				devices.forEach(function (element) {
					handleNewDevice(
						element.name, element.address, element.deviceInterface
					);
				});
			}
		)
		.emit(DeviceEvents.ToServer.QUERY_DEVICES);
	return this;
};

NewDevicesListener.prototype.stopListening = function () {
	this._serverConnection.off(DeviceEvents.FromServer.NEW_DEVICE);
	return this;
};

module.exports = NewDevicesListener;
