const AnalogTracker = require('./trackers/AnalogTracker');
const ButtonTracker = require('./trackers/ButtonTracker');
const DeviceInterface = require('../../constants/DeviceInterface');

/**
 * @author luizssb
 */
module.exports = function (deviceAccess) {
	switch (deviceAccess.deviceInterface) {
	case DeviceInterface.ANALOG:
		return new AnalogTracker(deviceAccess);

	case DeviceInterface.BUTTON:
		return new ButtonTracker(deviceAccess);

	default:
		return undefined;
	}
};
