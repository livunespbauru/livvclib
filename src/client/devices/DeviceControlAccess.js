const DeviceSimpleAccess = require('./DeviceSimpleAccess');
const DeviceEvents = require('../../constants/DeviceEvents');
const ObjectEx = require('../../utils/ObjectEx');

/**
 * @author luizssb
 */
function DeviceControlAccess (
	serverConnection, deviceName, deviceAddress, deviceInterface
) {
	DeviceSimpleAccess.call(
		this, serverConnection, deviceName, deviceAddress, deviceInterface
	);

	ObjectEx.defineEvents(
		this, [
			/**
			 * Called when the server successfully connects to the device.
			 */
			'connected',

			/**
			 * Called when the server fails to connect to the device.
			 * @param {Object} error Error data.
			 */
			'connectedError',

			/**
			 * Called when the server successfully subscribes to an event of the device.
			 * @param {string} event Name of the device's event.
			 */
			'subscribed',

			/**
			 * Called when the server fails to subscribe to an event of the device.
			 * @param {string} event Name of the device's event.
			 * @param {Object} error Error data.
			 */
			'subscribedError'
		]
	);

	this._connected = false;
}

DeviceControlAccess.prototype = Object.assign(
	Object.create(DeviceSimpleAccess.prototype),
	{ constructor: DeviceControlAccess }
);

DeviceControlAccess.prototype.connect = function () {
	if (this._connected) {
		if (!ObjectEx.evaluateDelegate(this, this.shouldReconnect)) {
			return;
		}

		this.disconnect();
	}

	const doConnectDevice = function () {
		this.subscribeToServer();
		this.serverConnection.emit(
			DeviceEvents.ToServer.CONNECT,
			this.deviceName, this.deviceAddress, this.deviceInterface,
			this.broadcastsData
		);
	}.bind(this);

	if (this.serverConnection.connected) {
		doConnectDevice();
	} else {
		const serverOn = this.serverConnection.on;
		const onConnect = function (error) {
			serverOn.connected.remove(onConnect, this);
			serverOn.connectedError.remove(onConnect, this);

			if (error) {
				this.on.connectedError.dispatch(error);
			} else {
				doConnectDevice();
			}
		}.bind(this);

		serverOn.connected.add(onConnect, this);
		serverOn.connectedError.add(onConnect, this);

		this.serverConnection.connect();
	}

	return this;
};

DeviceControlAccess.prototype.disconnect = function () {
	if (!this.connected) {
		return;
	}

	this.unsubscribeFromServer();
	this.serverConnection.emit(
		DeviceEvents.ToServer.DISCONNECT,
		this.deviceName, this.deviceAddress, this.deviceInterface
	);
	this._connected = false;

	return this;
};

DeviceControlAccess.prototype.subscribeToInterfaceEvent = function (
	eventName, broadcastsData
) {
	if (!this.connected) {
		this.on.subscribedError.dispatch(
			eventName, {message: 'can not subscribe if not connected'}
		);
		return;
	}

	if (!ObjectEx.isBoolean(broadcastsData)) {
		broadcastsData = true;
	}

	this.serverConnection.emit(
		this._events.ToServer.SUBSCRIBE_TO_EVENT, eventName, broadcastsData
	);

	return this;
};

DeviceControlAccess.prototype.unsubscribeFromInterfaceEvent = function (
	eventName
) {
	if (this.connected) {
		this.serverConnection.emit(
			this._events.ToServer.UNSUBSCRIBE_FROM_EVENT, eventName
		);
	}

	return this;
};

// Override
DeviceControlAccess.prototype.subscribeToServer = function () {
	DeviceSimpleAccess.prototype.subscribeToServer.call(this);

	const $this = this;

	this._serverConnection
		.on(
			this._events.FromServer.CONNECT,
			function (data) {
				$this._connected = true;
				$this.on.connected.dispatch();
			}
		)
		.on(
			this._events.FromServer.CONNECT_ERROR,
			function (data, error) {
				$this.unsubscribeFromServer();
				$this.on.connectedError.dispatch(error);
			}
		)
		.on(
			this._events.FromServer.SUBSCRIBE,
			function (data, event) {
				$this.on.subscribed.dispatch(event);
			}
		)
		.on(
			this._events.FromServer.SUBSCRIBE_ERROR,
			function (data, event, error) {
				$this.on.subscribedError.dispatch(event, error);
			}
		);

	return this;
};

// Override
DeviceControlAccess.prototype.unsubscribeFromServer = function () {
	DeviceSimpleAccess.prototype.unsubscribeFromServer.call(this);

	this._serverConnection
		.off(this._events.FromServer.CONNECT)
		.off(this._events.FromServer.CONNECT_ERROR)
		.off(this._events.FromServer.SUBSCRIBE)
		.off(this._events.FromServer.SUBSCRIBE_ERROR);

	return this;
};

/**
 * Callback/boolean to determine what should happen in case connect() is invoked
 * but the connection is already established.
 * @param {DeviceControlAccess} sender Instance trying to reconnect.
 * @return {boolean} True if the instance should disconnect and the connect again, false otherwise
 */
DeviceControlAccess.prototype.shouldReconnect = true;

module.exports = DeviceControlAccess;
