const Sync = require('../cluster/Sync');
const RotationSyncSetter = require('../cluster/RotationSyncSetter');
const ObjectEx = require('../../utils/ObjectEx');
const THREE = require('three');

function _cameraNameGenerator () {
	if (typeof _cameraNameGenerator.counter === 'undefined') {
		_cameraNameGenerator.counter = 0;
	}

	return 'DistributedCamera_' + (_cameraNameGenerator.counter++);
}

/**
 * @author luizssb
 */
function DistributedCamera (fov, aspect, near, far) {
	THREE.PerspectiveCamera.call(this, fov, aspect, near, far);

	this._screen = null;
	this._projectionPlane = null;
}

DistributedCamera.prototype = Object.assign(
	Object.create(THREE.PerspectiveCamera.prototype),
	{ constructor: DistributedCamera }
);

Object.defineProperty(
	DistributedCamera.prototype, 'projectionPlane',
	ObjectEx.readOnly(function () { return this._projectionPlane; })
);

Object.defineProperty(
	DistributedCamera.prototype, 'screen',
	{
		get: function () { return this._screen; },
		set: function (value) {
			this._screen = value;
			this.updateProjectionMatrix();
		},
		enumerable: true,
		configurable: true
	}
);

DistributedCamera.prototype.calculateProjectionMatrix = function (
	screen, projectionPlane, nearPlane, farPlane
) {
	const pa = projectionPlane.localToWorld(screen.pA.clone());
	const pb = projectionPlane.localToWorld(screen.pB.clone());
	const pc = projectionPlane.localToWorld(screen.pC.clone());
	const pe = screen.pE.clone();

	const vr = pb.clone().sub(pa).normalize();
	const vu = pc.clone().sub(pa).normalize();
	const vn = vr.clone().cross(vu).negate().normalize();

	const va = pa.clone().sub(pe);
	const vb = pb.clone().sub(pe);
	const vc = pc.clone().sub(pe);

	const d = -va.dot(vn);
	const l = vr.dot(va) * nearPlane / d;
	const r = vr.dot(vb) * nearPlane / d;
	const b = vu.dot(va) * nearPlane / d;
	const t = vu.dot(vc) * nearPlane / d;

	const n = nearPlane;
	const f = farPlane;
	this.p = this.p || new THREE.Matrix4();

	/* eslint-disable no-multi-spaces */
	this.p.set(
		2 * n / (r - l), 0,               (r + l) / (r - l), 0,
		0,               2 * n / (t - b), (t + b) / (t - b), 0,
		0,               0,               (f + n) / (n - f), 2 * f * n / (n - f),
		0,               0,               -1,                0
	);
	/* eslint-enable no-multi-spaces */

	return this.p;
};

DistributedCamera.prototype.updateProjectionMatrix_threejs =
	THREE.PerspectiveCamera.prototype.updateProjectionMatrix;

DistributedCamera.prototype.updateProjectionMatrix = function () {
	if (this.screen) {
		// luizssb: we ignore the 'projectionPlane' getter below, what
		// allows this method to be attached to cameras that cannot be an
		// instance of DistributedCamera (e.g. NGL viewer's camera)

		if (!this._projectionPlane) {
			this._projectionPlane = new THREE.Mesh(
				new THREE.PlaneGeometry(10, 10),
				new THREE.MeshBasicMaterial({
					transparent: true,
					opacity: 0
				})
			);

			this.add(this._projectionPlane);
			this._projectionPlane.position.z = -5;
		}

		this.projectionMatrix = this.calculateProjectionMatrix(
			this.screen, this._projectionPlane, this.near, this.far
		);
	} else {
		this.updateProjectionMatrix_threejs();
	}
};

DistributedCamera.prototype.attachToCluster = function (
	clusterAccess, nameGenerator
) {
	this.detachFromCluster();

	const baseName = (nameGenerator || _cameraNameGenerator)();
	this._sync = new Sync(clusterAccess, baseName)
		.trackAllAttributes(this.position)
		.trackAllAttributes(this.rotation, RotationSyncSetter)
		.trackAttribute(this, 'filmGauge')
		.trackAttribute(this, 'filmOffset')
		.trackAttribute(this, 'focus')
		.trackAttribute(this, 'zoom')
		.trackAttribute(this, 'near')
		.trackAttribute(this, 'far')
		.trackAttribute(this, 'fov')
		.trackAttribute(this, 'aspect');
};

DistributedCamera.prototype.detachFromCluster = function () {
	if (this._sync) {
		this._sync.stopTracking(this);
		this._sync = null;
	}
};

module.exports = DistributedCamera;
