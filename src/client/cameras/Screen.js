const THREE = require('three');

/**
 * @author luizssb
 */
function Screen (pA, pB, pC, pE) {
	if (pA instanceof Array) {
		Screen.apply(this, pA);
	} else {
		this.pA = pA instanceof THREE.Vector3 ? pA : new THREE.Vector3(-8, -4, 0);
		this.pB = pB instanceof THREE.Vector3 ? pB : new THREE.Vector3(8, -4, 0);
		this.pC = pC instanceof THREE.Vector3 ? pC : new THREE.Vector3(-8, 5, 0);
		this.pE = pE instanceof THREE.Vector3 ? pE : new THREE.Vector3(0, 0, -5);
	}
};

Screen.prototype.setPoint = function (index, vector) {
	switch (index) {
	case 0:
		this.pA = vector;
		break;
	case 1:
		this.pB = vector;
		break;
	case 2:
		this.pC = vector;
		break;
	case 3:
		this.pE = vector;
		break;
	}

	return this;
};

Screen.prototype.setPa = function (vector) {
	return this.setPoint(0, vector);
};

Screen.prototype.setPb = function (vector) {
	return this.setPoint(1, vector);
};

Screen.prototype.setPc = function (vector) {
	return this.setPoint(2, vector);
};

Screen.prototype.setPe = function (vector) {
	return this.setPoint(3, vector);
};

module.exports = Screen;
