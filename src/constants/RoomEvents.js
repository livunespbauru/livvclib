/**
 * @author luizssb
 */
module.exports = {
	ToServer: {
		JOIN: 'room:join',
		LEAVE: 'room:leave'
	}
};
