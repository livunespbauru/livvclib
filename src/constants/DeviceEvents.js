/**
 * @author luizssb
 */

module.exports = {
	forDevice: function (device, address, type) {
		var _makeEventName = function (event) {
			return 'device:' + [event, device, address, type].join(':');
		};
		return {
			ToServer: {
				SUBSCRIBE_TO_EVENT: _makeEventName('subscribe'),
				UNSUBSCRIBE_FROM_EVENT: _makeEventName('unsubscribe')
			},
			FromServer: {
				SUBSCRIBE: _makeEventName('subscribe'),
				SUBSCRIBE_ERROR: _makeEventName('subscribeError'),
				DEVICE_EVENT: _makeEventName('event'),
				CONNECT: _makeEventName('connect'),
				CONNECT_ERROR: _makeEventName('error'),
				LOST: _makeEventName('lost')
			}
		};
	},
	ToServer: {
		CONNECT: 'device',
		DISCONNECT: 'device:disconnect',
		QUERY_DEVICES: 'device:query'
	},
	FromServer: {
		NEW_DEVICE: 'device:new',
		DEVICES_QUERY: 'device:query'
	}
};
