/**
 * @author luizssb
 */
module.exports = {
	ToServer: {
		JOIN_CLUSTER: 'joinCluster',
		QUIT_CLUSTER: 'leave',
		PEEK_CLUSTER: 'peek'
	},
	FromServer: {
		JOINED_CLUSTER: 'joinCluster',
		JOIN_FAILED: 'joinClusterError',
		PEER_GONE: 'peerGone',
		MASTER_JOINED: 'masterJoin',
		PEEKED: 'peek'
	}
};
