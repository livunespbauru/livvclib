/**
 * @author luizssb
 */
module.exports = {
	ToServer: {
		OFFER_CONNECTION: 'offer',
		ANSWER_OFFER: 'answer',
		FAIL_ANSWER: 'answerFailed',
		CANDIDATE_PEER: 'candidate'
	},
	FromServer: {
		RECEIVED_OFFER: 'offer',
		RECEIVED_ANSWER: 'answer',
		ANSWER_FAILED: 'answerFailed',
		RECEIVED_CANDIDATE: 'candidate'
	}
};
