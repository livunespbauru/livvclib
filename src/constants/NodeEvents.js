/**
 * @author luizssb
 */
module.exports = {
	FromServer: {
		RECEIVED_DATA: 'node:data',
		NODE_GONE: 'node:gone'
	},
	ToServer: {
		SEND_DATA: 'node:data',
		QUIT: 'node:quit'
	}
};
