/**
 * @author luizssb
 */
module.exports = {
	ANY: 7,
	SLAVE_NOW: 1,
	SLAVE_WAIT: 2,
	MASTER: 4
};
