const ObjectEx = require('../../utils/ObjectEx');

/**
 * @author luizssb
 */
function ClientConnection (parentManager) {
	this._manager = parentManager;

	this.on = this.on.bind(this);
	ObjectEx.defineEvents(
		this, [
			/**
			 * Called when a request will be made and the default parameters must be customized.
			 * @param {ClientConnection} sender Instance emitting data.
			 * @param {Object} data The default parameters that can be customized.
			 */
			'willEmit',

			/**
			 * Called when the client joins a room in the server.
			 * @param {ClientConnection} sender Instance that joined the room.
			 */
			'joinedRoom',

			/**
			 * Called when the client leaves a room in the server.
			 * @param {ClientConnection} sender Instance that left the room.
			 * @param {string} room Name of the room that was left.
			 */
			'leftRoom'
		]
	);
}

ObjectEx.defineProperties(
	ClientConnection.prototype,
	{
		manager: ObjectEx.readOnly(function () { return this._manager; }),
		id: ObjectEx.readOnly(ObjectEx.abstractMethod),
		room: ObjectEx.readOnly(ObjectEx.abstractMethod),
		connected: ObjectEx.readOnly(ObjectEx.abstractMethod)
	}
);

/**
 * Registers a callback to be executed whenever some client sends a message.
 * @param {string} event Name of the event that triggers the callback;
 * @param {Function} callback Callback to be executed when the event is triggered.
 * @return {ClientsConnection} Itself.
 */
ClientConnection.prototype.on = ObjectEx.abstractMethod;

/**
 * Deregisters a callback of an evet
 * @param {string} event The event that triggers the callback.
 * @return {ClientsConnection} Itself.
 */
ClientConnection.prototype.off = ObjectEx.abstractMethod;

ClientConnection.prototype.emit = function (event) {
	return this.emitWithParams.apply(
		this, [event, {}].concat(Array.prototype.slice.call(arguments, 1))
	);
};

ClientConnection.prototype.emitWithParams = function (event, params) {
	const defaultParams = {};
	this.on.willEmit.dispatch(this, defaultParams);
	Object.assign(defaultParams, params);

	return this._emit.apply(
		this,
		[event, defaultParams].concat(Array.prototype.slice.call(arguments, 2))
	);
};

/**
 * Sends a message to a client.
 * @param {string} event The event to trigger on the clients.
 * @param {Object...} [] Data to send to the clients.
 * @return {ClientConnection} Itself.
 */
ClientConnection.prototype._emit = ObjectEx.abstractMethod;

/**
 * Makes the client connection join an specific room.
 * Client may be connected to only one room at a time.
 * @param {string} room The Room to join.
 * @return {ClientConnection} Itself.
 */
ClientConnection.prototype.join = ObjectEx.abstractMethod;

/**
 * Leaves the room.
 * @return {ClientConnection} Itself.
 */
ClientConnection.prototype.leave = ObjectEx.abstractMethod;

/**
 * Terminates connection.
 * @return {ClientConnection} Itself.
 */
ClientConnection.prototype.disconnect = ObjectEx.abstractMethod;

ClientConnection.prototype.broadcast = function (event) {
	const defaultParams = {};
	this.on.willEmit.dispatch(this, defaultParams);
	return this._broadcast.apply(
		this,
		[event, defaultParams].concat(Array.prototype.slice.call(arguments, 1))
	);
};

/**
 * Broadcasts a message to all nodes in the room this client is in, excluding it.
 * @param {string} event The event to trigger on the clients.
 * @param {Object...} [] Data to send to the clients.
 * @return {ClientConnection} Itself.
 */
ClientConnection.prototype._broadcast = ObjectEx.abstractMethod;

module.exports = ClientConnection;
