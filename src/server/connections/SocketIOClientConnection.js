const ClientConnection = require('./ClientConnection');
const ObjectEx = require('../../utils/ObjectEx');

function SocketIOClientConnection (socket, parentManager) {
	ClientConnection.call(this, parentManager);
	this.socket = socket;
}

SocketIOClientConnection.prototype = Object.assign(
	Object.create(ClientConnection.prototype),
	{ constructor: SocketIOClientConnection }
);

ObjectEx.defineProperties(
	SocketIOClientConnection.prototype,
	{
		id: ObjectEx.readOnly(function () { return this.socket.id; }),
		room: ObjectEx.readOnly(function () { return this._room; }),
		connected:
			ObjectEx.readOnly(function () { return this.socket.connected; })
	}
);

// Override
SocketIOClientConnection.prototype.on = function (event, callback) {
	this.socket.on(event, callback);
	return this;
};

// Override
SocketIOClientConnection.prototype.off = function (event) {
	this.socket.removeAllListeners(event);
	return this;
};

// Override
SocketIOClientConnection.prototype._emit = function (event) {
	this.socket.emit.apply(this.socket, Array.prototype.slice.call(arguments, 0));
	return this;
};

// Override
SocketIOClientConnection.prototype._broadcast = function (event) {
	const clients = this.socket.broadcast.to(this.room);
	clients.emit.apply(clients, Array.prototype.slice.call(arguments, 0));
};

// Override
SocketIOClientConnection.prototype.join = function (room) {
	if (this.room) {
		if (this.room === room) {
			return this;
		} else {
			throw new Error('client already in another room');
		}
	}

	this.socket.join(room);
	this._room = room;

	this.on.joinedRoom.dispatch(this);

	return this;
};

// Override
SocketIOClientConnection.prototype.leave = function () {
	const room = this._room;

	this.socket.leave(this.room);
	this._room = null;

	this.on.leftRoom.dispatch(room);

	return this;
};

// Override
SocketIOClientConnection.prototype.disconnect = function () {
	this.socket.disconnect();

	const room = this._room;
	this._room = null;
	this.on.leftRoom.dispatch(this, room);

	return this;
};

module.exports = SocketIOClientConnection;
