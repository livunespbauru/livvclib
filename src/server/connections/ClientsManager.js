const ObjectEx = require('../../utils/ObjectEx');

/**
 * @author luizssb
 */
function ClientsManager () {
	ObjectEx.defineEvents(
		this, [
			/**
			 * Called when a new client connects to the server.
			 * @param {ClientConnection} client Connection to the new client.
			 */
			'clientConnected',

			/**
			 * Called when a client disconnects from the server.
			 * @param {ClientConnection} client Connection to the client.
			 */
			'clientDisconnected',

			/**
			 * Called when data will be broadcast to all nodes in a room.
			 * @param {Object} data Data to be customized.
			 */
			'willBroadcast'
		]
	);
}

/**
 * Starts waiting for new clients to connect.
 * @return {ClientsManager} Itself.
 */
ClientsManager.prototype.start = ObjectEx.abstractMethod;

ClientsManager.prototype.broadcast = function (room, event) {
	const defaultParams = {};
	this.on.willBroadcast.dispatch(defaultParams);

	return this._broadcast.apply(
		this,
		[room, event, defaultParams]
			.concat(Array.prototype.slice.call(arguments, 2))
	);
};

/**
 * Broadcasts an event to all clients in a room.
 * @param {string} room The room to receive the broadcast.
 * @param {string} event The event to trigger on the clients.
 * @param {Object...} [] Data to send to the clients
 * @return {ClientsManager} Itself.
 */
ClientsManager.prototype._broadcast = ObjectEx.abstractMethod;

/**
 * Gets the number of clients.
 * @param {string} room Room in which the clients are.
 * @return {int} The number of clients in the room.
 */
ClientsManager.prototype.getNumberOfClients = ObjectEx.abstractMethod;

/**
 * Gets the connection to one of the clients.
 * @param {string} id The id of the client.
 * @return {ClientConnection} The connection to the client.
 */
ClientsManager.prototype.get = ObjectEx.abstractMethod;

module.exports = ClientsManager;
