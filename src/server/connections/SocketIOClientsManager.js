const ClientsManager = require('./ClientsManager');
const SocketIOClientConnection = require('./SocketIOClientConnection');

/**
 * @author luizssb
 */
function SocketIOClientsManager (io) {
	ClientsManager.call(this);

	this.io = io;
	this._clients = {};
}

SocketIOClientsManager.prototype = Object.assign(
	Object.create(ClientsManager.prototype),
	{ constructor: SocketIOClientsManager }
);

// Override
SocketIOClientsManager.prototype.start = function () {
	if (!this._started) {
		this.io.sockets.on('connection', function (socket) {
			const client = new SocketIOClientConnection(socket, this);
			this._clients[client.id] = client;
			this.on.clientConnected.dispatch(client);

			client.on('disconnect', function () {
				this.on.clientDisconnected.dispatch(client);
			}.bind(this));
		}.bind(this));
		this._started = true;
	}

	return this;
};

// Override
SocketIOClientsManager.prototype._broadcast = function (room) {
	const clients = this.io.sockets.in(room);
	clients.emit.apply(clients, Array.prototype.slice.call(arguments, 1));

	return this;
};

// Override
SocketIOClientsManager.prototype.getNumberOfClients = function (room) {
	return Object.keys(this.io.sockets.adapter.rooms[room].sockets).length;
};

// Override
SocketIOClientsManager.prototype.get = function (id) {
	return this._clients[id];
};

module.exports = SocketIOClientsManager;
