const ObjectEx = require('../../utils/ObjectEx');

/**
 * @author luizssb
 */
function InterfacesFactory () {}

/**
 * Creates an instance of a device interface.
 * @param {string} name Name of the device.
 * @param {string} address Address of the device.
 * @param {string} deviceInterface Interface of the device.
 * @return {IVRPNDeviceBinding} Device interface binding.
 */
InterfacesFactory.prototype.makeInterface = ObjectEx.abstractMethod;

/**
 * Create a looper for a device interface.
 * @param {IVRPNDeviceBinding} binding Device interface binding to be looped.
 * @return {IVRPNDeviceLooper} Looper for the device.
 */
InterfacesFactory.prototype.makeLooper = ObjectEx.abstractMethod;

module.exports = InterfacesFactory;
