const InterfacesFactory = require('./InterfacesFactory');
const DeviceInterface = require('../../constants/DeviceInterface');

/**
 * @author luizssb
 */
function DefaultInterfacesFactory (nativeLib) {
	InterfacesFactory.call(this);

	this.nativeLib = nativeLib;

	this._data = {};
	for (var deviceInterface in DeviceInterface) {
		this._data[DeviceInterface[deviceInterface]] = {
			class: DeviceInterface[deviceInterface][0].toUpperCase() +
				DeviceInterface[deviceInterface].substring(1) + 'RemoteDevice'
		};
	}
}

// Override
DefaultInterfacesFactory.prototype.makeInterface = function (
	name, address, deviceInterface
) {
	return new this.nativeLib[this._data[deviceInterface].class](name, address);
};

// Override
DefaultInterfacesFactory.prototype.makeLooper = function (deviceBinding) {
	return new this.nativeLib.AsyncWorkerDeviceLooper(deviceBinding);
};

module.exports = DefaultInterfacesFactory;
