const ClientEventsHandler = require('../events/ClientEventsHandler');
const DeviceEvents = require('../../constants/DeviceEvents');

function makeDeviceId (name, address, deviceInterface) {
	return [name, address, deviceInterface].join(':');
};

function eventGetterName (eventName) {
	return 'getOn' + eventName[0].toUpperCase() + eventName.substr(1);
};

function turnSerializable (obj) {
	if (obj instanceof Array) {
		return obj;
	}

	const serializable = {};

	for (var key in obj) {
		if (key.indexOf('get') === 0) {
			serializable[attrName(key)] = obj[key]();
		}
	}

	return serializable;

	function attrName (getterName) {
		getterName = getterName.substr(3);
		return getterName.charAt(0).toLowerCase() + getterName.slice(1);
	}
}

/**
 * @author luizssb
 */
function DeviceEventsHandler (interfaceFactory) {
	ClientEventsHandler.call(this);

	this._interfaceFactory = interfaceFactory;
	this._devicesPerClient = {};
}

DeviceEventsHandler.prototype = Object.assign(
	Object.create(ClientEventsHandler.prototype),
	{ constructor: DeviceEventsHandler }
);

// Override
DeviceEventsHandler.prototype.handleNewClient = function (client) {
	const devicesPerClient = this._devicesPerClient;
	devicesPerClient[client.id] = {};

	const factory = this._interfaceFactory;

	client.on(
		DeviceEvents.ToServer.CONNECT,
		function (data, name, address, deviceInterface) {
			console.log(
				DeviceEvents.ToServer.CONNECT, name, address, deviceInterface
			);

			const deviceId = makeDeviceId(name, address, deviceInterface);
			const handlerEvents =
				DeviceEvents.forDevice(name, address, deviceInterface);

			if (devicesPerClient[client.id][deviceId]) {
				client.emit(handlerEvents.FromServer.CONNECT);
				return;
			}

			const device =
				factory.makeInterface(name, address, deviceInterface);
			const looper = factory.makeLooper(device);

			const deviceData = devicesPerClient[client.id][deviceId] = {
				name: name,
				address: address,
				deviceInterface: deviceInterface,
				device: device,
				looper: looper,
				subscribedEvents: {},
				handlerEvents: handlerEvents
			};

			client.on(
				handlerEvents.ToServer.SUBSCRIBE_TO_EVENT,
				function (subscriptionData, eventName, broadcastsData) {
					console.log(
						handlerEvents.ToServer.SUBSCRIBE_TO_EVENT,
						eventName, broadcastsData
					);

					if (deviceData.subscribedEvents[eventName]) {
						client.emit(
							handlerEvents.FromServer.SUBSCRIBE, eventName
						);
						return;
					}

					if (!device[eventGetterName(eventName)]) {
						client.emit(
							handlerEvents.FromServer.SUBSCRIBE_ERROR,
							eventName,
							{ message: 'interface has no event ' + eventName }
						);
						return;
					}

					const event = device[eventGetterName(eventName)]();
					const result = event.registerCallback(
						client.id,
						function (eventData) {
							eventData = turnSerializable(eventData);

							if (broadcastsData && client.room) {
								client.manager.broadcast(
									client.room,
									handlerEvents.FromServer.DEVICE_EVENT,
									eventName, eventData
								);
							} else {
								client.emit(
									handlerEvents.FromServer.DEVICE_EVENT,
									eventName, eventData
								);
							}
						}
					);

					if (result === 0) {
						deviceData.subscribedEvents[eventName] = true;

						client.emit(
							handlerEvents.FromServer.SUBSCRIBE, eventName
						);

						if (!looper.isRunning()) {
							looper.start();
						}
					} else {
						client.emit(
							handlerEvents.FromServer.SUBSCRIBE_ERROR,
							eventName,
							{
								message: 'could not register callback for event',
								code: result
							}
						);
					}
				}
			)
				.on(
					handlerEvents.ToServer.UNSUBSCRIBE_FROM_EVENT,
					function (data, eventName) {
						console.log(
							handlerEvents.ToServer.UNSUBSCRIBE_FROM_EVENT, eventName
						);

						if (device[eventGetterName(eventName)]) {
							device[eventGetterName(eventName)]()
								.unregisterCallback(client.id);
							delete deviceData.subscribedEvents[eventName];
						} else {
							console.log('tried to unsubscribe from inexistent event');
						}
					}
				)
				.emit(handlerEvents.FromServer.CONNECT)
				.broadcast(
					DeviceEvents.FromServer.NEW_DEVICE,
					name, address, deviceInterface
				);
		}
	)
		.on(
			DeviceEvents.ToServer.DISCONNECT,
			function (data, name, address, deviceInterface) {
				console.log(
					DeviceEvents.ToServer.DISCONNECT,
					name, address, deviceInterface
				);

				this._killDevice(
					client, makeDeviceId(name, address, deviceInterface)
				);
			}.bind(this)
		)
		.on(
			DeviceEvents.ToServer.QUERY_DEVICES,
			function (data) {
				const result = [];

				for (var clientId in devicesPerClient) {
					for (var deviceId in devicesPerClient[clientId]) {
						const deviceData = devicesPerClient[clientId][deviceId];
						result.push({
							name: deviceData.name,
							address: deviceData.address,
							deviceInterface: deviceData.deviceInterface
						});
					}
				}

				client.emit(DeviceEvents.FromServer.DEVICES_QUERY, result);
			}
		);
};

// Override
DeviceEventsHandler.prototype.handleClientGone = function (client) {
	for (var identifier in this._devicesPerClient[client.id]) {
		this._killDevice(client, identifier);
	}
	delete this._devicesPerClient[client.id];
};

DeviceEventsHandler.prototype._killDevice = function (client, identifier) {
	const deviceData = this._devicesPerClient[client.id][identifier];

	if (!deviceData) {
		return;
	}

	deviceData.looper.stop();

	if (client.connected) {
		client.off(deviceData.handlerEvents.ToServer.SUBSCRIBE_TO_EVENT)
			.off(deviceData.handlerEvents.ToServer.UNSUBSCRIBE_FROM_EVENT);
	}

	for (var eventName in deviceData.subscribedEvents) {
		deviceData
			.device[eventGetterName(eventName)]().unregisterCallback(client.id);
		delete deviceData.subscribedEvents[eventName];
	}

	deviceData.device.disconnect();
	client.manager
		.broadcast(client.room, deviceData.handlerEvents.FromServer.LOST);
	delete this._devicesPerClient[client.id][identifier];
};

module.exports = DeviceEventsHandler;
