const RandomUtils = require('../utils/RandomUtils');
const defaultParams = require('./defaultParams');

/**
 * @author luizssb
 */
function WebClusterSupport (clientsManager) {
	this.tag = RandomUtils.string();
	this.clientsManager = clientsManager;
	this.clientEventsHandlers = [];
}

WebClusterSupport.prototype.setup = function () {
	this.clientsManager.start();

	this.clientsManager.on.clientConnected.add(
		function (client) {
			client.on.willEmit.add(defaultParams, this);

			this.clientEventsHandlers.forEach(function (handler) {
				handler.handleNewClient(client);
			});
		},
		this
	);

	this.clientsManager.on.clientDisconnected.add(
		function (client) {
			this.clientEventsHandlers.forEach(function (handler) {
				handler.handleClientGone(client);
			});
		},
		this
	);
};

WebClusterSupport.prototype.addEventsHandler = function () {
	for (var idx in arguments) {
		this.clientEventsHandlers.push(arguments[idx]);
	}

	return this;
};

module.exports = WebClusterSupport;
