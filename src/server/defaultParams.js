/**
 * @author luizssb
 */
function defaultParams (sender, data) {
	data = data || {};
	data.sender = sender.id;

	if (sender.room) {
		data.room = sender.room;
		data.numberOfParticipants =
			sender.manager.getNumberOfClients(sender.room);
	}

	return data;
};

module.exports = defaultParams;
