const ObjectEx = require('../../utils/ObjectEx');

/**
 * @author luizssb
 */
function MasterSlaveInfo () {
	this.master = undefined;
	this.slaves = {};
}

Object.defineProperty(
	MasterSlaveInfo.prototype, 'count', ObjectEx.readOnly(function () {
		return !!this.master + Object.keys(this.slaves).length;
	})
);

MasterSlaveInfo.prototype.get = function (nodeId) {
	return this.master && nodeId === this.master.id
		? this.master : this.slaves[nodeId];
};

MasterSlaveInfo.prototype.setMaster = function (nodeData) {
	this.master = nodeData;
};

MasterSlaveInfo.prototype.addSlave = function (nodeData) {
	this.slaves[nodeData.id] = nodeData;
};

module.exports = MasterSlaveInfo;
