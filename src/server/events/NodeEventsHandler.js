const ClientEventsHandler = require('./ClientEventsHandler');
const NodeEvents = require('../../constants/NodeEvents');

function NodeEventsHandler () {
	ClientEventsHandler.call(this);
	this._clientsRooms = {};
}

NodeEventsHandler.prototype = Object.assign(
	Object.create(ClientEventsHandler.prototype),
	{ constructor: NodeEventsHandler }
);

// Override
NodeEventsHandler.prototype.handleNewClient = function (client) {
	const $this = this;

	client
		.on(NodeEvents.ToServer.SEND_DATA, function (data, dataPackage) {
			if (dataPackage.recipient) {
				$this.sendMessage(
					client,
					dataPackage.recipient,
					NodeEvents.FromServer.RECEIVED_DATA, {}, dataPackage
				);
			} else {
				client.broadcast(
					NodeEvents.FromServer.RECEIVED_DATA, dataPackage
				);
			}
		})
		.on(NodeEvents.ToServer.QUIT, function (data) {
			client.broadcast(NodeEvents.FromServer.NODE_GONE, client.id);
		});

	this._binding = client.on.joinedRoom.add(function (sender) {
		$this._clientsRooms[sender.id] = sender.room;
	});

	this._clientsRooms[client.id] = client.room;
};

// Override
NodeEventsHandler.prototype.handleClientGone = function (client) {
	client.manager.broadcast(
		this._clientsRooms[client.id],
		NodeEvents.FromServer.NODE_GONE, client.id
	);

	this._binding.detach();

	delete this._clientsRooms[client.id];
};

module.exports = NodeEventsHandler;
