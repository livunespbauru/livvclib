const ClientEventsHandler = require('./ClientEventsHandler');
const MasterSlaveInfo = require('./MasterSlaveInfo');
const defaultParams = require('../defaultParams');
const ClusterEvents = require('../../constants/ClusterEvents');
const ClusterJoiningMode = require('../../constants/ClusterJoiningMode');
const NodeConnectionEvents = require('../../constants/NodeConnectionEvents');

/**
 * @author luizssb
 */
function ClusterEventsHandler () {
	ClientEventsHandler.call(this);
	this._clusters = {};

	this._bound_leaveCluster = this._leaveCluster.bind(this);
}

ClusterEventsHandler.prototype = Object.assign(
	Object.create(ClientEventsHandler.prototype),
	{ constructor: ClusterEventsHandler }
);

// Override
ClusterEventsHandler.prototype.handleNewClient = function (client) {
	const leaveCluster = this._bound_leaveCluster;
	const clusters = this._clusters;
	const message = function (toPeerId, event, data) {
		this.sendMessage(
			client, toPeerId, event, this._defaultParams(client, {}), data
		);
	}.bind(this);

	client.on(
		ClusterEvents.ToServer.JOIN_CLUSTER,
		function (data, room, preferredConnection, joiningMode) {
			console.log(ClusterEvents.ToServer.JOIN_CLUSTER);

			if (clusters[room] && clusters[room].get(client.id)) {
				return fail('node already joined cluster');
			}

			if (!client.room) {
				client.join(room);
			}

			const nodeData = {
				id: client.id, connectionMethod: preferredConnection
			};

			joiningMode = joiningMode || ClusterJoiningMode.ANY;
			clusters[room] = clusters[room] || new MasterSlaveInfo();

			var tellSlaves = false;

			if (clusters[room].master) {
				if (joiningMode === ClusterJoiningMode.MASTER) {
					return fail('cluster already has master');
				} else {
					clusters[room].addSlave(nodeData);
				}
			} else if (joiningMode & ClusterJoiningMode.MASTER) {
				clusters[room].setMaster(nodeData);
				tellSlaves = true;
				console.log(`Room '${room}' has master`);
			} else if (joiningMode & ClusterJoiningMode.SLAVE_WAIT) {
				clusters[room].addSlave(nodeData);
				console.log(`Room '${room}' has ${clusters[room].count} participants`);
			} else if (joiningMode & ClusterJoiningMode.SLAVE_NOW) {
				return fail('cluster has no master');
			} else {
				return fail('unknown joining mode');
			}

			client.emit(
				ClusterEvents.FromServer.JOINED_CLUSTER, clusters[room].master
			);

			if (tellSlaves) {
				client.broadcast(ClusterEvents.FromServer.MASTER_JOINED, nodeData);
			}

			function fail (message) {
				client.emit(
					ClusterEvents.FromServer.JOIN_FAILED, { message: message }
				);
			}
		}
	)
		.on(
			NodeConnectionEvents.ToServer.PEEK_CLUSTER,
			function (data, room) {
				const clusterData = {};
				if (clusters[room]) {
					clusterData.participants = clusters.count;
					clusterData.nodes = clusters[room];
				}

				client.emit(ClusterEvents.FromServer.PEEKED, clusterData);
			}
		)
		.on(
			NodeConnectionEvents.ToServer.OFFER_CONNECTION,
			function (data, toPeerId, offer) {
				console.log(
					NodeConnectionEvents.ToServer.OFFER_CONNECTION,
					data, toPeerId
				);
				message(
					toPeerId, NodeConnectionEvents.FromServer.RECEIVED_OFFER,
					offer
				);
			}
		)
		.on(
			NodeConnectionEvents.ToServer.ANSWER_OFFER,
			function (data, toPeerId, answer) {
				console.log(
					NodeConnectionEvents.ToServer.ANSWER_OFFER, data, toPeerId
				);
				message(
					toPeerId, NodeConnectionEvents.FromServer.RECEIVED_ANSWER,
					answer
				);
			}
		)
		.on(
			NodeConnectionEvents.ToServer.FAIL_ANSWER,
			function (data, toPeerId, error) {
				console.log(
					NodeConnectionEvents.ToServer.FAIL_ANSWER, data, toPeerId
				);
				message(
					toPeerId, NodeConnectionEvents.FromServer.ANSWER_FAILED,
					error
				);
			}
		)
		.on(
			NodeConnectionEvents.ToServer.CANDIDATE_PEER,
			function (data, toPeerId, candidate) {
				console.log(
					NodeConnectionEvents.ToServer.CANDIDATE_PEER, data, toPeerId
				);
				message(
					toPeerId, NodeConnectionEvents.FromServer.RECEIVED_CANDIDATE,
					candidate
				);
			}
		)
		.on(ClusterEvents.ToServer.QUIT_CLUSTER, function (data) {
			console.log(ClusterEvents.ToServer.QUIT_CLUSTER, data);
			leaveCluster(client);
		});

	client.on.willEmit.add(this._defaultParams, this);
};

// Override
ClusterEventsHandler.prototype.handleClientGone = function (client) {
	console.log('disconnect');

	const room = client.room;

	if (room) {
		this._leaveCluster(client);
	}
};

ClusterEventsHandler.prototype._generalBroadcast = function (client) {
	const binding = client.manager.on.willBroadcast.add(function (data) {
		defaultParams(client, data);
		this._defaultParams(client, data);
		binding.detach();
	}.bind(this));

	client.manager.broadcast.apply(
		client.manager, Array.prototype.slice.call(arguments, 1)
	);
};

ClusterEventsHandler.prototype._leaveCluster = function (client) {
	if (!this._clusters[client.room]) {
		return;
	}

	console.log('leaveCluster', client.room, this._clusters[client.room].count);

	const room = client.room;

	try {
		client.leave();
	} catch (ex) {}

	const wasMaster = this._clusters[room].master &&
		this._clusters[room].master.id === client.id;

	this._generalBroadcast(
		client, room, ClusterEvents.FromServer.PEER_GONE, wasMaster
	);

	if (wasMaster) {
		console.log(`Room '${room}' dead`);

		for (let clientId in this._clusters[room].slaves) {
			client.manager.get(clientId).leave();
		}

		delete this._clusters[room];
	}
};

ClusterEventsHandler.prototype._defaultParams = function (sender, data) {
	data = data || {};

	if (sender.room && this._clusters[sender.room]) {
		data.nodeData = this._clusters[sender.room].get(sender.id);
	}

	return data;
};

module.exports = ClusterEventsHandler;
