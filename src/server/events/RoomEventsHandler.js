const ClientEventsHandler = require('./ClientEventsHandler');
const RoomEvents = require('../../constants/RoomEvents');

function RoomEventsHandler () {
	ClientEventsHandler.call(this);
}

RoomEventsHandler.prototype = Object.assign(
	Object.create(ClientEventsHandler.prototype),
	{ constructor: RoomEventsHandler }
);

// Override
RoomEventsHandler.prototype.handleNewClient = function (client) {
	client
		.on(RoomEvents.ToServer.JOIN, function (data, room) {
			client.join(room);
		})
		.on(RoomEvents.ToServer.LEAVE, function (data) {
			client.leave();
		});
};

module.exports = RoomEventsHandler;
