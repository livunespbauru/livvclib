const ObjectEx = require('../../utils/ObjectEx');
const defaultParams = require('../defaultParams');

/**
 * @author luizssb
 */
function ClientEventsHandler () {}

/**
 * Handles a new client connection.
 * Should register for the client's events and handle them.
 * @param {ClientConnection} client The new connection.
 */
ClientEventsHandler.prototype.handleNewClient = ObjectEx.abstractMethod;

/**
 * Handles a client disconnection.
 * Should undo everything done by handleNewClient().
 * @param {ClienteConnection} client The gone client.
 */
ClientEventsHandler.prototype.handleClientGone = function () {};

ClientEventsHandler.prototype.sendMessage = function (fromClient, toClientId, event, params) {
	const toClient = fromClient.manager.get(toClientId);

	toClient.emitWithParams.apply(
		toClient,
		[event, Object.assign(defaultParams(fromClient, {}), params)]
			.concat(Array.prototype.slice.call(arguments, 4))
	);
};

module.exports = ClientEventsHandler;
