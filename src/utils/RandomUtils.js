/**
 * @author luizssb
 */
module.exports = {
	string: function (length) {
		if (!length) {
			length = 16;
		}

		var alphabet =
			'abcdefghijklmnopqrstuvwxyz' + 'ABCDEFGHIJKLMNOPQRSTUVWXY' +
			'Z0123456789';

		return Array(length)
			.join()
			.split(',')
			.map(
				function () {
					return alphabet.charAt(
						Math.floor(Math.random() * alphabet.length)
					);
				}
			)
			.join('');
	}
};
