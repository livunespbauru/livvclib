/**
 * Taken from UIVA's (Unity Indie VRPN Adapter) source code.
 * http://web.cs.wpi.edu/~gogo/hive/UIVA/
 * 
 * @author luizssb
 */
module.exports = function (xToRight, yBackwards, zUpwards) {
	const gravs = {
		x: xToRight,
		y: yBackwards,
		z: zUpwards
	};

	var roll, pitch;

	// Compute roll angle
	const sqrtY2Z2 = Math.sqrt(Math.pow(gravs.y, 2) + Math.pow(gravs.z, 2));

	// Prevent dividing by zero
	if (sqrtY2Z2 === 0) {
		roll = 90;
		if (gravs.x >= 0) {
			roll *= -1;
		}
	} else {
		// The math that works, compute roll angle
		roll = Math.atan(gravs.x / sqrtY2Z2);
	}

	// Compute pitch angle
	const sqrtX2Z2 = Math.sqrt(Math.pow(gravs.x, 2) + Math.pow(gravs.z, 2));

	// Prevent dividing by zero
	if (sqrtX2Z2 === 0) {
		pitch = 90;
		if (gravs.y >= 0) {
			pitch *= -1;
		}
	} else {
		// The math that works, compute pitch angle
		pitch = -Math.atan(gravs.y / sqrtX2Z2);
	}

	return {
		roll: roll,
		pitch: pitch
	};
};
