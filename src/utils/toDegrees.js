/**
 * @author luizssb
 */
module.exports = function (rad) {
	return rad * 180 / Math.PI;
};
