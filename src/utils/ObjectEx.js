const LivvclibError = require('../exceptions/LivvclibError.js');
const Signal = require('signals');

/**
 * @author luizssb
 */
module.exports = {
	callDelegate: function (obj, delegateName) {
		if (obj[delegateName]) {
			obj[delegateName].apply(
				null,
				[obj].concat(Array.prototype.slice.call(arguments, 2))
			);
		}
	},
	abstractMethod: function () {
		throw new LivvclibError('abstract method not implemented', 92);
	},
	notImplemented: function () {
		throw new LivvclibError('not implemented', 39);
	},
	isFunction: function (obj) {
		return obj && {}.toString.call(obj) === '[object Function]';
	},
	isObject: function (obj) {
		return obj && obj.toString().indexOf('[object') === 0;
	},
	isBoolean: function (obj) {
		return typeof obj === 'boolean';
	},
	readOnly: function (get, configurable, enumerable) {
		if (configurable === undefined) {
			configurable = true;
		}

		if (enumerable === undefined) {
			enumerable = true;
		}

		var definition = { configurable: configurable, enumerable: enumerable };

		if (this.isFunction(get)) {
			definition.get = get;
		} else {
			definition.value = get;
		}

		return definition;
	},
	defineProperties: function (obj, properties) {
		for (var property in properties) {
			Object.defineProperty(obj, property, properties[property]);
		}
	},
	defineEvents: function (obj, eventsNames) {
		obj.on = obj.on || {};

		eventsNames.forEach(function (element) {
			obj.on[element] = new Signal();
		});
	},
	evaluateDelegate: function (sender, delegate, args) {
		return this.isFunction(delegate)
			? delegate.apply(
				null, [sender].concat(Array.prototype.slice.call(arguments, 2))
			)
			: delegate;
	},
	enumToString: function (enumType, enumValue) {
		for (var key in enumType) {
			if (enumType[key] === enumValue) {
				return key;
			}
		}

		return '<unknown>';
	}
};
