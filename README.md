# Livvclib
----------
Livvclib (Laboratory of Interfaces and Visualization's Web Cluster Library) is a Javascript library/framework for developing interactive 3d information visualization web applications.

It supports the creation of master-slave graphic clusters, in which the nodes are web browsers connected to the web application. Data is then exchanged via WebRTC, for lower latency,  and the application's web server, for better compatibility

It also allows web applications to use diversified input devices (such as Xbox 360 controllers and Wii Remotes), thanks to [VRPN](https://github.com/vrpn/vrpn).

Livvclib is divided into two main parts:

 - the client-side, a set of Javascript classes and functions to be used with web browsers. File `dist/livvclib.client.js`.
 - the server-side, a set of Javascript and C++ classes and functions to be used with a Node.js web server.  File `dist/livvclib.server.js`.

The *browserified* files inside of `dist/` already contain all dependencies ([three.js](https://github.com/mrdoob/three.js), [Socket.IO](https://github.com/socketio/socket.io), [Watch.js](https://github.com/melanke/Watch.JS), [nbind](https://github.com/charto/nbind)).

**Note**:  Livvclib will probably be forever under development, so a few things might be missing (such as distribution via NPM). If you have any questions, please contact me at: `luizssb.biz [@] gmail [.] com`

## Server side usage example
See more in *examples/node/server.js*

	'use strict';
	
	// Common requires, nothing special.
	const os = require('os');
	const nodeStatic = require('node-static');
	const http = require('http');
	const socketIO = require('socket.io');
	const fs = require('fs');
	const LIVVCLIB = require('livvclib.server.js');

	// Starts a web server and also a Socket.IO server.
	const fileServer = new nodeStatic.Server();
	const app = http.createServer(function (req, res) {
		fileServer.serve(req, res);
	})
		.listen(8080);
	const io = socketIO.listen(app);

	// The client manager manages nodes connecting to the server.
	// These nodes can later join a cluster, or not.
	const clientManager = new LIVVCLIB.SocketIOClientsManager(io);

	// The web cluster support manages the structure of clusters in the server and the organization of nodes.
	const cluster = new LIVVCLIB.WebClusterSupport(clientManager)

		// The events handlers deal with events triggered by the client nodes on the server.
		.addEventsHandler(
	
			// Set of events that deal with nodes wanting to connect on to another.
			new LIVVCLIB.ClusterEventsHandler(),

			// Set of events that deal with message exchange between nodes without WebRTC support.
			new LIVVCLIB.NodeEventsHandler(),
			
			// Set of events that deal with nodes that join/leave specific rooms in the Socket.IO server.
			// Nodes using these events can join rooms reserved to clusters, without becoming part of them.
			new LIVVCLIB.RoomEventsHandler()
		)

		// Starts listening to new nodes connecting to the server.
		.setup();
	
	if (supportsVRPN()) {
		// nbind uses NAN to allow the application to use C/C++ code.
		const lib = require('nbind').init().lib;
		
		// The interfaces factory creates abstractions of a device's interfaces (buttons, analogs, etc.).
		const factory = new LIVVCLIB.DefaultInterfacesFactory(lib);

		// Set of events that deal with a node trying to connect to a device.
		cluster.addEventsHandler(new LIVVCLIB.DeviceEventsHandler(factory))
	}

	// Checks if the server supports VRPN.
	// The build files in the build/ folder are exclusively for Windows.
	function supportsVRPN () {
		return /^win/.test(process.platform);
	}

## Client side usage example
See more in *examples/browser/client.html*

	// Connection to the signaling server.
	var serverConnection = new LIVVCLIB.SocketIOServerConnection(
		'http://localhost:8080' // sample address
	);

	// Connection to the master-slave cluster
	// Specifies the connection to the signaling server and the name of the cluster.
	var node = new LIVVCLIB.BufferedMasterSlaveCluster(
		serverConnection, 'room'
	);

	// Gets object containing the events triggered by the node.
	// All events are instances of Signal (from the js-signals lib)
	var on = node.on;

	// Called when the node joins a cluster.
	on.joinedCluster.add(function () {

		// Simple object.
		var obj = {
			foo: 'bar',
			bar: function() {
				console.log(arguments);
			}
		};

		// Sync maintains an object's attributes and methods synchronized with all nodes of the cluster.
		var sync = new LIVVCLIB.Sync(node, 'foobar');
		sync.trackAttribute(obj, 'foo');
		sync.trackMethod(obj, 'bar');
	});

	// Called on slave nodes, when the master joins the cluster.
	on.masterJoined.add(function () {
		console.log('onMasterJoined');
	});

	// Called when it fails to join or create the cluster.
	on.joinedClusterError.add(function () {
		console.log('onJoinClusterError', arguments);
	});
		
	// Called when the master node sends an object.
	on.objectReceived.add(function () {
		console.log('onObjectData', arguments);
	});

	// Called when the master node sends a number/boolean/string.
	on.primitiveDataReceived.add(function () {
		console.log('onPrimitiveData', arguments);
	});

	// Called when the master node sends data that was buffered.
	on.bufferDataSynced.add(function () {
		console.log('onSynchronizedBufferedData', arguments);
	});

## VRPN interfaces example
See more in *examples/node/vrpnMouseBinding*
	// Acquires the C/C++ interfaces mapped to Javascript.
	var lib = require('nbind').init().lib;

	// Specifies an analog interface to connect to and the address to the VRPN server.
	// 'device' points to a C++ class.
	var device = new lib.AnalogRemoteDevice("Mouse0", "localhost");

	// Gets a reference to the 'change' event and registers a callback.
	// If the registration was successful 'runStatus' is 0.
	var runStatus = device.getOnChange().registerCallback(
		'foobar', // callback string identifier.
		function(channels) {
			console.log(channels);

			// Checks the X position of the cursor.
			if (channels[0] >= 0.9) {
				runStatus = 1;
			}
		}
	);

	// Continuously asks for updates from the device.
	while(runStatus === 0) {
		// equivalent of VRPN's mainloop() method.
		device.requestUpdate();
	}

	// Unregisters callback, using same identifier as before.
	device.getOnChange().unregisterCallback('foobar');

	// Disconnects from the device and closes the connection.
	device.disconnect();

## TODO

 - Improve this README and overall documentation.
 - Replace Watch.JS with another way of tracking the object (Javascript handlers?).
 - Find a way to build Livvclib without including all its dependencies in the build file.
 - Compile device interfaces to macOS and Linux.