{
	"targets": [
		{
			"includes": [
				"auto.gypi"
			],
			"sources": [
				"bindings\\vrpn\\CallbackList.cpp",

				"bindings\\vrpn\\IVRPNDeviceBinding.cpp",
				"bindings\\vrpn\\devices\\VRPNDeviceBindingUtils.cpp",
				"bindings\\vrpn\\devices\\BaseVRPNDeviceBinding.cpp",
				"bindings\\vrpn\\devices\\AnalogRemoteDevice.cpp",
				"bindings\\vrpn\\devices\\ButtonRemoteDevice.cpp",
				"bindings\\vrpn\\devices\\TrackerRemoteDevice.cpp",

				"bindings\\vrpn\\IVRPNDeviceLooper.cpp",
				"bindings\\vrpn\\loopers\\BaseVRPNDeviceLooper.cpp",
				"bindings\\vrpn\\loopers\\AsyncWorkerDeviceLooper.cpp",
				"bindings\\vrpn\\loopers\\ThreadedDeviceLooper.cpp",
			],
			"conditions":[
				[
					"OS=='win'",
					{
						"libraries": [
							"<(module_root_dir)/libs/vrpn/vrpn.lib",
							"<(module_root_dir)/libs/vrpn/quat.lib"
						],
						'msvs_settings': {
							'VCCLCompilerTool': {
								'AdditionalOptions': [
									'/MD',
									'/LD'
								]
							}
						},
						'ldflags': [
							'-Wl,-rpath,<(module_root_dir)',
						],
						"cflags": [
							"-std=c++11",
							"-stdlib=libc++"
						]
					}
				]
			]
		}
	],
	"includes": [
		"auto-top.gypi"
	]
}
