'use strict';

const os = require('os');
const nodeStatic = require('node-static');
const http = require('http');
const socketIO = require('socket.io');
const fs = require('fs');
const LIVVCLIB = require('../../dist/livvclib.server');
const ExperimentalDeviceEventsHandler =
	// require('./ExperimentalDeviceEventsHandler');
	require('./ShinExperimentalDeviceEventsHandler');

const fileServer = new nodeStatic.Server();
const app = http.createServer(function (req, res) {
	fileServer.serve(req, res);
})
	.listen(8080, `0.0.0.0`);
const io = socketIO.listen(app);

const clientManager = new LIVVCLIB.SocketIOClientsManager(io);
const cluster = new LIVVCLIB.WebClusterSupport(clientManager)
	.addEventsHandler(
		new LIVVCLIB.ClusterEventsHandler(),
		new LIVVCLIB.NodeEventsHandler(),
		new LIVVCLIB.RoomEventsHandler()
	);
cluster.setup();

if (supportsVRPN()) {
	const lib = require('nbind').init().lib;
	const factory = new LIVVCLIB.DefaultInterfacesFactory(lib);
	cluster.addEventsHandler(new ExperimentalDeviceEventsHandler(factory))
}

function supportsVRPN () {
	return /^win/.test(process.platform);
}