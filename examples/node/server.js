'use strict';

// Common requires, nothing special.
const os = require('os');
const nodeStatic = require('node-static');
const http = require('http');
const socketIO = require('socket.io');
const fs = require('fs');
const LIVVCLIB = require('../../dist/livvclib.server.js');

// Starts a web server and also a Socket.IO server.
const fileServer = new nodeStatic.Server();
const app = http.createServer(function (req, res) {
	fileServer.serve(req, res);
})
	.listen(8080);
const io = socketIO.listen(app);

// The client manager manages nodes connecting to the server.
// These nodes can later join a cluster, or not.
const clientManager = new LIVVCLIB.SocketIOClientsManager(io);

// The web cluster support manages the structure of clusters in the server and the organization of nodes.
const cluster = new LIVVCLIB.WebClusterSupport(clientManager)

	// The events handlers deal with events triggered by the client nodes on the server.
	.addEventsHandler(

		// Set of events that deal with nodes wanting to connect on to another.
		new LIVVCLIB.ClusterEventsHandler(),

		// Set of events that deal with message exchange between nodes without WebRTC support.
		new LIVVCLIB.NodeEventsHandler(),
		
		// Set of events that deal with nodes that join/leave specific rooms in the Socket.IO server.
		// Nodes using these events can join rooms reserved to clusters, without becoming part of them.
		new LIVVCLIB.RoomEventsHandler()
	)

	// Starts listening to new nodes connecting to the server.
	.setup();

if (supportsVRPN()) {
	// nbind uses NAN to allow the application to use C/C++ code.
	const lib = require('nbind').init().lib;
	
	// The interfaces factory creates abstractions of a device's interfaces (buttons, analogs, etc.).
	const factory = new LIVVCLIB.DefaultInterfacesFactory(lib);

	// Set of events that deal with a node trying to connect to a device.
	cluster.addEventsHandler(new LIVVCLIB.DeviceEventsHandler(factory))
}

// Checks if the server supports VRPN.
// The build files in the build/ folder are exclusively for Windows.
function supportsVRPN () {
	return /^win/.test(process.platform);
}