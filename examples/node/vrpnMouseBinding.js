// Acquires the C/C++ interfaces mapped to Javascript.
var lib = require('nbind').init().lib;

// Specifies an analog interface to connect to and the address to the VRPN server.
// 'device' points to a C++ class.
var device = new lib.AnalogRemoteDevice("Mouse0", "localhost");

// Gets a reference to the 'change' event and registers a callback.
// If the registration was successful 'runStatus' is 0.
var runStatus = device.getOnChange().registerCallback(
	'foobar', // callback string identifier.
	function(channels) {
		console.log(channels);

		// Checks the X position of the cursor.
		if (channels[0] >= 0.9) {
			runStatus = 1;
		}
	}
);

// Continuously asks for updates from the device.
while(runStatus === 0) {
	// equivalent of VRPN's mainloop() method.
	device.requestUpdate();
}

console.log('requesting updates, no callbacks')
// Unregisters callback, using same identifier as before.
device.getOnChange().unregisterCallback('foobar');
for (var idx = 0; idx < 1e6; ++idx) {
	device.requestUpdate();
}

console.log('requesting updates, disconnected')
// Disconnects from the device and closes the connection.
device.disconnect();
for (var idx = 0; idx < 1e6; ++idx) {
	device.requestUpdate();
}

console.log('end');
