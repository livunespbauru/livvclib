const LIVVCLIB = require('../../dist/livvclib.client.js');
const nbind = require('nbind');
const lib = nbind.init().lib;

const device = new lib.AnalogRemoteDevice("WiiMote0", "localhost");

console.log('requesting updates, callback registered', device.getDeviceName());
var runStatus = device.getOnChange().registerCallback(
	'foobar',
	function(channels) {
		const tilt = LIVVCLIB.computeTilt(channels[1], channels[2], channels[3]);
		console.log(tilt.roll, tilt.pitch);
	}
);
while(runStatus === 0) {
	device.requestUpdate();
}

console.log('requesting updates, no callbacks')
device.getOnVelocityChange().unregisterCallback('foobar');
runStatus = 0;
while(runStatus++ < 1e6) {
	device.requestUpdate();
}

console.log('requesting updates, disconnected')
device.disconnect();
runStatus = 0;
while(runStatus++ < 1e8) {
	device.requestUpdate();
}

console.log('end');
