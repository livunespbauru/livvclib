var nbind = require('nbind');
var lib = nbind.init().lib;
console.log(lib);

var _device = {};
var idx = 0;

var doStuff = function () {
	if (_device.device) {
		delete _device.device;
	}

	console.log(idx++);
	_device.device = new lib.AnalogRemoteDevice("Mouse0", "localhost");
	_device.device.getOnChange().registerCallback("lol", function () {});
	_device.device.disconnect();
};

setInterval(doStuff, 1);