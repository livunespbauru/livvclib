var nbind = require('nbind');
var lib = nbind.init().lib;

var device = new lib.ButtonRemoteDevice("Mouse0", "localhost");

console.log('requesting updates, callback registered', device.getDeviceName());
var runStatus = device.getOnChange().registerCallback(
	'foobar',
	function(changeData) {
		console.log(
			changeData.getButtonIndex(),
			changeData.getState()
		);

		for (var key in changeData) {
			console.log(key);
		}

		if (changeData.getButtonIndex() === 2 && changeData.getState()) {
			runStatus = 1;
		}
	}
);
while(runStatus === 0) {
	device.requestUpdate();
}

console.log('requesting updates, no callbacks')
device.getOnChange().unregisterCallback('foobar');
runStatus = 0;
while(runStatus++ < 1e6) {
	device.requestUpdate();
}

console.log('requesting updates, disconnected')
device.disconnect();
runStatus = 0;
while(runStatus++ < 1e8) {
	device.requestUpdate();
}

console.log('end');
