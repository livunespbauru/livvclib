var nbind = require('nbind');
var lib = nbind.init().lib;

var _devices = {};
var _idx = 0;

setInterval(function() {
	if (_devices.device) {
		_devices.tracker.stop();
		_devices.device.disconnect();
		delete _devices.tracker;
		delete _devices.device;
		console.log('killed', _idx++);
	}
	console.log('instantiating', _idx);
	_devices.device = new lib.AnalogRemoteDevice("Mouse0", "localhost");
	_devices.device.getOnChange().registerCallback(
		'foobar',
		function (channels) {
			console.log(channels);
		}
	);
	_devices.tracker = new lib.AsyncWorkerDeviceLooper(_devices.device);
	_devices.tracker.start();

}, 500);