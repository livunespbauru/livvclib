var nbind = require('nbind');
var lib = nbind.init().lib;

var device = new lib.AnalogRemoteDevice("Mouse0", "localhost");
var looper = new lib.AsyncWorkerDeviceLooper(device);

device.getOnChange().registerCallback('foobar', function (channels) {
	console.log(channels);
	if (channels[0] >= 0.9) {
		device.getOnChange().unregisterCallback('foobar');
		looper.stop();
		lib.VRPNDeviceBindingSafelyDisconnect(device);
	}
});

looper.start();

setTimeout(function () {}, 1e8);