(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.LIVVCLIB = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){
/*jslint onevar:true, undef:true, newcap:true, regexp:true, bitwise:true, maxerr:50, indent:4, white:false, nomen:false, plusplus:false */
/*global define:false, require:false, exports:false, module:false, signals:false */

/** @license
 * JS Signals <http://millermedeiros.github.com/js-signals/>
 * Released under the MIT license
 * Author: Miller Medeiros
 * Version: 1.0.0 - Build: 268 (2012/11/29 05:48 PM)
 */

(function(global){

    // SignalBinding -------------------------------------------------
    //================================================================

    /**
     * Object that represents a binding between a Signal and a listener function.
     * <br />- <strong>This is an internal constructor and shouldn't be called by regular users.</strong>
     * <br />- inspired by Joa Ebert AS3 SignalBinding and Robert Penner's Slot classes.
     * @author Miller Medeiros
     * @constructor
     * @internal
     * @name SignalBinding
     * @param {Signal} signal Reference to Signal object that listener is currently bound to.
     * @param {Function} listener Handler function bound to the signal.
     * @param {boolean} isOnce If binding should be executed just once.
     * @param {Object} [listenerContext] Context on which listener will be executed (object that should represent the `this` variable inside listener function).
     * @param {Number} [priority] The priority level of the event listener. (default = 0).
     */
    function SignalBinding(signal, listener, isOnce, listenerContext, priority) {

        /**
         * Handler function bound to the signal.
         * @type Function
         * @private
         */
        this._listener = listener;

        /**
         * If binding should be executed just once.
         * @type boolean
         * @private
         */
        this._isOnce = isOnce;

        /**
         * Context on which listener will be executed (object that should represent the `this` variable inside listener function).
         * @memberOf SignalBinding.prototype
         * @name context
         * @type Object|undefined|null
         */
        this.context = listenerContext;

        /**
         * Reference to Signal object that listener is currently bound to.
         * @type Signal
         * @private
         */
        this._signal = signal;

        /**
         * Listener priority
         * @type Number
         * @private
         */
        this._priority = priority || 0;
    }

    SignalBinding.prototype = {

        /**
         * If binding is active and should be executed.
         * @type boolean
         */
        active : true,

        /**
         * Default parameters passed to listener during `Signal.dispatch` and `SignalBinding.execute`. (curried parameters)
         * @type Array|null
         */
        params : null,

        /**
         * Call listener passing arbitrary parameters.
         * <p>If binding was added using `Signal.addOnce()` it will be automatically removed from signal dispatch queue, this method is used internally for the signal dispatch.</p>
         * @param {Array} [paramsArr] Array of parameters that should be passed to the listener
         * @return {*} Value returned by the listener.
         */
        execute : function (paramsArr) {
            var handlerReturn, params;
            if (this.active && !!this._listener) {
                params = this.params? this.params.concat(paramsArr) : paramsArr;
                handlerReturn = this._listener.apply(this.context, params);
                if (this._isOnce) {
                    this.detach();
                }
            }
            return handlerReturn;
        },

        /**
         * Detach binding from signal.
         * - alias to: mySignal.remove(myBinding.getListener());
         * @return {Function|null} Handler function bound to the signal or `null` if binding was previously detached.
         */
        detach : function () {
            return this.isBound()? this._signal.remove(this._listener, this.context) : null;
        },

        /**
         * @return {Boolean} `true` if binding is still bound to the signal and have a listener.
         */
        isBound : function () {
            return (!!this._signal && !!this._listener);
        },

        /**
         * @return {boolean} If SignalBinding will only be executed once.
         */
        isOnce : function () {
            return this._isOnce;
        },

        /**
         * @return {Function} Handler function bound to the signal.
         */
        getListener : function () {
            return this._listener;
        },

        /**
         * @return {Signal} Signal that listener is currently bound to.
         */
        getSignal : function () {
            return this._signal;
        },

        /**
         * Delete instance properties
         * @private
         */
        _destroy : function () {
            delete this._signal;
            delete this._listener;
            delete this.context;
        },

        /**
         * @return {string} String representation of the object.
         */
        toString : function () {
            return '[SignalBinding isOnce:' + this._isOnce +', isBound:'+ this.isBound() +', active:' + this.active + ']';
        }

    };


/*global SignalBinding:false*/

    // Signal --------------------------------------------------------
    //================================================================

    function validateListener(listener, fnName) {
        if (typeof listener !== 'function') {
            throw new Error( 'listener is a required param of {fn}() and should be a Function.'.replace('{fn}', fnName) );
        }
    }

    /**
     * Custom event broadcaster
     * <br />- inspired by Robert Penner's AS3 Signals.
     * @name Signal
     * @author Miller Medeiros
     * @constructor
     */
    function Signal() {
        /**
         * @type Array.<SignalBinding>
         * @private
         */
        this._bindings = [];
        this._prevParams = null;

        // enforce dispatch to aways work on same context (#47)
        var self = this;
        this.dispatch = function(){
            Signal.prototype.dispatch.apply(self, arguments);
        };
    }

    Signal.prototype = {

        /**
         * Signals Version Number
         * @type String
         * @const
         */
        VERSION : '1.0.0',

        /**
         * If Signal should keep record of previously dispatched parameters and
         * automatically execute listener during `add()`/`addOnce()` if Signal was
         * already dispatched before.
         * @type boolean
         */
        memorize : false,

        /**
         * @type boolean
         * @private
         */
        _shouldPropagate : true,

        /**
         * If Signal is active and should broadcast events.
         * <p><strong>IMPORTANT:</strong> Setting this property during a dispatch will only affect the next dispatch, if you want to stop the propagation of a signal use `halt()` instead.</p>
         * @type boolean
         */
        active : true,

        /**
         * @param {Function} listener
         * @param {boolean} isOnce
         * @param {Object} [listenerContext]
         * @param {Number} [priority]
         * @return {SignalBinding}
         * @private
         */
        _registerListener : function (listener, isOnce, listenerContext, priority) {

            var prevIndex = this._indexOfListener(listener, listenerContext),
                binding;

            if (prevIndex !== -1) {
                binding = this._bindings[prevIndex];
                if (binding.isOnce() !== isOnce) {
                    throw new Error('You cannot add'+ (isOnce? '' : 'Once') +'() then add'+ (!isOnce? '' : 'Once') +'() the same listener without removing the relationship first.');
                }
            } else {
                binding = new SignalBinding(this, listener, isOnce, listenerContext, priority);
                this._addBinding(binding);
            }

            if(this.memorize && this._prevParams){
                binding.execute(this._prevParams);
            }

            return binding;
        },

        /**
         * @param {SignalBinding} binding
         * @private
         */
        _addBinding : function (binding) {
            //simplified insertion sort
            var n = this._bindings.length;
            do { --n; } while (this._bindings[n] && binding._priority <= this._bindings[n]._priority);
            this._bindings.splice(n + 1, 0, binding);
        },

        /**
         * @param {Function} listener
         * @return {number}
         * @private
         */
        _indexOfListener : function (listener, context) {
            var n = this._bindings.length,
                cur;
            while (n--) {
                cur = this._bindings[n];
                if (cur._listener === listener && cur.context === context) {
                    return n;
                }
            }
            return -1;
        },

        /**
         * Check if listener was attached to Signal.
         * @param {Function} listener
         * @param {Object} [context]
         * @return {boolean} if Signal has the specified listener.
         */
        has : function (listener, context) {
            return this._indexOfListener(listener, context) !== -1;
        },

        /**
         * Add a listener to the signal.
         * @param {Function} listener Signal handler function.
         * @param {Object} [listenerContext] Context on which listener will be executed (object that should represent the `this` variable inside listener function).
         * @param {Number} [priority] The priority level of the event listener. Listeners with higher priority will be executed before listeners with lower priority. Listeners with same priority level will be executed at the same order as they were added. (default = 0)
         * @return {SignalBinding} An Object representing the binding between the Signal and listener.
         */
        add : function (listener, listenerContext, priority) {
            validateListener(listener, 'add');
            return this._registerListener(listener, false, listenerContext, priority);
        },

        /**
         * Add listener to the signal that should be removed after first execution (will be executed only once).
         * @param {Function} listener Signal handler function.
         * @param {Object} [listenerContext] Context on which listener will be executed (object that should represent the `this` variable inside listener function).
         * @param {Number} [priority] The priority level of the event listener. Listeners with higher priority will be executed before listeners with lower priority. Listeners with same priority level will be executed at the same order as they were added. (default = 0)
         * @return {SignalBinding} An Object representing the binding between the Signal and listener.
         */
        addOnce : function (listener, listenerContext, priority) {
            validateListener(listener, 'addOnce');
            return this._registerListener(listener, true, listenerContext, priority);
        },

        /**
         * Remove a single listener from the dispatch queue.
         * @param {Function} listener Handler function that should be removed.
         * @param {Object} [context] Execution context (since you can add the same handler multiple times if executing in a different context).
         * @return {Function} Listener handler function.
         */
        remove : function (listener, context) {
            validateListener(listener, 'remove');

            var i = this._indexOfListener(listener, context);
            if (i !== -1) {
                this._bindings[i]._destroy(); //no reason to a SignalBinding exist if it isn't attached to a signal
                this._bindings.splice(i, 1);
            }
            return listener;
        },

        /**
         * Remove all listeners from the Signal.
         */
        removeAll : function () {
            var n = this._bindings.length;
            while (n--) {
                this._bindings[n]._destroy();
            }
            this._bindings.length = 0;
        },

        /**
         * @return {number} Number of listeners attached to the Signal.
         */
        getNumListeners : function () {
            return this._bindings.length;
        },

        /**
         * Stop propagation of the event, blocking the dispatch to next listeners on the queue.
         * <p><strong>IMPORTANT:</strong> should be called only during signal dispatch, calling it before/after dispatch won't affect signal broadcast.</p>
         * @see Signal.prototype.disable
         */
        halt : function () {
            this._shouldPropagate = false;
        },

        /**
         * Dispatch/Broadcast Signal to all listeners added to the queue.
         * @param {...*} [params] Parameters that should be passed to each handler.
         */
        dispatch : function (params) {
            if (! this.active) {
                return;
            }

            var paramsArr = Array.prototype.slice.call(arguments),
                n = this._bindings.length,
                bindings;

            if (this.memorize) {
                this._prevParams = paramsArr;
            }

            if (! n) {
                //should come after memorize
                return;
            }

            bindings = this._bindings.slice(); //clone array in case add/remove items during dispatch
            this._shouldPropagate = true; //in case `halt` was called before dispatch or during the previous dispatch.

            //execute all callbacks until end of the list or until a callback returns `false` or stops propagation
            //reverse loop since listeners with higher priority will be added at the end of the list
            do { n--; } while (bindings[n] && this._shouldPropagate && bindings[n].execute(paramsArr) !== false);
        },

        /**
         * Forget memorized arguments.
         * @see Signal.memorize
         */
        forget : function(){
            this._prevParams = null;
        },

        /**
         * Remove all bindings from signal and destroy any reference to external objects (destroy Signal object).
         * <p><strong>IMPORTANT:</strong> calling any method on the signal instance after calling dispose will throw errors.</p>
         */
        dispose : function () {
            this.removeAll();
            delete this._bindings;
            delete this._prevParams;
        },

        /**
         * @return {string} String representation of the object.
         */
        toString : function () {
            return '[Signal active:'+ this.active +' numListeners:'+ this.getNumListeners() +']';
        }

    };


    // Namespace -----------------------------------------------------
    //================================================================

    /**
     * Signals namespace
     * @namespace
     * @name signals
     */
    var signals = Signal;

    /**
     * Custom event broadcaster
     * @see Signal
     */
    // alias for backwards compatibility (see #gh-44)
    signals.Signal = Signal;



    //exports to multiple environments
    if(typeof define === 'function' && define.amd){ //AMD
        define(function () { return signals; });
    } else if (typeof module !== 'undefined' && module.exports){ //node
        module.exports = signals;
    } else { //browser
        //use string because of Google closure compiler ADVANCED_MODE
        /*jslint sub:true */
        global['signals'] = signals;
    }

}(this));

},{}],2:[function(_dereq_,module,exports){
/**
 * @author luizssb
 */
module.exports = {
	ToServer: {
		JOIN_CLUSTER: 'joinCluster',
		QUIT_CLUSTER: 'leave',
		PEEK_CLUSTER: 'peek'
	},
	FromServer: {
		JOINED_CLUSTER: 'joinCluster',
		JOIN_FAILED: 'joinClusterError',
		PEER_GONE: 'peerGone',
		MASTER_JOINED: 'masterJoin',
		PEEKED: 'peek'
	}
};

},{}],3:[function(_dereq_,module,exports){
/**
 * @author luizssb
 */
module.exports = {
	ANY: 7,
	SLAVE_NOW: 1,
	SLAVE_WAIT: 2,
	MASTER: 4
};

},{}],4:[function(_dereq_,module,exports){
/**
 * @author luizssb
 */

module.exports = {
	forDevice: function (device, address, type) {
		var _makeEventName = function (event) {
			return 'device:' + [event, device, address, type].join(':');
		};
		return {
			ToServer: {
				SUBSCRIBE_TO_EVENT: _makeEventName('subscribe'),
				UNSUBSCRIBE_FROM_EVENT: _makeEventName('unsubscribe')
			},
			FromServer: {
				SUBSCRIBE: _makeEventName('subscribe'),
				SUBSCRIBE_ERROR: _makeEventName('subscribeError'),
				DEVICE_EVENT: _makeEventName('event'),
				CONNECT: _makeEventName('connect'),
				CONNECT_ERROR: _makeEventName('error'),
				LOST: _makeEventName('lost')
			}
		};
	},
	ToServer: {
		CONNECT: 'device',
		DISCONNECT: 'device:disconnect',
		QUERY_DEVICES: 'device:query'
	},
	FromServer: {
		NEW_DEVICE: 'device:new',
		DEVICES_QUERY: 'device:query'
	}
};

},{}],5:[function(_dereq_,module,exports){
/**
 * @author luizssb
 */
module.exports = {
	ANALOG: 'analog',
	BUTTON: 'button'
};

},{}],6:[function(_dereq_,module,exports){
/**
 * @author luizssb
 */
module.exports = {
	ToServer: {
		OFFER_CONNECTION: 'offer',
		ANSWER_OFFER: 'answer',
		FAIL_ANSWER: 'answerFailed',
		CANDIDATE_PEER: 'candidate'
	},
	FromServer: {
		RECEIVED_OFFER: 'offer',
		RECEIVED_ANSWER: 'answer',
		ANSWER_FAILED: 'answerFailed',
		RECEIVED_CANDIDATE: 'candidate'
	}
};

},{}],7:[function(_dereq_,module,exports){
/**
 * @author luizssb
 */
module.exports = {
	FromServer: {
		RECEIVED_DATA: 'node:data',
		NODE_GONE: 'node:gone'
	},
	ToServer: {
		SEND_DATA: 'node:data',
		QUIT: 'node:quit'
	}
};

},{}],8:[function(_dereq_,module,exports){
/**
 * @author luizssb
 */
module.exports = {
	ToServer: {
		JOIN: 'room:join',
		LEAVE: 'room:leave'
	}
};

},{}],9:[function(_dereq_,module,exports){
/**
 * @author luizssb
 */
function LivvclibError (message, code, previous, doNotLog) {
	var tmp = Error.apply(this, arguments);
	tmp.name = this.name = 'LivvclibError';

	this.message = tmp.message;
	this.code = code;
	this.previous = previous;
	this.stack = tmp.stack;

	if (!doNotLog) {
		console.error(this);
	}
	return this;
}

LivvclibError.prototype = Object.assign(
	Object.create(Error.prototype),
	{ constructor: LivvclibError }
);

module.exports = LivvclibError;

},{}],10:[function(_dereq_,module,exports){
const RandomUtils = _dereq_('../utils/RandomUtils');
const defaultParams = _dereq_('./defaultParams');

/**
 * @author luizssb
 */
function WebClusterSupport (clientsManager) {
	this.tag = RandomUtils.string();
	this.clientsManager = clientsManager;
	this.clientEventsHandlers = [];
}

WebClusterSupport.prototype.setup = function () {
	this.clientsManager.start();

	this.clientsManager.on.clientConnected.add(
		function (client) {
			client.on.willEmit.add(defaultParams, this);

			this.clientEventsHandlers.forEach(function (handler) {
				handler.handleNewClient(client);
			});
		},
		this
	);

	this.clientsManager.on.clientDisconnected.add(
		function (client) {
			this.clientEventsHandlers.forEach(function (handler) {
				handler.handleClientGone(client);
			});
		},
		this
	);
};

WebClusterSupport.prototype.addEventsHandler = function () {
	for (var idx in arguments) {
		this.clientEventsHandlers.push(arguments[idx]);
	}

	return this;
};

module.exports = WebClusterSupport;

},{"../utils/RandomUtils":26,"./defaultParams":15}],11:[function(_dereq_,module,exports){
const ObjectEx = _dereq_('../../utils/ObjectEx');

/**
 * @author luizssb
 */
function ClientConnection (parentManager) {
	this._manager = parentManager;

	this.on = this.on.bind(this);
	ObjectEx.defineEvents(
		this, [
			/**
			 * Called when a request will be made and the default parameters must be customized.
			 * @param {ClientConnection} sender Instance emitting data.
			 * @param {Object} data The default parameters that can be customized.
			 */
			'willEmit',

			/**
			 * Called when the client joins a room in the server.
			 * @param {ClientConnection} sender Instance that joined the room.
			 */
			'joinedRoom',

			/**
			 * Called when the client leaves a room in the server.
			 * @param {ClientConnection} sender Instance that left the room.
			 * @param {string} room Name of the room that was left.
			 */
			'leftRoom'
		]
	);
}

ObjectEx.defineProperties(
	ClientConnection.prototype,
	{
		manager: ObjectEx.readOnly(function () { return this._manager; }),
		id: ObjectEx.readOnly(ObjectEx.abstractMethod),
		room: ObjectEx.readOnly(ObjectEx.abstractMethod),
		connected: ObjectEx.readOnly(ObjectEx.abstractMethod)
	}
);

/**
 * Registers a callback to be executed whenever some client sends a message.
 * @param {string} event Name of the event that triggers the callback;
 * @param {Function} callback Callback to be executed when the event is triggered.
 * @return {ClientsConnection} Itself.
 */
ClientConnection.prototype.on = ObjectEx.abstractMethod;

/**
 * Deregisters a callback of an evet
 * @param {string} event The event that triggers the callback.
 * @return {ClientsConnection} Itself.
 */
ClientConnection.prototype.off = ObjectEx.abstractMethod;

ClientConnection.prototype.emit = function (event) {
	return this.emitWithParams.apply(
		this, [event, {}].concat(Array.prototype.slice.call(arguments, 1))
	);
};

ClientConnection.prototype.emitWithParams = function (event, params) {
	const defaultParams = {};
	this.on.willEmit.dispatch(this, defaultParams);
	Object.assign(defaultParams, params);

	return this._emit.apply(
		this,
		[event, defaultParams].concat(Array.prototype.slice.call(arguments, 2))
	);
};

/**
 * Sends a message to a client.
 * @param {string} event The event to trigger on the clients.
 * @param {Object...} [] Data to send to the clients.
 * @return {ClientConnection} Itself.
 */
ClientConnection.prototype._emit = ObjectEx.abstractMethod;

/**
 * Makes the client connection join an specific room.
 * Client may be connected to only one room at a time.
 * @param {string} room The Room to join.
 * @return {ClientConnection} Itself.
 */
ClientConnection.prototype.join = ObjectEx.abstractMethod;

/**
 * Leaves the room.
 * @return {ClientConnection} Itself.
 */
ClientConnection.prototype.leave = ObjectEx.abstractMethod;

/**
 * Terminates connection.
 * @return {ClientConnection} Itself.
 */
ClientConnection.prototype.disconnect = ObjectEx.abstractMethod;

ClientConnection.prototype.broadcast = function (event) {
	const defaultParams = {};
	this.on.willEmit.dispatch(this, defaultParams);
	return this._broadcast.apply(
		this,
		[event, defaultParams].concat(Array.prototype.slice.call(arguments, 1))
	);
};

/**
 * Broadcasts a message to all nodes in the room this client is in, excluding it.
 * @param {string} event The event to trigger on the clients.
 * @param {Object...} [] Data to send to the clients.
 * @return {ClientConnection} Itself.
 */
ClientConnection.prototype._broadcast = ObjectEx.abstractMethod;

module.exports = ClientConnection;

},{"../../utils/ObjectEx":25}],12:[function(_dereq_,module,exports){
const ObjectEx = _dereq_('../../utils/ObjectEx');

/**
 * @author luizssb
 */
function ClientsManager () {
	ObjectEx.defineEvents(
		this, [
			/**
			 * Called when a new client connects to the server.
			 * @param {ClientConnection} client Connection to the new client.
			 */
			'clientConnected',

			/**
			 * Called when a client disconnects from the server.
			 * @param {ClientConnection} client Connection to the client.
			 */
			'clientDisconnected',

			/**
			 * Called when data will be broadcast to all nodes in a room.
			 * @param {Object} data Data to be customized.
			 */
			'willBroadcast'
		]
	);
}

/**
 * Starts waiting for new clients to connect.
 * @return {ClientsManager} Itself.
 */
ClientsManager.prototype.start = ObjectEx.abstractMethod;

ClientsManager.prototype.broadcast = function (room, event) {
	const defaultParams = {};
	this.on.willBroadcast.dispatch(defaultParams);

	return this._broadcast.apply(
		this,
		[room, event, defaultParams]
			.concat(Array.prototype.slice.call(arguments, 2))
	);
};

/**
 * Broadcasts an event to all clients in a room.
 * @param {string} room The room to receive the broadcast.
 * @param {string} event The event to trigger on the clients.
 * @param {Object...} [] Data to send to the clients
 * @return {ClientsManager} Itself.
 */
ClientsManager.prototype._broadcast = ObjectEx.abstractMethod;

/**
 * Gets the number of clients.
 * @param {string} room Room in which the clients are.
 * @return {int} The number of clients in the room.
 */
ClientsManager.prototype.getNumberOfClients = ObjectEx.abstractMethod;

/**
 * Gets the connection to one of the clients.
 * @param {string} id The id of the client.
 * @return {ClientConnection} The connection to the client.
 */
ClientsManager.prototype.get = ObjectEx.abstractMethod;

module.exports = ClientsManager;

},{"../../utils/ObjectEx":25}],13:[function(_dereq_,module,exports){
const ClientConnection = _dereq_('./ClientConnection');
const ObjectEx = _dereq_('../../utils/ObjectEx');

function SocketIOClientConnection (socket, parentManager) {
	ClientConnection.call(this, parentManager);
	this.socket = socket;
}

SocketIOClientConnection.prototype = Object.assign(
	Object.create(ClientConnection.prototype),
	{ constructor: SocketIOClientConnection }
);

ObjectEx.defineProperties(
	SocketIOClientConnection.prototype,
	{
		id: ObjectEx.readOnly(function () { return this.socket.id; }),
		room: ObjectEx.readOnly(function () { return this._room; }),
		connected:
			ObjectEx.readOnly(function () { return this.socket.connected; })
	}
);

// Override
SocketIOClientConnection.prototype.on = function (event, callback) {
	this.socket.on(event, callback);
	return this;
};

// Override
SocketIOClientConnection.prototype.off = function (event) {
	this.socket.removeAllListeners(event);
	return this;
};

// Override
SocketIOClientConnection.prototype._emit = function (event) {
	this.socket.emit.apply(this.socket, Array.prototype.slice.call(arguments, 0));
	return this;
};

// Override
SocketIOClientConnection.prototype._broadcast = function (event) {
	const clients = this.socket.broadcast.to(this.room);
	clients.emit.apply(clients, Array.prototype.slice.call(arguments, 0));
};

// Override
SocketIOClientConnection.prototype.join = function (room) {
	if (this.room) {
		if (this.room === room) {
			return this;
		} else {
			throw new Error('client already in another room');
		}
	}

	this.socket.join(room);
	this._room = room;

	this.on.joinedRoom.dispatch(this);

	return this;
};

// Override
SocketIOClientConnection.prototype.leave = function () {
	const room = this._room;

	this.socket.leave(this.room);
	this._room = null;

	this.on.leftRoom.dispatch(room);

	return this;
};

// Override
SocketIOClientConnection.prototype.disconnect = function () {
	this.socket.disconnect();

	const room = this._room;
	this._room = null;
	this.on.leftRoom.dispatch(this, room);

	return this;
};

module.exports = SocketIOClientConnection;

},{"../../utils/ObjectEx":25,"./ClientConnection":11}],14:[function(_dereq_,module,exports){
const ClientsManager = _dereq_('./ClientsManager');
const SocketIOClientConnection = _dereq_('./SocketIOClientConnection');

/**
 * @author luizssb
 */
function SocketIOClientsManager (io) {
	ClientsManager.call(this);

	this.io = io;
	this._clients = {};
}

SocketIOClientsManager.prototype = Object.assign(
	Object.create(ClientsManager.prototype),
	{ constructor: SocketIOClientsManager }
);

// Override
SocketIOClientsManager.prototype.start = function () {
	if (!this._started) {
		this.io.sockets.on('connection', function (socket) {
			const client = new SocketIOClientConnection(socket, this);
			this._clients[client.id] = client;
			this.on.clientConnected.dispatch(client);

			client.on('disconnect', function () {
				this.on.clientDisconnected.dispatch(client);
			}.bind(this));
		}.bind(this));
		this._started = true;
	}

	return this;
};

// Override
SocketIOClientsManager.prototype._broadcast = function (room) {
	const clients = this.io.sockets.in(room);
	clients.emit.apply(clients, Array.prototype.slice.call(arguments, 1));

	return this;
};

// Override
SocketIOClientsManager.prototype.getNumberOfClients = function (room) {
	return Object.keys(this.io.sockets.adapter.rooms[room].sockets).length;
};

// Override
SocketIOClientsManager.prototype.get = function (id) {
	return this._clients[id];
};

module.exports = SocketIOClientsManager;

},{"./ClientsManager":12,"./SocketIOClientConnection":13}],15:[function(_dereq_,module,exports){
/**
 * @author luizssb
 */
function defaultParams (sender, data) {
	data = data || {};
	data.sender = sender.id;

	if (sender.room) {
		data.room = sender.room;
		data.numberOfParticipants =
			sender.manager.getNumberOfClients(sender.room);
	}

	return data;
};

module.exports = defaultParams;

},{}],16:[function(_dereq_,module,exports){
const InterfacesFactory = _dereq_('./InterfacesFactory');
const DeviceInterface = _dereq_('../../constants/DeviceInterface');

/**
 * @author luizssb
 */
function DefaultInterfacesFactory (nativeLib) {
	InterfacesFactory.call(this);

	this.nativeLib = nativeLib;

	this._data = {};
	for (var deviceInterface in DeviceInterface) {
		this._data[DeviceInterface[deviceInterface]] = {
			class: DeviceInterface[deviceInterface][0].toUpperCase() +
				DeviceInterface[deviceInterface].substring(1) + 'RemoteDevice'
		};
	}
}

// Override
DefaultInterfacesFactory.prototype.makeInterface = function (
	name, address, deviceInterface
) {
	return new this.nativeLib[this._data[deviceInterface].class](name, address);
};

// Override
DefaultInterfacesFactory.prototype.makeLooper = function (deviceBinding) {
	return new this.nativeLib.AsyncWorkerDeviceLooper(deviceBinding);
};

module.exports = DefaultInterfacesFactory;

},{"../../constants/DeviceInterface":5,"./InterfacesFactory":18}],17:[function(_dereq_,module,exports){
const ClientEventsHandler = _dereq_('../events/ClientEventsHandler');
const DeviceEvents = _dereq_('../../constants/DeviceEvents');

function makeDeviceId (name, address, deviceInterface) {
	return [name, address, deviceInterface].join(':');
};

function eventGetterName (eventName) {
	return 'getOn' + eventName[0].toUpperCase() + eventName.substr(1);
};

function turnSerializable (obj) {
	if (obj instanceof Array) {
		return obj;
	}

	const serializable = {};

	for (var key in obj) {
		if (key.indexOf('get') === 0) {
			serializable[attrName(key)] = obj[key]();
		}
	}

	return serializable;

	function attrName (getterName) {
		getterName = getterName.substr(3);
		return getterName.charAt(0).toLowerCase() + getterName.slice(1);
	}
}

/**
 * @author luizssb
 */
function DeviceEventsHandler (interfaceFactory) {
	ClientEventsHandler.call(this);

	this._interfaceFactory = interfaceFactory;
	this._devicesPerClient = {};
}

DeviceEventsHandler.prototype = Object.assign(
	Object.create(ClientEventsHandler.prototype),
	{ constructor: DeviceEventsHandler }
);

// Override
DeviceEventsHandler.prototype.handleNewClient = function (client) {
	const devicesPerClient = this._devicesPerClient;
	devicesPerClient[client.id] = {};

	const factory = this._interfaceFactory;

	client.on(
		DeviceEvents.ToServer.CONNECT,
		function (data, name, address, deviceInterface) {
			console.log(
				DeviceEvents.ToServer.CONNECT, name, address, deviceInterface
			);

			const deviceId = makeDeviceId(name, address, deviceInterface);
			const handlerEvents =
				DeviceEvents.forDevice(name, address, deviceInterface);

			if (devicesPerClient[client.id][deviceId]) {
				client.emit(handlerEvents.FromServer.CONNECT);
				return;
			}

			const device =
				factory.makeInterface(name, address, deviceInterface);
			const looper = factory.makeLooper(device);

			const deviceData = devicesPerClient[client.id][deviceId] = {
				name: name,
				address: address,
				deviceInterface: deviceInterface,
				device: device,
				looper: looper,
				subscribedEvents: {},
				handlerEvents: handlerEvents
			};

			client.on(
				handlerEvents.ToServer.SUBSCRIBE_TO_EVENT,
				function (subscriptionData, eventName, broadcastsData) {
					console.log(
						handlerEvents.ToServer.SUBSCRIBE_TO_EVENT,
						eventName, broadcastsData
					);

					if (deviceData.subscribedEvents[eventName]) {
						client.emit(
							handlerEvents.FromServer.SUBSCRIBE, eventName
						);
						return;
					}

					if (!device[eventGetterName(eventName)]) {
						client.emit(
							handlerEvents.FromServer.SUBSCRIBE_ERROR,
							eventName,
							{ message: 'interface has no event ' + eventName }
						);
						return;
					}

					const event = device[eventGetterName(eventName)]();
					const result = event.registerCallback(
						client.id,
						function (eventData) {
							eventData = turnSerializable(eventData);

							if (broadcastsData && client.room) {
								client.manager.broadcast(
									client.room,
									handlerEvents.FromServer.DEVICE_EVENT,
									eventName, eventData
								);
							} else {
								client.emit(
									handlerEvents.FromServer.DEVICE_EVENT,
									eventName, eventData
								);
							}
						}
					);

					if (result === 0) {
						deviceData.subscribedEvents[eventName] = true;

						client.emit(
							handlerEvents.FromServer.SUBSCRIBE, eventName
						);

						if (!looper.isRunning()) {
							looper.start();
						}
					} else {
						client.emit(
							handlerEvents.FromServer.SUBSCRIBE_ERROR,
							eventName,
							{
								message: 'could not register callback for event',
								code: result
							}
						);
					}
				}
			)
				.on(
					handlerEvents.ToServer.UNSUBSCRIBE_FROM_EVENT,
					function (data, eventName) {
						console.log(
							handlerEvents.ToServer.UNSUBSCRIBE_FROM_EVENT, eventName
						);

						if (device[eventGetterName(eventName)]) {
							device[eventGetterName(eventName)]()
								.unregisterCallback(client.id);
							delete deviceData.subscribedEvents[eventName];
						} else {
							console.log('tried to unsubscribe from inexistent event');
						}
					}
				)
				.emit(handlerEvents.FromServer.CONNECT)
				.broadcast(
					DeviceEvents.FromServer.NEW_DEVICE,
					name, address, deviceInterface
				);
		}
	)
		.on(
			DeviceEvents.ToServer.DISCONNECT,
			function (data, name, address, deviceInterface) {
				console.log(
					DeviceEvents.ToServer.DISCONNECT,
					name, address, deviceInterface
				);

				this._killDevice(
					client, makeDeviceId(name, address, deviceInterface)
				);
			}.bind(this)
		)
		.on(
			DeviceEvents.ToServer.QUERY_DEVICES,
			function (data) {
				const result = [];

				for (var clientId in devicesPerClient) {
					for (var deviceId in devicesPerClient[clientId]) {
						const deviceData = devicesPerClient[clientId][deviceId];
						result.push({
							name: deviceData.name,
							address: deviceData.address,
							deviceInterface: deviceData.deviceInterface
						});
					}
				}

				client.emit(DeviceEvents.FromServer.DEVICES_QUERY, result);
			}
		);
};

// Override
DeviceEventsHandler.prototype.handleClientGone = function (client) {
	for (var identifier in this._devicesPerClient[client.id]) {
		this._killDevice(client, identifier);
	}
	delete this._devicesPerClient[client.id];
};

DeviceEventsHandler.prototype._killDevice = function (client, identifier) {
	const deviceData = this._devicesPerClient[client.id][identifier];

	if (!deviceData) {
		return;
	}

	deviceData.looper.stop();

	if (client.connected) {
		client.off(deviceData.handlerEvents.ToServer.SUBSCRIBE_TO_EVENT)
			.off(deviceData.handlerEvents.ToServer.UNSUBSCRIBE_FROM_EVENT);
	}

	for (var eventName in deviceData.subscribedEvents) {
		deviceData
			.device[eventGetterName(eventName)]().unregisterCallback(client.id);
		delete deviceData.subscribedEvents[eventName];
	}

	deviceData.device.disconnect();
	client.manager
		.broadcast(client.room, deviceData.handlerEvents.FromServer.LOST);
	delete this._devicesPerClient[client.id][identifier];
};

module.exports = DeviceEventsHandler;

},{"../../constants/DeviceEvents":4,"../events/ClientEventsHandler":19}],18:[function(_dereq_,module,exports){
const ObjectEx = _dereq_('../../utils/ObjectEx');

/**
 * @author luizssb
 */
function InterfacesFactory () {}

/**
 * Creates an instance of a device interface.
 * @param {string} name Name of the device.
 * @param {string} address Address of the device.
 * @param {string} deviceInterface Interface of the device.
 * @return {IVRPNDeviceBinding} Device interface binding.
 */
InterfacesFactory.prototype.makeInterface = ObjectEx.abstractMethod;

/**
 * Create a looper for a device interface.
 * @param {IVRPNDeviceBinding} binding Device interface binding to be looped.
 * @return {IVRPNDeviceLooper} Looper for the device.
 */
InterfacesFactory.prototype.makeLooper = ObjectEx.abstractMethod;

module.exports = InterfacesFactory;

},{"../../utils/ObjectEx":25}],19:[function(_dereq_,module,exports){
const ObjectEx = _dereq_('../../utils/ObjectEx');
const defaultParams = _dereq_('../defaultParams');

/**
 * @author luizssb
 */
function ClientEventsHandler () {}

/**
 * Handles a new client connection.
 * Should register for the client's events and handle them.
 * @param {ClientConnection} client The new connection.
 */
ClientEventsHandler.prototype.handleNewClient = ObjectEx.abstractMethod;

/**
 * Handles a client disconnection.
 * Should undo everything done by handleNewClient().
 * @param {ClienteConnection} client The gone client.
 */
ClientEventsHandler.prototype.handleClientGone = function () {};

ClientEventsHandler.prototype.sendMessage = function (fromClient, toClientId, event, params) {
	const toClient = fromClient.manager.get(toClientId);

	toClient.emitWithParams.apply(
		toClient,
		[event, Object.assign(defaultParams(fromClient, {}), params)]
			.concat(Array.prototype.slice.call(arguments, 4))
	);
};

module.exports = ClientEventsHandler;

},{"../../utils/ObjectEx":25,"../defaultParams":15}],20:[function(_dereq_,module,exports){
const ClientEventsHandler = _dereq_('./ClientEventsHandler');
const MasterSlaveInfo = _dereq_('./MasterSlaveInfo');
const defaultParams = _dereq_('../defaultParams');
const ClusterEvents = _dereq_('../../constants/ClusterEvents');
const ClusterJoiningMode = _dereq_('../../constants/ClusterJoiningMode');
const NodeConnectionEvents = _dereq_('../../constants/NodeConnectionEvents');

/**
 * @author luizssb
 */
function ClusterEventsHandler () {
	ClientEventsHandler.call(this);
	this._clusters = {};

	this._bound_leaveCluster = this._leaveCluster.bind(this);
}

ClusterEventsHandler.prototype = Object.assign(
	Object.create(ClientEventsHandler.prototype),
	{ constructor: ClusterEventsHandler }
);

// Override
ClusterEventsHandler.prototype.handleNewClient = function (client) {
	const leaveCluster = this._bound_leaveCluster;
	const clusters = this._clusters;
	const message = function (toPeerId, event, data) {
		this.sendMessage(
			client, toPeerId, event, this._defaultParams(client, {}), data
		);
	}.bind(this);

	client.on(
		ClusterEvents.ToServer.JOIN_CLUSTER,
		function (data, room, preferredConnection, joiningMode) {
			console.log(ClusterEvents.ToServer.JOIN_CLUSTER);

			if (clusters[room] && clusters[room].get(client.id)) {
				return fail('node already joined cluster');
			}

			if (!client.room) {
				client.join(room);
			}

			const nodeData = {
				id: client.id, connectionMethod: preferredConnection
			};

			joiningMode = joiningMode || ClusterJoiningMode.ANY;
			clusters[room] = clusters[room] || new MasterSlaveInfo();

			var tellSlaves = false;

			if (clusters[room].master) {
				if (joiningMode === ClusterJoiningMode.MASTER) {
					return fail('cluster already has master');
				} else {
					clusters[room].addSlave(nodeData);
				}
			} else if (joiningMode & ClusterJoiningMode.MASTER) {
				clusters[room].setMaster(nodeData);
				tellSlaves = true;
				console.log(`Room '${room}' has master`);
			} else if (joiningMode & ClusterJoiningMode.SLAVE_WAIT) {
				clusters[room].addSlave(nodeData);
				console.log(`Room '${room}' has ${clusters[room].count} participants`);
			} else if (joiningMode & ClusterJoiningMode.SLAVE_NOW) {
				return fail('cluster has no master');
			} else {
				return fail('unknown joining mode');
			}

			client.emit(
				ClusterEvents.FromServer.JOINED_CLUSTER, clusters[room].master
			);

			if (tellSlaves) {
				client.broadcast(ClusterEvents.FromServer.MASTER_JOINED, nodeData);
			}

			function fail (message) {
				client.emit(
					ClusterEvents.FromServer.JOIN_FAILED, { message: message }
				);
			}
		}
	)
		.on(
			NodeConnectionEvents.ToServer.PEEK_CLUSTER,
			function (data, room) {
				const clusterData = {};
				if (clusters[room]) {
					clusterData.participants = clusters.count;
					clusterData.nodes = clusters[room];
				}

				client.emit(ClusterEvents.FromServer.PEEKED, clusterData);
			}
		)
		.on(
			NodeConnectionEvents.ToServer.OFFER_CONNECTION,
			function (data, toPeerId, offer) {
				console.log(
					NodeConnectionEvents.ToServer.OFFER_CONNECTION,
					data, toPeerId
				);
				message(
					toPeerId, NodeConnectionEvents.FromServer.RECEIVED_OFFER,
					offer
				);
			}
		)
		.on(
			NodeConnectionEvents.ToServer.ANSWER_OFFER,
			function (data, toPeerId, answer) {
				console.log(
					NodeConnectionEvents.ToServer.ANSWER_OFFER, data, toPeerId
				);
				message(
					toPeerId, NodeConnectionEvents.FromServer.RECEIVED_ANSWER,
					answer
				);
			}
		)
		.on(
			NodeConnectionEvents.ToServer.FAIL_ANSWER,
			function (data, toPeerId, error) {
				console.log(
					NodeConnectionEvents.ToServer.FAIL_ANSWER, data, toPeerId
				);
				message(
					toPeerId, NodeConnectionEvents.FromServer.ANSWER_FAILED,
					error
				);
			}
		)
		.on(
			NodeConnectionEvents.ToServer.CANDIDATE_PEER,
			function (data, toPeerId, candidate) {
				console.log(
					NodeConnectionEvents.ToServer.CANDIDATE_PEER, data, toPeerId
				);
				message(
					toPeerId, NodeConnectionEvents.FromServer.RECEIVED_CANDIDATE,
					candidate
				);
			}
		)
		.on(ClusterEvents.ToServer.QUIT_CLUSTER, function (data) {
			console.log(ClusterEvents.ToServer.QUIT_CLUSTER, data);
			leaveCluster(client);
		});

	client.on.willEmit.add(this._defaultParams, this);
};

// Override
ClusterEventsHandler.prototype.handleClientGone = function (client) {
	console.log('disconnect');

	const room = client.room;

	if (room) {
		this._leaveCluster(client);
	}
};

ClusterEventsHandler.prototype._generalBroadcast = function (client) {
	const binding = client.manager.on.willBroadcast.add(function (data) {
		defaultParams(client, data);
		this._defaultParams(client, data);
		binding.detach();
	}.bind(this));

	client.manager.broadcast.apply(
		client.manager, Array.prototype.slice.call(arguments, 1)
	);
};

ClusterEventsHandler.prototype._leaveCluster = function (client) {
	if (!this._clusters[client.room]) {
		return;
	}

	console.log('leaveCluster', client.room, this._clusters[client.room].count);

	const room = client.room;

	try {
		client.leave();
	} catch (ex) {}

	const wasMaster = this._clusters[room].master &&
		this._clusters[room].master.id === client.id;

	this._generalBroadcast(
		client, room, ClusterEvents.FromServer.PEER_GONE, wasMaster
	);

	if (wasMaster) {
		console.log(`Room '${room}' dead`);

		for (let clientId in this._clusters[room].slaves) {
			client.manager.get(clientId).leave();
		}

		delete this._clusters[room];
	}
};

ClusterEventsHandler.prototype._defaultParams = function (sender, data) {
	data = data || {};

	if (sender.room && this._clusters[sender.room]) {
		data.nodeData = this._clusters[sender.room].get(sender.id);
	}

	return data;
};

module.exports = ClusterEventsHandler;

},{"../../constants/ClusterEvents":2,"../../constants/ClusterJoiningMode":3,"../../constants/NodeConnectionEvents":6,"../defaultParams":15,"./ClientEventsHandler":19,"./MasterSlaveInfo":21}],21:[function(_dereq_,module,exports){
const ObjectEx = _dereq_('../../utils/ObjectEx');

/**
 * @author luizssb
 */
function MasterSlaveInfo () {
	this.master = undefined;
	this.slaves = {};
}

Object.defineProperty(
	MasterSlaveInfo.prototype, 'count', ObjectEx.readOnly(function () {
		return !!this.master + Object.keys(this.slaves).length;
	})
);

MasterSlaveInfo.prototype.get = function (nodeId) {
	return this.master && nodeId === this.master.id
		? this.master : this.slaves[nodeId];
};

MasterSlaveInfo.prototype.setMaster = function (nodeData) {
	this.master = nodeData;
};

MasterSlaveInfo.prototype.addSlave = function (nodeData) {
	this.slaves[nodeData.id] = nodeData;
};

module.exports = MasterSlaveInfo;

},{"../../utils/ObjectEx":25}],22:[function(_dereq_,module,exports){
const ClientEventsHandler = _dereq_('./ClientEventsHandler');
const NodeEvents = _dereq_('../../constants/NodeEvents');

function NodeEventsHandler () {
	ClientEventsHandler.call(this);
	this._clientsRooms = {};
}

NodeEventsHandler.prototype = Object.assign(
	Object.create(ClientEventsHandler.prototype),
	{ constructor: NodeEventsHandler }
);

// Override
NodeEventsHandler.prototype.handleNewClient = function (client) {
	const $this = this;

	client
		.on(NodeEvents.ToServer.SEND_DATA, function (data, dataPackage) {
			if (dataPackage.recipient) {
				$this.sendMessage(
					client,
					dataPackage.recipient,
					NodeEvents.FromServer.RECEIVED_DATA, {}, dataPackage
				);
			} else {
				client.broadcast(
					NodeEvents.FromServer.RECEIVED_DATA, dataPackage
				);
			}
		})
		.on(NodeEvents.ToServer.QUIT, function (data) {
			client.broadcast(NodeEvents.FromServer.NODE_GONE, client.id);
		});

	this._binding = client.on.joinedRoom.add(function (sender) {
		$this._clientsRooms[sender.id] = sender.room;
	});

	this._clientsRooms[client.id] = client.room;
};

// Override
NodeEventsHandler.prototype.handleClientGone = function (client) {
	client.manager.broadcast(
		this._clientsRooms[client.id],
		NodeEvents.FromServer.NODE_GONE, client.id
	);

	this._binding.detach();

	delete this._clientsRooms[client.id];
};

module.exports = NodeEventsHandler;

},{"../../constants/NodeEvents":7,"./ClientEventsHandler":19}],23:[function(_dereq_,module,exports){
const ClientEventsHandler = _dereq_('./ClientEventsHandler');
const RoomEvents = _dereq_('../../constants/RoomEvents');

function RoomEventsHandler () {
	ClientEventsHandler.call(this);
}

RoomEventsHandler.prototype = Object.assign(
	Object.create(ClientEventsHandler.prototype),
	{ constructor: RoomEventsHandler }
);

// Override
RoomEventsHandler.prototype.handleNewClient = function (client) {
	client
		.on(RoomEvents.ToServer.JOIN, function (data, room) {
			client.join(room);
		})
		.on(RoomEvents.ToServer.LEAVE, function (data) {
			client.leave();
		});
};

module.exports = RoomEventsHandler;

},{"../../constants/RoomEvents":8,"./ClientEventsHandler":19}],24:[function(_dereq_,module,exports){
module.exports = {
	ClientConnection: _dereq_('./connections/ClientConnection'),
	SocketIOServerConnection: _dereq_('./connections/SocketIOClientConnection'),

	RoomEventsHandler: _dereq_('./events/RoomEventsHandler'),
	RoomEvents: _dereq_('../constants/RoomEvents'),

	ClientsManager: _dereq_('./connections/ClientsManager'),
	SocketIOClientsManager: _dereq_('./connections/SocketIOClientsManager'),

	ClientEventsHandler: _dereq_('./events/ClientEventsHandler'),
	ClusterEventsHandler: _dereq_('./events/ClusterEventsHandler'),
	NodeEventsHandler: _dereq_('./events/NodeEventsHandler'),

	WebClusterSupport: _dereq_('./WebClusterSupport'),

	ObjectEx: _dereq_('../utils/ObjectEx'),
	RandomUtils: _dereq_('../utils/RandomUtils'),

	computeTilt: _dereq_('../utils/computeTilt'),
	toDegrees: _dereq_('../utils/toDegrees'),

	DeviceEventsHandler: _dereq_('./devices/DeviceEventsHandler'),
	DeviceEvents: _dereq_('../constants/DeviceEvents'),
	InterfacesFactory: _dereq_('./devices/InterfacesFactory'),
	DefaultInterfacesFactory: _dereq_('./devices/DefaultInterfacesFactory')
};

},{"../constants/DeviceEvents":4,"../constants/RoomEvents":8,"../utils/ObjectEx":25,"../utils/RandomUtils":26,"../utils/computeTilt":27,"../utils/toDegrees":28,"./WebClusterSupport":10,"./connections/ClientConnection":11,"./connections/ClientsManager":12,"./connections/SocketIOClientConnection":13,"./connections/SocketIOClientsManager":14,"./devices/DefaultInterfacesFactory":16,"./devices/DeviceEventsHandler":17,"./devices/InterfacesFactory":18,"./events/ClientEventsHandler":19,"./events/ClusterEventsHandler":20,"./events/NodeEventsHandler":22,"./events/RoomEventsHandler":23}],25:[function(_dereq_,module,exports){
const LivvclibError = _dereq_('../exceptions/LivvclibError.js');
const Signal = _dereq_('signals');

/**
 * @author luizssb
 */
module.exports = {
	callDelegate: function (obj, delegateName) {
		if (obj[delegateName]) {
			obj[delegateName].apply(
				null,
				[obj].concat(Array.prototype.slice.call(arguments, 2))
			);
		}
	},
	abstractMethod: function () {
		throw new LivvclibError('abstract method not implemented', 92);
	},
	notImplemented: function () {
		throw new LivvclibError('not implemented', 39);
	},
	isFunction: function (obj) {
		return obj && {}.toString.call(obj) === '[object Function]';
	},
	isObject: function (obj) {
		return obj && obj.toString().indexOf('[object') === 0;
	},
	isBoolean: function (obj) {
		return typeof obj === 'boolean';
	},
	readOnly: function (get, configurable, enumerable) {
		if (configurable === undefined) {
			configurable = true;
		}

		if (enumerable === undefined) {
			enumerable = true;
		}

		var definition = { configurable: configurable, enumerable: enumerable };

		if (this.isFunction(get)) {
			definition.get = get;
		} else {
			definition.value = get;
		}

		return definition;
	},
	defineProperties: function (obj, properties) {
		for (var property in properties) {
			Object.defineProperty(obj, property, properties[property]);
		}
	},
	defineEvents: function (obj, eventsNames) {
		obj.on = obj.on || {};

		eventsNames.forEach(function (element) {
			obj.on[element] = new Signal();
		});
	},
	evaluateDelegate: function (sender, delegate, args) {
		return this.isFunction(delegate)
			? delegate.apply(
				null, [sender].concat(Array.prototype.slice.call(arguments, 2))
			)
			: delegate;
	},
	enumToString: function (enumType, enumValue) {
		for (var key in enumType) {
			if (enumType[key] === enumValue) {
				return key;
			}
		}

		return '<unknown>';
	}
};

},{"../exceptions/LivvclibError.js":9,"signals":1}],26:[function(_dereq_,module,exports){
/**
 * @author luizssb
 */
module.exports = {
	string: function (length) {
		if (!length) {
			length = 16;
		}

		var alphabet =
			'abcdefghijklmnopqrstuvwxyz' + 'ABCDEFGHIJKLMNOPQRSTUVWXY' +
			'Z0123456789';

		return Array(length)
			.join()
			.split(',')
			.map(
				function () {
					return alphabet.charAt(
						Math.floor(Math.random() * alphabet.length)
					);
				}
			)
			.join('');
	}
};

},{}],27:[function(_dereq_,module,exports){
/**
 * Taken from UIVA's (Unity Indie VRPN Adapter) source code.
 * http://web.cs.wpi.edu/~gogo/hive/UIVA/
 * 
 * @author luizssb
 */
module.exports = function (xToRight, yBackwards, zUpwards) {
	const gravs = {
		x: xToRight,
		y: yBackwards,
		z: zUpwards
	};

	var roll, pitch;

	// Compute roll angle
	const sqrtY2Z2 = Math.sqrt(Math.pow(gravs.y, 2) + Math.pow(gravs.z, 2));

	// Prevent dividing by zero
	if (sqrtY2Z2 === 0) {
		roll = 90;
		if (gravs.x >= 0) {
			roll *= -1;
		}
	} else {
		// The math that works, compute roll angle
		roll = Math.atan(gravs.x / sqrtY2Z2);
	}

	// Compute pitch angle
	const sqrtX2Z2 = Math.sqrt(Math.pow(gravs.x, 2) + Math.pow(gravs.z, 2));

	// Prevent dividing by zero
	if (sqrtX2Z2 === 0) {
		pitch = 90;
		if (gravs.y >= 0) {
			pitch *= -1;
		}
	} else {
		// The math that works, compute pitch angle
		pitch = -Math.atan(gravs.y / sqrtX2Z2);
	}

	return {
		roll: roll,
		pitch: pitch
	};
};

},{}],28:[function(_dereq_,module,exports){
/**
 * @author luizssb
 */
module.exports = function (rad) {
	return rad * 180 / Math.PI;
};

},{}]},{},[24])(24)
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJub2RlX21vZHVsZXMvc2lnbmFscy9kaXN0L3NpZ25hbHMuanMiLCJzcmMvY29uc3RhbnRzL0NsdXN0ZXJFdmVudHMuanMiLCJzcmMvY29uc3RhbnRzL0NsdXN0ZXJKb2luaW5nTW9kZS5qcyIsInNyYy9jb25zdGFudHMvRGV2aWNlRXZlbnRzLmpzIiwic3JjL2NvbnN0YW50cy9EZXZpY2VJbnRlcmZhY2UuanMiLCJzcmMvY29uc3RhbnRzL05vZGVDb25uZWN0aW9uRXZlbnRzLmpzIiwic3JjL2NvbnN0YW50cy9Ob2RlRXZlbnRzLmpzIiwic3JjL2NvbnN0YW50cy9Sb29tRXZlbnRzLmpzIiwic3JjL2V4Y2VwdGlvbnMvTGl2dmNsaWJFcnJvci5qcyIsInNyYy9zZXJ2ZXIvV2ViQ2x1c3RlclN1cHBvcnQuanMiLCJzcmMvc2VydmVyL2Nvbm5lY3Rpb25zL0NsaWVudENvbm5lY3Rpb24uanMiLCJzcmMvc2VydmVyL2Nvbm5lY3Rpb25zL0NsaWVudHNNYW5hZ2VyLmpzIiwic3JjL3NlcnZlci9jb25uZWN0aW9ucy9Tb2NrZXRJT0NsaWVudENvbm5lY3Rpb24uanMiLCJzcmMvc2VydmVyL2Nvbm5lY3Rpb25zL1NvY2tldElPQ2xpZW50c01hbmFnZXIuanMiLCJzcmMvc2VydmVyL2RlZmF1bHRQYXJhbXMuanMiLCJzcmMvc2VydmVyL2RldmljZXMvRGVmYXVsdEludGVyZmFjZXNGYWN0b3J5LmpzIiwic3JjL3NlcnZlci9kZXZpY2VzL0RldmljZUV2ZW50c0hhbmRsZXIuanMiLCJzcmMvc2VydmVyL2RldmljZXMvSW50ZXJmYWNlc0ZhY3RvcnkuanMiLCJzcmMvc2VydmVyL2V2ZW50cy9DbGllbnRFdmVudHNIYW5kbGVyLmpzIiwic3JjL3NlcnZlci9ldmVudHMvQ2x1c3RlckV2ZW50c0hhbmRsZXIuanMiLCJzcmMvc2VydmVyL2V2ZW50cy9NYXN0ZXJTbGF2ZUluZm8uanMiLCJzcmMvc2VydmVyL2V2ZW50cy9Ob2RlRXZlbnRzSGFuZGxlci5qcyIsInNyYy9zZXJ2ZXIvZXZlbnRzL1Jvb21FdmVudHNIYW5kbGVyLmpzIiwic3JjL3NlcnZlci9pbmRleC5qcyIsInNyYy91dGlscy9PYmplY3RFeC5qcyIsInNyYy91dGlscy9SYW5kb21VdGlscy5qcyIsInNyYy91dGlscy9jb21wdXRlVGlsdC5qcyIsInNyYy91dGlscy90b0RlZ3JlZXMuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzdiQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDakJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ1RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2pCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ1RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3hCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM3Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzFIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3RFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDekZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDdkRBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNqQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDakNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ25QQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN4QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDakNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzlOQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM5QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN2REE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDeEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzNCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM5RUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzFCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNoREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiLypqc2xpbnQgb25ldmFyOnRydWUsIHVuZGVmOnRydWUsIG5ld2NhcDp0cnVlLCByZWdleHA6dHJ1ZSwgYml0d2lzZTp0cnVlLCBtYXhlcnI6NTAsIGluZGVudDo0LCB3aGl0ZTpmYWxzZSwgbm9tZW46ZmFsc2UsIHBsdXNwbHVzOmZhbHNlICovXG4vKmdsb2JhbCBkZWZpbmU6ZmFsc2UsIHJlcXVpcmU6ZmFsc2UsIGV4cG9ydHM6ZmFsc2UsIG1vZHVsZTpmYWxzZSwgc2lnbmFsczpmYWxzZSAqL1xuXG4vKiogQGxpY2Vuc2VcbiAqIEpTIFNpZ25hbHMgPGh0dHA6Ly9taWxsZXJtZWRlaXJvcy5naXRodWIuY29tL2pzLXNpZ25hbHMvPlxuICogUmVsZWFzZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlXG4gKiBBdXRob3I6IE1pbGxlciBNZWRlaXJvc1xuICogVmVyc2lvbjogMS4wLjAgLSBCdWlsZDogMjY4ICgyMDEyLzExLzI5IDA1OjQ4IFBNKVxuICovXG5cbihmdW5jdGlvbihnbG9iYWwpe1xuXG4gICAgLy8gU2lnbmFsQmluZGluZyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAgLy89PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICAvKipcbiAgICAgKiBPYmplY3QgdGhhdCByZXByZXNlbnRzIGEgYmluZGluZyBiZXR3ZWVuIGEgU2lnbmFsIGFuZCBhIGxpc3RlbmVyIGZ1bmN0aW9uLlxuICAgICAqIDxiciAvPi0gPHN0cm9uZz5UaGlzIGlzIGFuIGludGVybmFsIGNvbnN0cnVjdG9yIGFuZCBzaG91bGRuJ3QgYmUgY2FsbGVkIGJ5IHJlZ3VsYXIgdXNlcnMuPC9zdHJvbmc+XG4gICAgICogPGJyIC8+LSBpbnNwaXJlZCBieSBKb2EgRWJlcnQgQVMzIFNpZ25hbEJpbmRpbmcgYW5kIFJvYmVydCBQZW5uZXIncyBTbG90IGNsYXNzZXMuXG4gICAgICogQGF1dGhvciBNaWxsZXIgTWVkZWlyb3NcbiAgICAgKiBAY29uc3RydWN0b3JcbiAgICAgKiBAaW50ZXJuYWxcbiAgICAgKiBAbmFtZSBTaWduYWxCaW5kaW5nXG4gICAgICogQHBhcmFtIHtTaWduYWx9IHNpZ25hbCBSZWZlcmVuY2UgdG8gU2lnbmFsIG9iamVjdCB0aGF0IGxpc3RlbmVyIGlzIGN1cnJlbnRseSBib3VuZCB0by5cbiAgICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBsaXN0ZW5lciBIYW5kbGVyIGZ1bmN0aW9uIGJvdW5kIHRvIHRoZSBzaWduYWwuXG4gICAgICogQHBhcmFtIHtib29sZWFufSBpc09uY2UgSWYgYmluZGluZyBzaG91bGQgYmUgZXhlY3V0ZWQganVzdCBvbmNlLlxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBbbGlzdGVuZXJDb250ZXh0XSBDb250ZXh0IG9uIHdoaWNoIGxpc3RlbmVyIHdpbGwgYmUgZXhlY3V0ZWQgKG9iamVjdCB0aGF0IHNob3VsZCByZXByZXNlbnQgdGhlIGB0aGlzYCB2YXJpYWJsZSBpbnNpZGUgbGlzdGVuZXIgZnVuY3Rpb24pLlxuICAgICAqIEBwYXJhbSB7TnVtYmVyfSBbcHJpb3JpdHldIFRoZSBwcmlvcml0eSBsZXZlbCBvZiB0aGUgZXZlbnQgbGlzdGVuZXIuIChkZWZhdWx0ID0gMCkuXG4gICAgICovXG4gICAgZnVuY3Rpb24gU2lnbmFsQmluZGluZyhzaWduYWwsIGxpc3RlbmVyLCBpc09uY2UsIGxpc3RlbmVyQ29udGV4dCwgcHJpb3JpdHkpIHtcblxuICAgICAgICAvKipcbiAgICAgICAgICogSGFuZGxlciBmdW5jdGlvbiBib3VuZCB0byB0aGUgc2lnbmFsLlxuICAgICAgICAgKiBAdHlwZSBGdW5jdGlvblxuICAgICAgICAgKiBAcHJpdmF0ZVxuICAgICAgICAgKi9cbiAgICAgICAgdGhpcy5fbGlzdGVuZXIgPSBsaXN0ZW5lcjtcblxuICAgICAgICAvKipcbiAgICAgICAgICogSWYgYmluZGluZyBzaG91bGQgYmUgZXhlY3V0ZWQganVzdCBvbmNlLlxuICAgICAgICAgKiBAdHlwZSBib29sZWFuXG4gICAgICAgICAqIEBwcml2YXRlXG4gICAgICAgICAqL1xuICAgICAgICB0aGlzLl9pc09uY2UgPSBpc09uY2U7XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIENvbnRleHQgb24gd2hpY2ggbGlzdGVuZXIgd2lsbCBiZSBleGVjdXRlZCAob2JqZWN0IHRoYXQgc2hvdWxkIHJlcHJlc2VudCB0aGUgYHRoaXNgIHZhcmlhYmxlIGluc2lkZSBsaXN0ZW5lciBmdW5jdGlvbikuXG4gICAgICAgICAqIEBtZW1iZXJPZiBTaWduYWxCaW5kaW5nLnByb3RvdHlwZVxuICAgICAgICAgKiBAbmFtZSBjb250ZXh0XG4gICAgICAgICAqIEB0eXBlIE9iamVjdHx1bmRlZmluZWR8bnVsbFxuICAgICAgICAgKi9cbiAgICAgICAgdGhpcy5jb250ZXh0ID0gbGlzdGVuZXJDb250ZXh0O1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBSZWZlcmVuY2UgdG8gU2lnbmFsIG9iamVjdCB0aGF0IGxpc3RlbmVyIGlzIGN1cnJlbnRseSBib3VuZCB0by5cbiAgICAgICAgICogQHR5cGUgU2lnbmFsXG4gICAgICAgICAqIEBwcml2YXRlXG4gICAgICAgICAqL1xuICAgICAgICB0aGlzLl9zaWduYWwgPSBzaWduYWw7XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIExpc3RlbmVyIHByaW9yaXR5XG4gICAgICAgICAqIEB0eXBlIE51bWJlclxuICAgICAgICAgKiBAcHJpdmF0ZVxuICAgICAgICAgKi9cbiAgICAgICAgdGhpcy5fcHJpb3JpdHkgPSBwcmlvcml0eSB8fCAwO1xuICAgIH1cblxuICAgIFNpZ25hbEJpbmRpbmcucHJvdG90eXBlID0ge1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBJZiBiaW5kaW5nIGlzIGFjdGl2ZSBhbmQgc2hvdWxkIGJlIGV4ZWN1dGVkLlxuICAgICAgICAgKiBAdHlwZSBib29sZWFuXG4gICAgICAgICAqL1xuICAgICAgICBhY3RpdmUgOiB0cnVlLFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBEZWZhdWx0IHBhcmFtZXRlcnMgcGFzc2VkIHRvIGxpc3RlbmVyIGR1cmluZyBgU2lnbmFsLmRpc3BhdGNoYCBhbmQgYFNpZ25hbEJpbmRpbmcuZXhlY3V0ZWAuIChjdXJyaWVkIHBhcmFtZXRlcnMpXG4gICAgICAgICAqIEB0eXBlIEFycmF5fG51bGxcbiAgICAgICAgICovXG4gICAgICAgIHBhcmFtcyA6IG51bGwsXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIENhbGwgbGlzdGVuZXIgcGFzc2luZyBhcmJpdHJhcnkgcGFyYW1ldGVycy5cbiAgICAgICAgICogPHA+SWYgYmluZGluZyB3YXMgYWRkZWQgdXNpbmcgYFNpZ25hbC5hZGRPbmNlKClgIGl0IHdpbGwgYmUgYXV0b21hdGljYWxseSByZW1vdmVkIGZyb20gc2lnbmFsIGRpc3BhdGNoIHF1ZXVlLCB0aGlzIG1ldGhvZCBpcyB1c2VkIGludGVybmFsbHkgZm9yIHRoZSBzaWduYWwgZGlzcGF0Y2guPC9wPlxuICAgICAgICAgKiBAcGFyYW0ge0FycmF5fSBbcGFyYW1zQXJyXSBBcnJheSBvZiBwYXJhbWV0ZXJzIHRoYXQgc2hvdWxkIGJlIHBhc3NlZCB0byB0aGUgbGlzdGVuZXJcbiAgICAgICAgICogQHJldHVybiB7Kn0gVmFsdWUgcmV0dXJuZWQgYnkgdGhlIGxpc3RlbmVyLlxuICAgICAgICAgKi9cbiAgICAgICAgZXhlY3V0ZSA6IGZ1bmN0aW9uIChwYXJhbXNBcnIpIHtcbiAgICAgICAgICAgIHZhciBoYW5kbGVyUmV0dXJuLCBwYXJhbXM7XG4gICAgICAgICAgICBpZiAodGhpcy5hY3RpdmUgJiYgISF0aGlzLl9saXN0ZW5lcikge1xuICAgICAgICAgICAgICAgIHBhcmFtcyA9IHRoaXMucGFyYW1zPyB0aGlzLnBhcmFtcy5jb25jYXQocGFyYW1zQXJyKSA6IHBhcmFtc0FycjtcbiAgICAgICAgICAgICAgICBoYW5kbGVyUmV0dXJuID0gdGhpcy5fbGlzdGVuZXIuYXBwbHkodGhpcy5jb250ZXh0LCBwYXJhbXMpO1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLl9pc09uY2UpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kZXRhY2goKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gaGFuZGxlclJldHVybjtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogRGV0YWNoIGJpbmRpbmcgZnJvbSBzaWduYWwuXG4gICAgICAgICAqIC0gYWxpYXMgdG86IG15U2lnbmFsLnJlbW92ZShteUJpbmRpbmcuZ2V0TGlzdGVuZXIoKSk7XG4gICAgICAgICAqIEByZXR1cm4ge0Z1bmN0aW9ufG51bGx9IEhhbmRsZXIgZnVuY3Rpb24gYm91bmQgdG8gdGhlIHNpZ25hbCBvciBgbnVsbGAgaWYgYmluZGluZyB3YXMgcHJldmlvdXNseSBkZXRhY2hlZC5cbiAgICAgICAgICovXG4gICAgICAgIGRldGFjaCA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmlzQm91bmQoKT8gdGhpcy5fc2lnbmFsLnJlbW92ZSh0aGlzLl9saXN0ZW5lciwgdGhpcy5jb250ZXh0KSA6IG51bGw7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEByZXR1cm4ge0Jvb2xlYW59IGB0cnVlYCBpZiBiaW5kaW5nIGlzIHN0aWxsIGJvdW5kIHRvIHRoZSBzaWduYWwgYW5kIGhhdmUgYSBsaXN0ZW5lci5cbiAgICAgICAgICovXG4gICAgICAgIGlzQm91bmQgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gKCEhdGhpcy5fc2lnbmFsICYmICEhdGhpcy5fbGlzdGVuZXIpO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBAcmV0dXJuIHtib29sZWFufSBJZiBTaWduYWxCaW5kaW5nIHdpbGwgb25seSBiZSBleGVjdXRlZCBvbmNlLlxuICAgICAgICAgKi9cbiAgICAgICAgaXNPbmNlIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2lzT25jZTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogQHJldHVybiB7RnVuY3Rpb259IEhhbmRsZXIgZnVuY3Rpb24gYm91bmQgdG8gdGhlIHNpZ25hbC5cbiAgICAgICAgICovXG4gICAgICAgIGdldExpc3RlbmVyIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2xpc3RlbmVyO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBAcmV0dXJuIHtTaWduYWx9IFNpZ25hbCB0aGF0IGxpc3RlbmVyIGlzIGN1cnJlbnRseSBib3VuZCB0by5cbiAgICAgICAgICovXG4gICAgICAgIGdldFNpZ25hbCA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9zaWduYWw7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIERlbGV0ZSBpbnN0YW5jZSBwcm9wZXJ0aWVzXG4gICAgICAgICAqIEBwcml2YXRlXG4gICAgICAgICAqL1xuICAgICAgICBfZGVzdHJveSA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGRlbGV0ZSB0aGlzLl9zaWduYWw7XG4gICAgICAgICAgICBkZWxldGUgdGhpcy5fbGlzdGVuZXI7XG4gICAgICAgICAgICBkZWxldGUgdGhpcy5jb250ZXh0O1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBAcmV0dXJuIHtzdHJpbmd9IFN0cmluZyByZXByZXNlbnRhdGlvbiBvZiB0aGUgb2JqZWN0LlxuICAgICAgICAgKi9cbiAgICAgICAgdG9TdHJpbmcgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gJ1tTaWduYWxCaW5kaW5nIGlzT25jZTonICsgdGhpcy5faXNPbmNlICsnLCBpc0JvdW5kOicrIHRoaXMuaXNCb3VuZCgpICsnLCBhY3RpdmU6JyArIHRoaXMuYWN0aXZlICsgJ10nO1xuICAgICAgICB9XG5cbiAgICB9O1xuXG5cbi8qZ2xvYmFsIFNpZ25hbEJpbmRpbmc6ZmFsc2UqL1xuXG4gICAgLy8gU2lnbmFsIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICAgLy89PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbiAgICBmdW5jdGlvbiB2YWxpZGF0ZUxpc3RlbmVyKGxpc3RlbmVyLCBmbk5hbWUpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBsaXN0ZW5lciAhPT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCAnbGlzdGVuZXIgaXMgYSByZXF1aXJlZCBwYXJhbSBvZiB7Zm59KCkgYW5kIHNob3VsZCBiZSBhIEZ1bmN0aW9uLicucmVwbGFjZSgne2ZufScsIGZuTmFtZSkgKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEN1c3RvbSBldmVudCBicm9hZGNhc3RlclxuICAgICAqIDxiciAvPi0gaW5zcGlyZWQgYnkgUm9iZXJ0IFBlbm5lcidzIEFTMyBTaWduYWxzLlxuICAgICAqIEBuYW1lIFNpZ25hbFxuICAgICAqIEBhdXRob3IgTWlsbGVyIE1lZGVpcm9zXG4gICAgICogQGNvbnN0cnVjdG9yXG4gICAgICovXG4gICAgZnVuY3Rpb24gU2lnbmFsKCkge1xuICAgICAgICAvKipcbiAgICAgICAgICogQHR5cGUgQXJyYXkuPFNpZ25hbEJpbmRpbmc+XG4gICAgICAgICAqIEBwcml2YXRlXG4gICAgICAgICAqL1xuICAgICAgICB0aGlzLl9iaW5kaW5ncyA9IFtdO1xuICAgICAgICB0aGlzLl9wcmV2UGFyYW1zID0gbnVsbDtcblxuICAgICAgICAvLyBlbmZvcmNlIGRpc3BhdGNoIHRvIGF3YXlzIHdvcmsgb24gc2FtZSBjb250ZXh0ICgjNDcpXG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcbiAgICAgICAgdGhpcy5kaXNwYXRjaCA9IGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICBTaWduYWwucHJvdG90eXBlLmRpc3BhdGNoLmFwcGx5KHNlbGYsIGFyZ3VtZW50cyk7XG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgU2lnbmFsLnByb3RvdHlwZSA9IHtcblxuICAgICAgICAvKipcbiAgICAgICAgICogU2lnbmFscyBWZXJzaW9uIE51bWJlclxuICAgICAgICAgKiBAdHlwZSBTdHJpbmdcbiAgICAgICAgICogQGNvbnN0XG4gICAgICAgICAqL1xuICAgICAgICBWRVJTSU9OIDogJzEuMC4wJyxcblxuICAgICAgICAvKipcbiAgICAgICAgICogSWYgU2lnbmFsIHNob3VsZCBrZWVwIHJlY29yZCBvZiBwcmV2aW91c2x5IGRpc3BhdGNoZWQgcGFyYW1ldGVycyBhbmRcbiAgICAgICAgICogYXV0b21hdGljYWxseSBleGVjdXRlIGxpc3RlbmVyIGR1cmluZyBgYWRkKClgL2BhZGRPbmNlKClgIGlmIFNpZ25hbCB3YXNcbiAgICAgICAgICogYWxyZWFkeSBkaXNwYXRjaGVkIGJlZm9yZS5cbiAgICAgICAgICogQHR5cGUgYm9vbGVhblxuICAgICAgICAgKi9cbiAgICAgICAgbWVtb3JpemUgOiBmYWxzZSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogQHR5cGUgYm9vbGVhblxuICAgICAgICAgKiBAcHJpdmF0ZVxuICAgICAgICAgKi9cbiAgICAgICAgX3Nob3VsZFByb3BhZ2F0ZSA6IHRydWUsXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIElmIFNpZ25hbCBpcyBhY3RpdmUgYW5kIHNob3VsZCBicm9hZGNhc3QgZXZlbnRzLlxuICAgICAgICAgKiA8cD48c3Ryb25nPklNUE9SVEFOVDo8L3N0cm9uZz4gU2V0dGluZyB0aGlzIHByb3BlcnR5IGR1cmluZyBhIGRpc3BhdGNoIHdpbGwgb25seSBhZmZlY3QgdGhlIG5leHQgZGlzcGF0Y2gsIGlmIHlvdSB3YW50IHRvIHN0b3AgdGhlIHByb3BhZ2F0aW9uIG9mIGEgc2lnbmFsIHVzZSBgaGFsdCgpYCBpbnN0ZWFkLjwvcD5cbiAgICAgICAgICogQHR5cGUgYm9vbGVhblxuICAgICAgICAgKi9cbiAgICAgICAgYWN0aXZlIDogdHJ1ZSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogQHBhcmFtIHtGdW5jdGlvbn0gbGlzdGVuZXJcbiAgICAgICAgICogQHBhcmFtIHtib29sZWFufSBpc09uY2VcbiAgICAgICAgICogQHBhcmFtIHtPYmplY3R9IFtsaXN0ZW5lckNvbnRleHRdXG4gICAgICAgICAqIEBwYXJhbSB7TnVtYmVyfSBbcHJpb3JpdHldXG4gICAgICAgICAqIEByZXR1cm4ge1NpZ25hbEJpbmRpbmd9XG4gICAgICAgICAqIEBwcml2YXRlXG4gICAgICAgICAqL1xuICAgICAgICBfcmVnaXN0ZXJMaXN0ZW5lciA6IGZ1bmN0aW9uIChsaXN0ZW5lciwgaXNPbmNlLCBsaXN0ZW5lckNvbnRleHQsIHByaW9yaXR5KSB7XG5cbiAgICAgICAgICAgIHZhciBwcmV2SW5kZXggPSB0aGlzLl9pbmRleE9mTGlzdGVuZXIobGlzdGVuZXIsIGxpc3RlbmVyQ29udGV4dCksXG4gICAgICAgICAgICAgICAgYmluZGluZztcblxuICAgICAgICAgICAgaWYgKHByZXZJbmRleCAhPT0gLTEpIHtcbiAgICAgICAgICAgICAgICBiaW5kaW5nID0gdGhpcy5fYmluZGluZ3NbcHJldkluZGV4XTtcbiAgICAgICAgICAgICAgICBpZiAoYmluZGluZy5pc09uY2UoKSAhPT0gaXNPbmNlKSB7XG4gICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignWW91IGNhbm5vdCBhZGQnKyAoaXNPbmNlPyAnJyA6ICdPbmNlJykgKycoKSB0aGVuIGFkZCcrICghaXNPbmNlPyAnJyA6ICdPbmNlJykgKycoKSB0aGUgc2FtZSBsaXN0ZW5lciB3aXRob3V0IHJlbW92aW5nIHRoZSByZWxhdGlvbnNoaXAgZmlyc3QuJyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBiaW5kaW5nID0gbmV3IFNpZ25hbEJpbmRpbmcodGhpcywgbGlzdGVuZXIsIGlzT25jZSwgbGlzdGVuZXJDb250ZXh0LCBwcmlvcml0eSk7XG4gICAgICAgICAgICAgICAgdGhpcy5fYWRkQmluZGluZyhiaW5kaW5nKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYodGhpcy5tZW1vcml6ZSAmJiB0aGlzLl9wcmV2UGFyYW1zKXtcbiAgICAgICAgICAgICAgICBiaW5kaW5nLmV4ZWN1dGUodGhpcy5fcHJldlBhcmFtcyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiBiaW5kaW5nO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBAcGFyYW0ge1NpZ25hbEJpbmRpbmd9IGJpbmRpbmdcbiAgICAgICAgICogQHByaXZhdGVcbiAgICAgICAgICovXG4gICAgICAgIF9hZGRCaW5kaW5nIDogZnVuY3Rpb24gKGJpbmRpbmcpIHtcbiAgICAgICAgICAgIC8vc2ltcGxpZmllZCBpbnNlcnRpb24gc29ydFxuICAgICAgICAgICAgdmFyIG4gPSB0aGlzLl9iaW5kaW5ncy5sZW5ndGg7XG4gICAgICAgICAgICBkbyB7IC0tbjsgfSB3aGlsZSAodGhpcy5fYmluZGluZ3Nbbl0gJiYgYmluZGluZy5fcHJpb3JpdHkgPD0gdGhpcy5fYmluZGluZ3Nbbl0uX3ByaW9yaXR5KTtcbiAgICAgICAgICAgIHRoaXMuX2JpbmRpbmdzLnNwbGljZShuICsgMSwgMCwgYmluZGluZyk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEBwYXJhbSB7RnVuY3Rpb259IGxpc3RlbmVyXG4gICAgICAgICAqIEByZXR1cm4ge251bWJlcn1cbiAgICAgICAgICogQHByaXZhdGVcbiAgICAgICAgICovXG4gICAgICAgIF9pbmRleE9mTGlzdGVuZXIgOiBmdW5jdGlvbiAobGlzdGVuZXIsIGNvbnRleHQpIHtcbiAgICAgICAgICAgIHZhciBuID0gdGhpcy5fYmluZGluZ3MubGVuZ3RoLFxuICAgICAgICAgICAgICAgIGN1cjtcbiAgICAgICAgICAgIHdoaWxlIChuLS0pIHtcbiAgICAgICAgICAgICAgICBjdXIgPSB0aGlzLl9iaW5kaW5nc1tuXTtcbiAgICAgICAgICAgICAgICBpZiAoY3VyLl9saXN0ZW5lciA9PT0gbGlzdGVuZXIgJiYgY3VyLmNvbnRleHQgPT09IGNvbnRleHQpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIC0xO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBDaGVjayBpZiBsaXN0ZW5lciB3YXMgYXR0YWNoZWQgdG8gU2lnbmFsLlxuICAgICAgICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBsaXN0ZW5lclxuICAgICAgICAgKiBAcGFyYW0ge09iamVjdH0gW2NvbnRleHRdXG4gICAgICAgICAqIEByZXR1cm4ge2Jvb2xlYW59IGlmIFNpZ25hbCBoYXMgdGhlIHNwZWNpZmllZCBsaXN0ZW5lci5cbiAgICAgICAgICovXG4gICAgICAgIGhhcyA6IGZ1bmN0aW9uIChsaXN0ZW5lciwgY29udGV4dCkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2luZGV4T2ZMaXN0ZW5lcihsaXN0ZW5lciwgY29udGV4dCkgIT09IC0xO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBBZGQgYSBsaXN0ZW5lciB0byB0aGUgc2lnbmFsLlxuICAgICAgICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBsaXN0ZW5lciBTaWduYWwgaGFuZGxlciBmdW5jdGlvbi5cbiAgICAgICAgICogQHBhcmFtIHtPYmplY3R9IFtsaXN0ZW5lckNvbnRleHRdIENvbnRleHQgb24gd2hpY2ggbGlzdGVuZXIgd2lsbCBiZSBleGVjdXRlZCAob2JqZWN0IHRoYXQgc2hvdWxkIHJlcHJlc2VudCB0aGUgYHRoaXNgIHZhcmlhYmxlIGluc2lkZSBsaXN0ZW5lciBmdW5jdGlvbikuXG4gICAgICAgICAqIEBwYXJhbSB7TnVtYmVyfSBbcHJpb3JpdHldIFRoZSBwcmlvcml0eSBsZXZlbCBvZiB0aGUgZXZlbnQgbGlzdGVuZXIuIExpc3RlbmVycyB3aXRoIGhpZ2hlciBwcmlvcml0eSB3aWxsIGJlIGV4ZWN1dGVkIGJlZm9yZSBsaXN0ZW5lcnMgd2l0aCBsb3dlciBwcmlvcml0eS4gTGlzdGVuZXJzIHdpdGggc2FtZSBwcmlvcml0eSBsZXZlbCB3aWxsIGJlIGV4ZWN1dGVkIGF0IHRoZSBzYW1lIG9yZGVyIGFzIHRoZXkgd2VyZSBhZGRlZC4gKGRlZmF1bHQgPSAwKVxuICAgICAgICAgKiBAcmV0dXJuIHtTaWduYWxCaW5kaW5nfSBBbiBPYmplY3QgcmVwcmVzZW50aW5nIHRoZSBiaW5kaW5nIGJldHdlZW4gdGhlIFNpZ25hbCBhbmQgbGlzdGVuZXIuXG4gICAgICAgICAqL1xuICAgICAgICBhZGQgOiBmdW5jdGlvbiAobGlzdGVuZXIsIGxpc3RlbmVyQ29udGV4dCwgcHJpb3JpdHkpIHtcbiAgICAgICAgICAgIHZhbGlkYXRlTGlzdGVuZXIobGlzdGVuZXIsICdhZGQnKTtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9yZWdpc3Rlckxpc3RlbmVyKGxpc3RlbmVyLCBmYWxzZSwgbGlzdGVuZXJDb250ZXh0LCBwcmlvcml0eSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEFkZCBsaXN0ZW5lciB0byB0aGUgc2lnbmFsIHRoYXQgc2hvdWxkIGJlIHJlbW92ZWQgYWZ0ZXIgZmlyc3QgZXhlY3V0aW9uICh3aWxsIGJlIGV4ZWN1dGVkIG9ubHkgb25jZSkuXG4gICAgICAgICAqIEBwYXJhbSB7RnVuY3Rpb259IGxpc3RlbmVyIFNpZ25hbCBoYW5kbGVyIGZ1bmN0aW9uLlxuICAgICAgICAgKiBAcGFyYW0ge09iamVjdH0gW2xpc3RlbmVyQ29udGV4dF0gQ29udGV4dCBvbiB3aGljaCBsaXN0ZW5lciB3aWxsIGJlIGV4ZWN1dGVkIChvYmplY3QgdGhhdCBzaG91bGQgcmVwcmVzZW50IHRoZSBgdGhpc2AgdmFyaWFibGUgaW5zaWRlIGxpc3RlbmVyIGZ1bmN0aW9uKS5cbiAgICAgICAgICogQHBhcmFtIHtOdW1iZXJ9IFtwcmlvcml0eV0gVGhlIHByaW9yaXR5IGxldmVsIG9mIHRoZSBldmVudCBsaXN0ZW5lci4gTGlzdGVuZXJzIHdpdGggaGlnaGVyIHByaW9yaXR5IHdpbGwgYmUgZXhlY3V0ZWQgYmVmb3JlIGxpc3RlbmVycyB3aXRoIGxvd2VyIHByaW9yaXR5LiBMaXN0ZW5lcnMgd2l0aCBzYW1lIHByaW9yaXR5IGxldmVsIHdpbGwgYmUgZXhlY3V0ZWQgYXQgdGhlIHNhbWUgb3JkZXIgYXMgdGhleSB3ZXJlIGFkZGVkLiAoZGVmYXVsdCA9IDApXG4gICAgICAgICAqIEByZXR1cm4ge1NpZ25hbEJpbmRpbmd9IEFuIE9iamVjdCByZXByZXNlbnRpbmcgdGhlIGJpbmRpbmcgYmV0d2VlbiB0aGUgU2lnbmFsIGFuZCBsaXN0ZW5lci5cbiAgICAgICAgICovXG4gICAgICAgIGFkZE9uY2UgOiBmdW5jdGlvbiAobGlzdGVuZXIsIGxpc3RlbmVyQ29udGV4dCwgcHJpb3JpdHkpIHtcbiAgICAgICAgICAgIHZhbGlkYXRlTGlzdGVuZXIobGlzdGVuZXIsICdhZGRPbmNlJyk7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fcmVnaXN0ZXJMaXN0ZW5lcihsaXN0ZW5lciwgdHJ1ZSwgbGlzdGVuZXJDb250ZXh0LCBwcmlvcml0eSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFJlbW92ZSBhIHNpbmdsZSBsaXN0ZW5lciBmcm9tIHRoZSBkaXNwYXRjaCBxdWV1ZS5cbiAgICAgICAgICogQHBhcmFtIHtGdW5jdGlvbn0gbGlzdGVuZXIgSGFuZGxlciBmdW5jdGlvbiB0aGF0IHNob3VsZCBiZSByZW1vdmVkLlxuICAgICAgICAgKiBAcGFyYW0ge09iamVjdH0gW2NvbnRleHRdIEV4ZWN1dGlvbiBjb250ZXh0IChzaW5jZSB5b3UgY2FuIGFkZCB0aGUgc2FtZSBoYW5kbGVyIG11bHRpcGxlIHRpbWVzIGlmIGV4ZWN1dGluZyBpbiBhIGRpZmZlcmVudCBjb250ZXh0KS5cbiAgICAgICAgICogQHJldHVybiB7RnVuY3Rpb259IExpc3RlbmVyIGhhbmRsZXIgZnVuY3Rpb24uXG4gICAgICAgICAqL1xuICAgICAgICByZW1vdmUgOiBmdW5jdGlvbiAobGlzdGVuZXIsIGNvbnRleHQpIHtcbiAgICAgICAgICAgIHZhbGlkYXRlTGlzdGVuZXIobGlzdGVuZXIsICdyZW1vdmUnKTtcblxuICAgICAgICAgICAgdmFyIGkgPSB0aGlzLl9pbmRleE9mTGlzdGVuZXIobGlzdGVuZXIsIGNvbnRleHQpO1xuICAgICAgICAgICAgaWYgKGkgIT09IC0xKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5fYmluZGluZ3NbaV0uX2Rlc3Ryb3koKTsgLy9ubyByZWFzb24gdG8gYSBTaWduYWxCaW5kaW5nIGV4aXN0IGlmIGl0IGlzbid0IGF0dGFjaGVkIHRvIGEgc2lnbmFsXG4gICAgICAgICAgICAgICAgdGhpcy5fYmluZGluZ3Muc3BsaWNlKGksIDEpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGxpc3RlbmVyO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBSZW1vdmUgYWxsIGxpc3RlbmVycyBmcm9tIHRoZSBTaWduYWwuXG4gICAgICAgICAqL1xuICAgICAgICByZW1vdmVBbGwgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgbiA9IHRoaXMuX2JpbmRpbmdzLmxlbmd0aDtcbiAgICAgICAgICAgIHdoaWxlIChuLS0pIHtcbiAgICAgICAgICAgICAgICB0aGlzLl9iaW5kaW5nc1tuXS5fZGVzdHJveSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5fYmluZGluZ3MubGVuZ3RoID0gMDtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogQHJldHVybiB7bnVtYmVyfSBOdW1iZXIgb2YgbGlzdGVuZXJzIGF0dGFjaGVkIHRvIHRoZSBTaWduYWwuXG4gICAgICAgICAqL1xuICAgICAgICBnZXROdW1MaXN0ZW5lcnMgOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fYmluZGluZ3MubGVuZ3RoO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBTdG9wIHByb3BhZ2F0aW9uIG9mIHRoZSBldmVudCwgYmxvY2tpbmcgdGhlIGRpc3BhdGNoIHRvIG5leHQgbGlzdGVuZXJzIG9uIHRoZSBxdWV1ZS5cbiAgICAgICAgICogPHA+PHN0cm9uZz5JTVBPUlRBTlQ6PC9zdHJvbmc+IHNob3VsZCBiZSBjYWxsZWQgb25seSBkdXJpbmcgc2lnbmFsIGRpc3BhdGNoLCBjYWxsaW5nIGl0IGJlZm9yZS9hZnRlciBkaXNwYXRjaCB3b24ndCBhZmZlY3Qgc2lnbmFsIGJyb2FkY2FzdC48L3A+XG4gICAgICAgICAqIEBzZWUgU2lnbmFsLnByb3RvdHlwZS5kaXNhYmxlXG4gICAgICAgICAqL1xuICAgICAgICBoYWx0IDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdGhpcy5fc2hvdWxkUHJvcGFnYXRlID0gZmFsc2U7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIERpc3BhdGNoL0Jyb2FkY2FzdCBTaWduYWwgdG8gYWxsIGxpc3RlbmVycyBhZGRlZCB0byB0aGUgcXVldWUuXG4gICAgICAgICAqIEBwYXJhbSB7Li4uKn0gW3BhcmFtc10gUGFyYW1ldGVycyB0aGF0IHNob3VsZCBiZSBwYXNzZWQgdG8gZWFjaCBoYW5kbGVyLlxuICAgICAgICAgKi9cbiAgICAgICAgZGlzcGF0Y2ggOiBmdW5jdGlvbiAocGFyYW1zKSB7XG4gICAgICAgICAgICBpZiAoISB0aGlzLmFjdGl2ZSkge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdmFyIHBhcmFtc0FyciA9IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cyksXG4gICAgICAgICAgICAgICAgbiA9IHRoaXMuX2JpbmRpbmdzLmxlbmd0aCxcbiAgICAgICAgICAgICAgICBiaW5kaW5ncztcblxuICAgICAgICAgICAgaWYgKHRoaXMubWVtb3JpemUpIHtcbiAgICAgICAgICAgICAgICB0aGlzLl9wcmV2UGFyYW1zID0gcGFyYW1zQXJyO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoISBuKSB7XG4gICAgICAgICAgICAgICAgLy9zaG91bGQgY29tZSBhZnRlciBtZW1vcml6ZVxuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgYmluZGluZ3MgPSB0aGlzLl9iaW5kaW5ncy5zbGljZSgpOyAvL2Nsb25lIGFycmF5IGluIGNhc2UgYWRkL3JlbW92ZSBpdGVtcyBkdXJpbmcgZGlzcGF0Y2hcbiAgICAgICAgICAgIHRoaXMuX3Nob3VsZFByb3BhZ2F0ZSA9IHRydWU7IC8vaW4gY2FzZSBgaGFsdGAgd2FzIGNhbGxlZCBiZWZvcmUgZGlzcGF0Y2ggb3IgZHVyaW5nIHRoZSBwcmV2aW91cyBkaXNwYXRjaC5cblxuICAgICAgICAgICAgLy9leGVjdXRlIGFsbCBjYWxsYmFja3MgdW50aWwgZW5kIG9mIHRoZSBsaXN0IG9yIHVudGlsIGEgY2FsbGJhY2sgcmV0dXJucyBgZmFsc2VgIG9yIHN0b3BzIHByb3BhZ2F0aW9uXG4gICAgICAgICAgICAvL3JldmVyc2UgbG9vcCBzaW5jZSBsaXN0ZW5lcnMgd2l0aCBoaWdoZXIgcHJpb3JpdHkgd2lsbCBiZSBhZGRlZCBhdCB0aGUgZW5kIG9mIHRoZSBsaXN0XG4gICAgICAgICAgICBkbyB7IG4tLTsgfSB3aGlsZSAoYmluZGluZ3Nbbl0gJiYgdGhpcy5fc2hvdWxkUHJvcGFnYXRlICYmIGJpbmRpbmdzW25dLmV4ZWN1dGUocGFyYW1zQXJyKSAhPT0gZmFsc2UpO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBGb3JnZXQgbWVtb3JpemVkIGFyZ3VtZW50cy5cbiAgICAgICAgICogQHNlZSBTaWduYWwubWVtb3JpemVcbiAgICAgICAgICovXG4gICAgICAgIGZvcmdldCA6IGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICB0aGlzLl9wcmV2UGFyYW1zID0gbnVsbDtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogUmVtb3ZlIGFsbCBiaW5kaW5ncyBmcm9tIHNpZ25hbCBhbmQgZGVzdHJveSBhbnkgcmVmZXJlbmNlIHRvIGV4dGVybmFsIG9iamVjdHMgKGRlc3Ryb3kgU2lnbmFsIG9iamVjdCkuXG4gICAgICAgICAqIDxwPjxzdHJvbmc+SU1QT1JUQU5UOjwvc3Ryb25nPiBjYWxsaW5nIGFueSBtZXRob2Qgb24gdGhlIHNpZ25hbCBpbnN0YW5jZSBhZnRlciBjYWxsaW5nIGRpc3Bvc2Ugd2lsbCB0aHJvdyBlcnJvcnMuPC9wPlxuICAgICAgICAgKi9cbiAgICAgICAgZGlzcG9zZSA6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHRoaXMucmVtb3ZlQWxsKCk7XG4gICAgICAgICAgICBkZWxldGUgdGhpcy5fYmluZGluZ3M7XG4gICAgICAgICAgICBkZWxldGUgdGhpcy5fcHJldlBhcmFtcztcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogQHJldHVybiB7c3RyaW5nfSBTdHJpbmcgcmVwcmVzZW50YXRpb24gb2YgdGhlIG9iamVjdC5cbiAgICAgICAgICovXG4gICAgICAgIHRvU3RyaW5nIDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgcmV0dXJuICdbU2lnbmFsIGFjdGl2ZTonKyB0aGlzLmFjdGl2ZSArJyBudW1MaXN0ZW5lcnM6JysgdGhpcy5nZXROdW1MaXN0ZW5lcnMoKSArJ10nO1xuICAgICAgICB9XG5cbiAgICB9O1xuXG5cbiAgICAvLyBOYW1lc3BhY2UgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICAvLz09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAgIC8qKlxuICAgICAqIFNpZ25hbHMgbmFtZXNwYWNlXG4gICAgICogQG5hbWVzcGFjZVxuICAgICAqIEBuYW1lIHNpZ25hbHNcbiAgICAgKi9cbiAgICB2YXIgc2lnbmFscyA9IFNpZ25hbDtcblxuICAgIC8qKlxuICAgICAqIEN1c3RvbSBldmVudCBicm9hZGNhc3RlclxuICAgICAqIEBzZWUgU2lnbmFsXG4gICAgICovXG4gICAgLy8gYWxpYXMgZm9yIGJhY2t3YXJkcyBjb21wYXRpYmlsaXR5IChzZWUgI2doLTQ0KVxuICAgIHNpZ25hbHMuU2lnbmFsID0gU2lnbmFsO1xuXG5cblxuICAgIC8vZXhwb3J0cyB0byBtdWx0aXBsZSBlbnZpcm9ubWVudHNcbiAgICBpZih0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpeyAvL0FNRFxuICAgICAgICBkZWZpbmUoZnVuY3Rpb24gKCkgeyByZXR1cm4gc2lnbmFsczsgfSk7XG4gICAgfSBlbHNlIGlmICh0eXBlb2YgbW9kdWxlICE9PSAndW5kZWZpbmVkJyAmJiBtb2R1bGUuZXhwb3J0cyl7IC8vbm9kZVxuICAgICAgICBtb2R1bGUuZXhwb3J0cyA9IHNpZ25hbHM7XG4gICAgfSBlbHNlIHsgLy9icm93c2VyXG4gICAgICAgIC8vdXNlIHN0cmluZyBiZWNhdXNlIG9mIEdvb2dsZSBjbG9zdXJlIGNvbXBpbGVyIEFEVkFOQ0VEX01PREVcbiAgICAgICAgLypqc2xpbnQgc3ViOnRydWUgKi9cbiAgICAgICAgZ2xvYmFsWydzaWduYWxzJ10gPSBzaWduYWxzO1xuICAgIH1cblxufSh0aGlzKSk7XG4iLCIvKipcbiAqIEBhdXRob3IgbHVpenNzYlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IHtcblx0VG9TZXJ2ZXI6IHtcblx0XHRKT0lOX0NMVVNURVI6ICdqb2luQ2x1c3RlcicsXG5cdFx0UVVJVF9DTFVTVEVSOiAnbGVhdmUnLFxuXHRcdFBFRUtfQ0xVU1RFUjogJ3BlZWsnXG5cdH0sXG5cdEZyb21TZXJ2ZXI6IHtcblx0XHRKT0lORURfQ0xVU1RFUjogJ2pvaW5DbHVzdGVyJyxcblx0XHRKT0lOX0ZBSUxFRDogJ2pvaW5DbHVzdGVyRXJyb3InLFxuXHRcdFBFRVJfR09ORTogJ3BlZXJHb25lJyxcblx0XHRNQVNURVJfSk9JTkVEOiAnbWFzdGVySm9pbicsXG5cdFx0UEVFS0VEOiAncGVlaydcblx0fVxufTtcbiIsIi8qKlxuICogQGF1dGhvciBsdWl6c3NiXG4gKi9cbm1vZHVsZS5leHBvcnRzID0ge1xuXHRBTlk6IDcsXG5cdFNMQVZFX05PVzogMSxcblx0U0xBVkVfV0FJVDogMixcblx0TUFTVEVSOiA0XG59O1xuIiwiLyoqXG4gKiBAYXV0aG9yIGx1aXpzc2JcbiAqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IHtcblx0Zm9yRGV2aWNlOiBmdW5jdGlvbiAoZGV2aWNlLCBhZGRyZXNzLCB0eXBlKSB7XG5cdFx0dmFyIF9tYWtlRXZlbnROYW1lID0gZnVuY3Rpb24gKGV2ZW50KSB7XG5cdFx0XHRyZXR1cm4gJ2RldmljZTonICsgW2V2ZW50LCBkZXZpY2UsIGFkZHJlc3MsIHR5cGVdLmpvaW4oJzonKTtcblx0XHR9O1xuXHRcdHJldHVybiB7XG5cdFx0XHRUb1NlcnZlcjoge1xuXHRcdFx0XHRTVUJTQ1JJQkVfVE9fRVZFTlQ6IF9tYWtlRXZlbnROYW1lKCdzdWJzY3JpYmUnKSxcblx0XHRcdFx0VU5TVUJTQ1JJQkVfRlJPTV9FVkVOVDogX21ha2VFdmVudE5hbWUoJ3Vuc3Vic2NyaWJlJylcblx0XHRcdH0sXG5cdFx0XHRGcm9tU2VydmVyOiB7XG5cdFx0XHRcdFNVQlNDUklCRTogX21ha2VFdmVudE5hbWUoJ3N1YnNjcmliZScpLFxuXHRcdFx0XHRTVUJTQ1JJQkVfRVJST1I6IF9tYWtlRXZlbnROYW1lKCdzdWJzY3JpYmVFcnJvcicpLFxuXHRcdFx0XHRERVZJQ0VfRVZFTlQ6IF9tYWtlRXZlbnROYW1lKCdldmVudCcpLFxuXHRcdFx0XHRDT05ORUNUOiBfbWFrZUV2ZW50TmFtZSgnY29ubmVjdCcpLFxuXHRcdFx0XHRDT05ORUNUX0VSUk9SOiBfbWFrZUV2ZW50TmFtZSgnZXJyb3InKSxcblx0XHRcdFx0TE9TVDogX21ha2VFdmVudE5hbWUoJ2xvc3QnKVxuXHRcdFx0fVxuXHRcdH07XG5cdH0sXG5cdFRvU2VydmVyOiB7XG5cdFx0Q09OTkVDVDogJ2RldmljZScsXG5cdFx0RElTQ09OTkVDVDogJ2RldmljZTpkaXNjb25uZWN0Jyxcblx0XHRRVUVSWV9ERVZJQ0VTOiAnZGV2aWNlOnF1ZXJ5J1xuXHR9LFxuXHRGcm9tU2VydmVyOiB7XG5cdFx0TkVXX0RFVklDRTogJ2RldmljZTpuZXcnLFxuXHRcdERFVklDRVNfUVVFUlk6ICdkZXZpY2U6cXVlcnknXG5cdH1cbn07XG4iLCIvKipcbiAqIEBhdXRob3IgbHVpenNzYlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IHtcblx0QU5BTE9HOiAnYW5hbG9nJyxcblx0QlVUVE9OOiAnYnV0dG9uJ1xufTtcbiIsIi8qKlxuICogQGF1dGhvciBsdWl6c3NiXG4gKi9cbm1vZHVsZS5leHBvcnRzID0ge1xuXHRUb1NlcnZlcjoge1xuXHRcdE9GRkVSX0NPTk5FQ1RJT046ICdvZmZlcicsXG5cdFx0QU5TV0VSX09GRkVSOiAnYW5zd2VyJyxcblx0XHRGQUlMX0FOU1dFUjogJ2Fuc3dlckZhaWxlZCcsXG5cdFx0Q0FORElEQVRFX1BFRVI6ICdjYW5kaWRhdGUnXG5cdH0sXG5cdEZyb21TZXJ2ZXI6IHtcblx0XHRSRUNFSVZFRF9PRkZFUjogJ29mZmVyJyxcblx0XHRSRUNFSVZFRF9BTlNXRVI6ICdhbnN3ZXInLFxuXHRcdEFOU1dFUl9GQUlMRUQ6ICdhbnN3ZXJGYWlsZWQnLFxuXHRcdFJFQ0VJVkVEX0NBTkRJREFURTogJ2NhbmRpZGF0ZSdcblx0fVxufTtcbiIsIi8qKlxuICogQGF1dGhvciBsdWl6c3NiXG4gKi9cbm1vZHVsZS5leHBvcnRzID0ge1xuXHRGcm9tU2VydmVyOiB7XG5cdFx0UkVDRUlWRURfREFUQTogJ25vZGU6ZGF0YScsXG5cdFx0Tk9ERV9HT05FOiAnbm9kZTpnb25lJ1xuXHR9LFxuXHRUb1NlcnZlcjoge1xuXHRcdFNFTkRfREFUQTogJ25vZGU6ZGF0YScsXG5cdFx0UVVJVDogJ25vZGU6cXVpdCdcblx0fVxufTtcbiIsIi8qKlxuICogQGF1dGhvciBsdWl6c3NiXG4gKi9cbm1vZHVsZS5leHBvcnRzID0ge1xuXHRUb1NlcnZlcjoge1xuXHRcdEpPSU46ICdyb29tOmpvaW4nLFxuXHRcdExFQVZFOiAncm9vbTpsZWF2ZSdcblx0fVxufTtcbiIsIi8qKlxuICogQGF1dGhvciBsdWl6c3NiXG4gKi9cbmZ1bmN0aW9uIExpdnZjbGliRXJyb3IgKG1lc3NhZ2UsIGNvZGUsIHByZXZpb3VzLCBkb05vdExvZykge1xuXHR2YXIgdG1wID0gRXJyb3IuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcblx0dG1wLm5hbWUgPSB0aGlzLm5hbWUgPSAnTGl2dmNsaWJFcnJvcic7XG5cblx0dGhpcy5tZXNzYWdlID0gdG1wLm1lc3NhZ2U7XG5cdHRoaXMuY29kZSA9IGNvZGU7XG5cdHRoaXMucHJldmlvdXMgPSBwcmV2aW91cztcblx0dGhpcy5zdGFjayA9IHRtcC5zdGFjaztcblxuXHRpZiAoIWRvTm90TG9nKSB7XG5cdFx0Y29uc29sZS5lcnJvcih0aGlzKTtcblx0fVxuXHRyZXR1cm4gdGhpcztcbn1cblxuTGl2dmNsaWJFcnJvci5wcm90b3R5cGUgPSBPYmplY3QuYXNzaWduKFxuXHRPYmplY3QuY3JlYXRlKEVycm9yLnByb3RvdHlwZSksXG5cdHsgY29uc3RydWN0b3I6IExpdnZjbGliRXJyb3IgfVxuKTtcblxubW9kdWxlLmV4cG9ydHMgPSBMaXZ2Y2xpYkVycm9yO1xuIiwiY29uc3QgUmFuZG9tVXRpbHMgPSByZXF1aXJlKCcuLi91dGlscy9SYW5kb21VdGlscycpO1xuY29uc3QgZGVmYXVsdFBhcmFtcyA9IHJlcXVpcmUoJy4vZGVmYXVsdFBhcmFtcycpO1xuXG4vKipcbiAqIEBhdXRob3IgbHVpenNzYlxuICovXG5mdW5jdGlvbiBXZWJDbHVzdGVyU3VwcG9ydCAoY2xpZW50c01hbmFnZXIpIHtcblx0dGhpcy50YWcgPSBSYW5kb21VdGlscy5zdHJpbmcoKTtcblx0dGhpcy5jbGllbnRzTWFuYWdlciA9IGNsaWVudHNNYW5hZ2VyO1xuXHR0aGlzLmNsaWVudEV2ZW50c0hhbmRsZXJzID0gW107XG59XG5cbldlYkNsdXN0ZXJTdXBwb3J0LnByb3RvdHlwZS5zZXR1cCA9IGZ1bmN0aW9uICgpIHtcblx0dGhpcy5jbGllbnRzTWFuYWdlci5zdGFydCgpO1xuXG5cdHRoaXMuY2xpZW50c01hbmFnZXIub24uY2xpZW50Q29ubmVjdGVkLmFkZChcblx0XHRmdW5jdGlvbiAoY2xpZW50KSB7XG5cdFx0XHRjbGllbnQub24ud2lsbEVtaXQuYWRkKGRlZmF1bHRQYXJhbXMsIHRoaXMpO1xuXG5cdFx0XHR0aGlzLmNsaWVudEV2ZW50c0hhbmRsZXJzLmZvckVhY2goZnVuY3Rpb24gKGhhbmRsZXIpIHtcblx0XHRcdFx0aGFuZGxlci5oYW5kbGVOZXdDbGllbnQoY2xpZW50KTtcblx0XHRcdH0pO1xuXHRcdH0sXG5cdFx0dGhpc1xuXHQpO1xuXG5cdHRoaXMuY2xpZW50c01hbmFnZXIub24uY2xpZW50RGlzY29ubmVjdGVkLmFkZChcblx0XHRmdW5jdGlvbiAoY2xpZW50KSB7XG5cdFx0XHR0aGlzLmNsaWVudEV2ZW50c0hhbmRsZXJzLmZvckVhY2goZnVuY3Rpb24gKGhhbmRsZXIpIHtcblx0XHRcdFx0aGFuZGxlci5oYW5kbGVDbGllbnRHb25lKGNsaWVudCk7XG5cdFx0XHR9KTtcblx0XHR9LFxuXHRcdHRoaXNcblx0KTtcbn07XG5cbldlYkNsdXN0ZXJTdXBwb3J0LnByb3RvdHlwZS5hZGRFdmVudHNIYW5kbGVyID0gZnVuY3Rpb24gKCkge1xuXHRmb3IgKHZhciBpZHggaW4gYXJndW1lbnRzKSB7XG5cdFx0dGhpcy5jbGllbnRFdmVudHNIYW5kbGVycy5wdXNoKGFyZ3VtZW50c1tpZHhdKTtcblx0fVxuXG5cdHJldHVybiB0aGlzO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBXZWJDbHVzdGVyU3VwcG9ydDtcbiIsImNvbnN0IE9iamVjdEV4ID0gcmVxdWlyZSgnLi4vLi4vdXRpbHMvT2JqZWN0RXgnKTtcblxuLyoqXG4gKiBAYXV0aG9yIGx1aXpzc2JcbiAqL1xuZnVuY3Rpb24gQ2xpZW50Q29ubmVjdGlvbiAocGFyZW50TWFuYWdlcikge1xuXHR0aGlzLl9tYW5hZ2VyID0gcGFyZW50TWFuYWdlcjtcblxuXHR0aGlzLm9uID0gdGhpcy5vbi5iaW5kKHRoaXMpO1xuXHRPYmplY3RFeC5kZWZpbmVFdmVudHMoXG5cdFx0dGhpcywgW1xuXHRcdFx0LyoqXG5cdFx0XHQgKiBDYWxsZWQgd2hlbiBhIHJlcXVlc3Qgd2lsbCBiZSBtYWRlIGFuZCB0aGUgZGVmYXVsdCBwYXJhbWV0ZXJzIG11c3QgYmUgY3VzdG9taXplZC5cblx0XHRcdCAqIEBwYXJhbSB7Q2xpZW50Q29ubmVjdGlvbn0gc2VuZGVyIEluc3RhbmNlIGVtaXR0aW5nIGRhdGEuXG5cdFx0XHQgKiBAcGFyYW0ge09iamVjdH0gZGF0YSBUaGUgZGVmYXVsdCBwYXJhbWV0ZXJzIHRoYXQgY2FuIGJlIGN1c3RvbWl6ZWQuXG5cdFx0XHQgKi9cblx0XHRcdCd3aWxsRW1pdCcsXG5cblx0XHRcdC8qKlxuXHRcdFx0ICogQ2FsbGVkIHdoZW4gdGhlIGNsaWVudCBqb2lucyBhIHJvb20gaW4gdGhlIHNlcnZlci5cblx0XHRcdCAqIEBwYXJhbSB7Q2xpZW50Q29ubmVjdGlvbn0gc2VuZGVyIEluc3RhbmNlIHRoYXQgam9pbmVkIHRoZSByb29tLlxuXHRcdFx0ICovXG5cdFx0XHQnam9pbmVkUm9vbScsXG5cblx0XHRcdC8qKlxuXHRcdFx0ICogQ2FsbGVkIHdoZW4gdGhlIGNsaWVudCBsZWF2ZXMgYSByb29tIGluIHRoZSBzZXJ2ZXIuXG5cdFx0XHQgKiBAcGFyYW0ge0NsaWVudENvbm5lY3Rpb259IHNlbmRlciBJbnN0YW5jZSB0aGF0IGxlZnQgdGhlIHJvb20uXG5cdFx0XHQgKiBAcGFyYW0ge3N0cmluZ30gcm9vbSBOYW1lIG9mIHRoZSByb29tIHRoYXQgd2FzIGxlZnQuXG5cdFx0XHQgKi9cblx0XHRcdCdsZWZ0Um9vbSdcblx0XHRdXG5cdCk7XG59XG5cbk9iamVjdEV4LmRlZmluZVByb3BlcnRpZXMoXG5cdENsaWVudENvbm5lY3Rpb24ucHJvdG90eXBlLFxuXHR7XG5cdFx0bWFuYWdlcjogT2JqZWN0RXgucmVhZE9ubHkoZnVuY3Rpb24gKCkgeyByZXR1cm4gdGhpcy5fbWFuYWdlcjsgfSksXG5cdFx0aWQ6IE9iamVjdEV4LnJlYWRPbmx5KE9iamVjdEV4LmFic3RyYWN0TWV0aG9kKSxcblx0XHRyb29tOiBPYmplY3RFeC5yZWFkT25seShPYmplY3RFeC5hYnN0cmFjdE1ldGhvZCksXG5cdFx0Y29ubmVjdGVkOiBPYmplY3RFeC5yZWFkT25seShPYmplY3RFeC5hYnN0cmFjdE1ldGhvZClcblx0fVxuKTtcblxuLyoqXG4gKiBSZWdpc3RlcnMgYSBjYWxsYmFjayB0byBiZSBleGVjdXRlZCB3aGVuZXZlciBzb21lIGNsaWVudCBzZW5kcyBhIG1lc3NhZ2UuXG4gKiBAcGFyYW0ge3N0cmluZ30gZXZlbnQgTmFtZSBvZiB0aGUgZXZlbnQgdGhhdCB0cmlnZ2VycyB0aGUgY2FsbGJhY2s7XG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFjayBDYWxsYmFjayB0byBiZSBleGVjdXRlZCB3aGVuIHRoZSBldmVudCBpcyB0cmlnZ2VyZWQuXG4gKiBAcmV0dXJuIHtDbGllbnRzQ29ubmVjdGlvbn0gSXRzZWxmLlxuICovXG5DbGllbnRDb25uZWN0aW9uLnByb3RvdHlwZS5vbiA9IE9iamVjdEV4LmFic3RyYWN0TWV0aG9kO1xuXG4vKipcbiAqIERlcmVnaXN0ZXJzIGEgY2FsbGJhY2sgb2YgYW4gZXZldFxuICogQHBhcmFtIHtzdHJpbmd9IGV2ZW50IFRoZSBldmVudCB0aGF0IHRyaWdnZXJzIHRoZSBjYWxsYmFjay5cbiAqIEByZXR1cm4ge0NsaWVudHNDb25uZWN0aW9ufSBJdHNlbGYuXG4gKi9cbkNsaWVudENvbm5lY3Rpb24ucHJvdG90eXBlLm9mZiA9IE9iamVjdEV4LmFic3RyYWN0TWV0aG9kO1xuXG5DbGllbnRDb25uZWN0aW9uLnByb3RvdHlwZS5lbWl0ID0gZnVuY3Rpb24gKGV2ZW50KSB7XG5cdHJldHVybiB0aGlzLmVtaXRXaXRoUGFyYW1zLmFwcGx5KFxuXHRcdHRoaXMsIFtldmVudCwge31dLmNvbmNhdChBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpKVxuXHQpO1xufTtcblxuQ2xpZW50Q29ubmVjdGlvbi5wcm90b3R5cGUuZW1pdFdpdGhQYXJhbXMgPSBmdW5jdGlvbiAoZXZlbnQsIHBhcmFtcykge1xuXHRjb25zdCBkZWZhdWx0UGFyYW1zID0ge307XG5cdHRoaXMub24ud2lsbEVtaXQuZGlzcGF0Y2godGhpcywgZGVmYXVsdFBhcmFtcyk7XG5cdE9iamVjdC5hc3NpZ24oZGVmYXVsdFBhcmFtcywgcGFyYW1zKTtcblxuXHRyZXR1cm4gdGhpcy5fZW1pdC5hcHBseShcblx0XHR0aGlzLFxuXHRcdFtldmVudCwgZGVmYXVsdFBhcmFtc10uY29uY2F0KEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMikpXG5cdCk7XG59O1xuXG4vKipcbiAqIFNlbmRzIGEgbWVzc2FnZSB0byBhIGNsaWVudC5cbiAqIEBwYXJhbSB7c3RyaW5nfSBldmVudCBUaGUgZXZlbnQgdG8gdHJpZ2dlciBvbiB0aGUgY2xpZW50cy5cbiAqIEBwYXJhbSB7T2JqZWN0Li4ufSBbXSBEYXRhIHRvIHNlbmQgdG8gdGhlIGNsaWVudHMuXG4gKiBAcmV0dXJuIHtDbGllbnRDb25uZWN0aW9ufSBJdHNlbGYuXG4gKi9cbkNsaWVudENvbm5lY3Rpb24ucHJvdG90eXBlLl9lbWl0ID0gT2JqZWN0RXguYWJzdHJhY3RNZXRob2Q7XG5cbi8qKlxuICogTWFrZXMgdGhlIGNsaWVudCBjb25uZWN0aW9uIGpvaW4gYW4gc3BlY2lmaWMgcm9vbS5cbiAqIENsaWVudCBtYXkgYmUgY29ubmVjdGVkIHRvIG9ubHkgb25lIHJvb20gYXQgYSB0aW1lLlxuICogQHBhcmFtIHtzdHJpbmd9IHJvb20gVGhlIFJvb20gdG8gam9pbi5cbiAqIEByZXR1cm4ge0NsaWVudENvbm5lY3Rpb259IEl0c2VsZi5cbiAqL1xuQ2xpZW50Q29ubmVjdGlvbi5wcm90b3R5cGUuam9pbiA9IE9iamVjdEV4LmFic3RyYWN0TWV0aG9kO1xuXG4vKipcbiAqIExlYXZlcyB0aGUgcm9vbS5cbiAqIEByZXR1cm4ge0NsaWVudENvbm5lY3Rpb259IEl0c2VsZi5cbiAqL1xuQ2xpZW50Q29ubmVjdGlvbi5wcm90b3R5cGUubGVhdmUgPSBPYmplY3RFeC5hYnN0cmFjdE1ldGhvZDtcblxuLyoqXG4gKiBUZXJtaW5hdGVzIGNvbm5lY3Rpb24uXG4gKiBAcmV0dXJuIHtDbGllbnRDb25uZWN0aW9ufSBJdHNlbGYuXG4gKi9cbkNsaWVudENvbm5lY3Rpb24ucHJvdG90eXBlLmRpc2Nvbm5lY3QgPSBPYmplY3RFeC5hYnN0cmFjdE1ldGhvZDtcblxuQ2xpZW50Q29ubmVjdGlvbi5wcm90b3R5cGUuYnJvYWRjYXN0ID0gZnVuY3Rpb24gKGV2ZW50KSB7XG5cdGNvbnN0IGRlZmF1bHRQYXJhbXMgPSB7fTtcblx0dGhpcy5vbi53aWxsRW1pdC5kaXNwYXRjaCh0aGlzLCBkZWZhdWx0UGFyYW1zKTtcblx0cmV0dXJuIHRoaXMuX2Jyb2FkY2FzdC5hcHBseShcblx0XHR0aGlzLFxuXHRcdFtldmVudCwgZGVmYXVsdFBhcmFtc10uY29uY2F0KEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSkpXG5cdCk7XG59O1xuXG4vKipcbiAqIEJyb2FkY2FzdHMgYSBtZXNzYWdlIHRvIGFsbCBub2RlcyBpbiB0aGUgcm9vbSB0aGlzIGNsaWVudCBpcyBpbiwgZXhjbHVkaW5nIGl0LlxuICogQHBhcmFtIHtzdHJpbmd9IGV2ZW50IFRoZSBldmVudCB0byB0cmlnZ2VyIG9uIHRoZSBjbGllbnRzLlxuICogQHBhcmFtIHtPYmplY3QuLi59IFtdIERhdGEgdG8gc2VuZCB0byB0aGUgY2xpZW50cy5cbiAqIEByZXR1cm4ge0NsaWVudENvbm5lY3Rpb259IEl0c2VsZi5cbiAqL1xuQ2xpZW50Q29ubmVjdGlvbi5wcm90b3R5cGUuX2Jyb2FkY2FzdCA9IE9iamVjdEV4LmFic3RyYWN0TWV0aG9kO1xuXG5tb2R1bGUuZXhwb3J0cyA9IENsaWVudENvbm5lY3Rpb247XG4iLCJjb25zdCBPYmplY3RFeCA9IHJlcXVpcmUoJy4uLy4uL3V0aWxzL09iamVjdEV4Jyk7XG5cbi8qKlxuICogQGF1dGhvciBsdWl6c3NiXG4gKi9cbmZ1bmN0aW9uIENsaWVudHNNYW5hZ2VyICgpIHtcblx0T2JqZWN0RXguZGVmaW5lRXZlbnRzKFxuXHRcdHRoaXMsIFtcblx0XHRcdC8qKlxuXHRcdFx0ICogQ2FsbGVkIHdoZW4gYSBuZXcgY2xpZW50IGNvbm5lY3RzIHRvIHRoZSBzZXJ2ZXIuXG5cdFx0XHQgKiBAcGFyYW0ge0NsaWVudENvbm5lY3Rpb259IGNsaWVudCBDb25uZWN0aW9uIHRvIHRoZSBuZXcgY2xpZW50LlxuXHRcdFx0ICovXG5cdFx0XHQnY2xpZW50Q29ubmVjdGVkJyxcblxuXHRcdFx0LyoqXG5cdFx0XHQgKiBDYWxsZWQgd2hlbiBhIGNsaWVudCBkaXNjb25uZWN0cyBmcm9tIHRoZSBzZXJ2ZXIuXG5cdFx0XHQgKiBAcGFyYW0ge0NsaWVudENvbm5lY3Rpb259IGNsaWVudCBDb25uZWN0aW9uIHRvIHRoZSBjbGllbnQuXG5cdFx0XHQgKi9cblx0XHRcdCdjbGllbnREaXNjb25uZWN0ZWQnLFxuXG5cdFx0XHQvKipcblx0XHRcdCAqIENhbGxlZCB3aGVuIGRhdGEgd2lsbCBiZSBicm9hZGNhc3QgdG8gYWxsIG5vZGVzIGluIGEgcm9vbS5cblx0XHRcdCAqIEBwYXJhbSB7T2JqZWN0fSBkYXRhIERhdGEgdG8gYmUgY3VzdG9taXplZC5cblx0XHRcdCAqL1xuXHRcdFx0J3dpbGxCcm9hZGNhc3QnXG5cdFx0XVxuXHQpO1xufVxuXG4vKipcbiAqIFN0YXJ0cyB3YWl0aW5nIGZvciBuZXcgY2xpZW50cyB0byBjb25uZWN0LlxuICogQHJldHVybiB7Q2xpZW50c01hbmFnZXJ9IEl0c2VsZi5cbiAqL1xuQ2xpZW50c01hbmFnZXIucHJvdG90eXBlLnN0YXJ0ID0gT2JqZWN0RXguYWJzdHJhY3RNZXRob2Q7XG5cbkNsaWVudHNNYW5hZ2VyLnByb3RvdHlwZS5icm9hZGNhc3QgPSBmdW5jdGlvbiAocm9vbSwgZXZlbnQpIHtcblx0Y29uc3QgZGVmYXVsdFBhcmFtcyA9IHt9O1xuXHR0aGlzLm9uLndpbGxCcm9hZGNhc3QuZGlzcGF0Y2goZGVmYXVsdFBhcmFtcyk7XG5cblx0cmV0dXJuIHRoaXMuX2Jyb2FkY2FzdC5hcHBseShcblx0XHR0aGlzLFxuXHRcdFtyb29tLCBldmVudCwgZGVmYXVsdFBhcmFtc11cblx0XHRcdC5jb25jYXQoQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAyKSlcblx0KTtcbn07XG5cbi8qKlxuICogQnJvYWRjYXN0cyBhbiBldmVudCB0byBhbGwgY2xpZW50cyBpbiBhIHJvb20uXG4gKiBAcGFyYW0ge3N0cmluZ30gcm9vbSBUaGUgcm9vbSB0byByZWNlaXZlIHRoZSBicm9hZGNhc3QuXG4gKiBAcGFyYW0ge3N0cmluZ30gZXZlbnQgVGhlIGV2ZW50IHRvIHRyaWdnZXIgb24gdGhlIGNsaWVudHMuXG4gKiBAcGFyYW0ge09iamVjdC4uLn0gW10gRGF0YSB0byBzZW5kIHRvIHRoZSBjbGllbnRzXG4gKiBAcmV0dXJuIHtDbGllbnRzTWFuYWdlcn0gSXRzZWxmLlxuICovXG5DbGllbnRzTWFuYWdlci5wcm90b3R5cGUuX2Jyb2FkY2FzdCA9IE9iamVjdEV4LmFic3RyYWN0TWV0aG9kO1xuXG4vKipcbiAqIEdldHMgdGhlIG51bWJlciBvZiBjbGllbnRzLlxuICogQHBhcmFtIHtzdHJpbmd9IHJvb20gUm9vbSBpbiB3aGljaCB0aGUgY2xpZW50cyBhcmUuXG4gKiBAcmV0dXJuIHtpbnR9IFRoZSBudW1iZXIgb2YgY2xpZW50cyBpbiB0aGUgcm9vbS5cbiAqL1xuQ2xpZW50c01hbmFnZXIucHJvdG90eXBlLmdldE51bWJlck9mQ2xpZW50cyA9IE9iamVjdEV4LmFic3RyYWN0TWV0aG9kO1xuXG4vKipcbiAqIEdldHMgdGhlIGNvbm5lY3Rpb24gdG8gb25lIG9mIHRoZSBjbGllbnRzLlxuICogQHBhcmFtIHtzdHJpbmd9IGlkIFRoZSBpZCBvZiB0aGUgY2xpZW50LlxuICogQHJldHVybiB7Q2xpZW50Q29ubmVjdGlvbn0gVGhlIGNvbm5lY3Rpb24gdG8gdGhlIGNsaWVudC5cbiAqL1xuQ2xpZW50c01hbmFnZXIucHJvdG90eXBlLmdldCA9IE9iamVjdEV4LmFic3RyYWN0TWV0aG9kO1xuXG5tb2R1bGUuZXhwb3J0cyA9IENsaWVudHNNYW5hZ2VyO1xuIiwiY29uc3QgQ2xpZW50Q29ubmVjdGlvbiA9IHJlcXVpcmUoJy4vQ2xpZW50Q29ubmVjdGlvbicpO1xuY29uc3QgT2JqZWN0RXggPSByZXF1aXJlKCcuLi8uLi91dGlscy9PYmplY3RFeCcpO1xuXG5mdW5jdGlvbiBTb2NrZXRJT0NsaWVudENvbm5lY3Rpb24gKHNvY2tldCwgcGFyZW50TWFuYWdlcikge1xuXHRDbGllbnRDb25uZWN0aW9uLmNhbGwodGhpcywgcGFyZW50TWFuYWdlcik7XG5cdHRoaXMuc29ja2V0ID0gc29ja2V0O1xufVxuXG5Tb2NrZXRJT0NsaWVudENvbm5lY3Rpb24ucHJvdG90eXBlID0gT2JqZWN0LmFzc2lnbihcblx0T2JqZWN0LmNyZWF0ZShDbGllbnRDb25uZWN0aW9uLnByb3RvdHlwZSksXG5cdHsgY29uc3RydWN0b3I6IFNvY2tldElPQ2xpZW50Q29ubmVjdGlvbiB9XG4pO1xuXG5PYmplY3RFeC5kZWZpbmVQcm9wZXJ0aWVzKFxuXHRTb2NrZXRJT0NsaWVudENvbm5lY3Rpb24ucHJvdG90eXBlLFxuXHR7XG5cdFx0aWQ6IE9iamVjdEV4LnJlYWRPbmx5KGZ1bmN0aW9uICgpIHsgcmV0dXJuIHRoaXMuc29ja2V0LmlkOyB9KSxcblx0XHRyb29tOiBPYmplY3RFeC5yZWFkT25seShmdW5jdGlvbiAoKSB7IHJldHVybiB0aGlzLl9yb29tOyB9KSxcblx0XHRjb25uZWN0ZWQ6XG5cdFx0XHRPYmplY3RFeC5yZWFkT25seShmdW5jdGlvbiAoKSB7IHJldHVybiB0aGlzLnNvY2tldC5jb25uZWN0ZWQ7IH0pXG5cdH1cbik7XG5cbi8vIE92ZXJyaWRlXG5Tb2NrZXRJT0NsaWVudENvbm5lY3Rpb24ucHJvdG90eXBlLm9uID0gZnVuY3Rpb24gKGV2ZW50LCBjYWxsYmFjaykge1xuXHR0aGlzLnNvY2tldC5vbihldmVudCwgY2FsbGJhY2spO1xuXHRyZXR1cm4gdGhpcztcbn07XG5cbi8vIE92ZXJyaWRlXG5Tb2NrZXRJT0NsaWVudENvbm5lY3Rpb24ucHJvdG90eXBlLm9mZiA9IGZ1bmN0aW9uIChldmVudCkge1xuXHR0aGlzLnNvY2tldC5yZW1vdmVBbGxMaXN0ZW5lcnMoZXZlbnQpO1xuXHRyZXR1cm4gdGhpcztcbn07XG5cbi8vIE92ZXJyaWRlXG5Tb2NrZXRJT0NsaWVudENvbm5lY3Rpb24ucHJvdG90eXBlLl9lbWl0ID0gZnVuY3Rpb24gKGV2ZW50KSB7XG5cdHRoaXMuc29ja2V0LmVtaXQuYXBwbHkodGhpcy5zb2NrZXQsIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMCkpO1xuXHRyZXR1cm4gdGhpcztcbn07XG5cbi8vIE92ZXJyaWRlXG5Tb2NrZXRJT0NsaWVudENvbm5lY3Rpb24ucHJvdG90eXBlLl9icm9hZGNhc3QgPSBmdW5jdGlvbiAoZXZlbnQpIHtcblx0Y29uc3QgY2xpZW50cyA9IHRoaXMuc29ja2V0LmJyb2FkY2FzdC50byh0aGlzLnJvb20pO1xuXHRjbGllbnRzLmVtaXQuYXBwbHkoY2xpZW50cywgQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAwKSk7XG59O1xuXG4vLyBPdmVycmlkZVxuU29ja2V0SU9DbGllbnRDb25uZWN0aW9uLnByb3RvdHlwZS5qb2luID0gZnVuY3Rpb24gKHJvb20pIHtcblx0aWYgKHRoaXMucm9vbSkge1xuXHRcdGlmICh0aGlzLnJvb20gPT09IHJvb20pIHtcblx0XHRcdHJldHVybiB0aGlzO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ2NsaWVudCBhbHJlYWR5IGluIGFub3RoZXIgcm9vbScpO1xuXHRcdH1cblx0fVxuXG5cdHRoaXMuc29ja2V0LmpvaW4ocm9vbSk7XG5cdHRoaXMuX3Jvb20gPSByb29tO1xuXG5cdHRoaXMub24uam9pbmVkUm9vbS5kaXNwYXRjaCh0aGlzKTtcblxuXHRyZXR1cm4gdGhpcztcbn07XG5cbi8vIE92ZXJyaWRlXG5Tb2NrZXRJT0NsaWVudENvbm5lY3Rpb24ucHJvdG90eXBlLmxlYXZlID0gZnVuY3Rpb24gKCkge1xuXHRjb25zdCByb29tID0gdGhpcy5fcm9vbTtcblxuXHR0aGlzLnNvY2tldC5sZWF2ZSh0aGlzLnJvb20pO1xuXHR0aGlzLl9yb29tID0gbnVsbDtcblxuXHR0aGlzLm9uLmxlZnRSb29tLmRpc3BhdGNoKHJvb20pO1xuXG5cdHJldHVybiB0aGlzO1xufTtcblxuLy8gT3ZlcnJpZGVcblNvY2tldElPQ2xpZW50Q29ubmVjdGlvbi5wcm90b3R5cGUuZGlzY29ubmVjdCA9IGZ1bmN0aW9uICgpIHtcblx0dGhpcy5zb2NrZXQuZGlzY29ubmVjdCgpO1xuXG5cdGNvbnN0IHJvb20gPSB0aGlzLl9yb29tO1xuXHR0aGlzLl9yb29tID0gbnVsbDtcblx0dGhpcy5vbi5sZWZ0Um9vbS5kaXNwYXRjaCh0aGlzLCByb29tKTtcblxuXHRyZXR1cm4gdGhpcztcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gU29ja2V0SU9DbGllbnRDb25uZWN0aW9uO1xuIiwiY29uc3QgQ2xpZW50c01hbmFnZXIgPSByZXF1aXJlKCcuL0NsaWVudHNNYW5hZ2VyJyk7XG5jb25zdCBTb2NrZXRJT0NsaWVudENvbm5lY3Rpb24gPSByZXF1aXJlKCcuL1NvY2tldElPQ2xpZW50Q29ubmVjdGlvbicpO1xuXG4vKipcbiAqIEBhdXRob3IgbHVpenNzYlxuICovXG5mdW5jdGlvbiBTb2NrZXRJT0NsaWVudHNNYW5hZ2VyIChpbykge1xuXHRDbGllbnRzTWFuYWdlci5jYWxsKHRoaXMpO1xuXG5cdHRoaXMuaW8gPSBpbztcblx0dGhpcy5fY2xpZW50cyA9IHt9O1xufVxuXG5Tb2NrZXRJT0NsaWVudHNNYW5hZ2VyLnByb3RvdHlwZSA9IE9iamVjdC5hc3NpZ24oXG5cdE9iamVjdC5jcmVhdGUoQ2xpZW50c01hbmFnZXIucHJvdG90eXBlKSxcblx0eyBjb25zdHJ1Y3RvcjogU29ja2V0SU9DbGllbnRzTWFuYWdlciB9XG4pO1xuXG4vLyBPdmVycmlkZVxuU29ja2V0SU9DbGllbnRzTWFuYWdlci5wcm90b3R5cGUuc3RhcnQgPSBmdW5jdGlvbiAoKSB7XG5cdGlmICghdGhpcy5fc3RhcnRlZCkge1xuXHRcdHRoaXMuaW8uc29ja2V0cy5vbignY29ubmVjdGlvbicsIGZ1bmN0aW9uIChzb2NrZXQpIHtcblx0XHRcdGNvbnN0IGNsaWVudCA9IG5ldyBTb2NrZXRJT0NsaWVudENvbm5lY3Rpb24oc29ja2V0LCB0aGlzKTtcblx0XHRcdHRoaXMuX2NsaWVudHNbY2xpZW50LmlkXSA9IGNsaWVudDtcblx0XHRcdHRoaXMub24uY2xpZW50Q29ubmVjdGVkLmRpc3BhdGNoKGNsaWVudCk7XG5cblx0XHRcdGNsaWVudC5vbignZGlzY29ubmVjdCcsIGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0dGhpcy5vbi5jbGllbnREaXNjb25uZWN0ZWQuZGlzcGF0Y2goY2xpZW50KTtcblx0XHRcdH0uYmluZCh0aGlzKSk7XG5cdFx0fS5iaW5kKHRoaXMpKTtcblx0XHR0aGlzLl9zdGFydGVkID0gdHJ1ZTtcblx0fVxuXG5cdHJldHVybiB0aGlzO1xufTtcblxuLy8gT3ZlcnJpZGVcblNvY2tldElPQ2xpZW50c01hbmFnZXIucHJvdG90eXBlLl9icm9hZGNhc3QgPSBmdW5jdGlvbiAocm9vbSkge1xuXHRjb25zdCBjbGllbnRzID0gdGhpcy5pby5zb2NrZXRzLmluKHJvb20pO1xuXHRjbGllbnRzLmVtaXQuYXBwbHkoY2xpZW50cywgQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSk7XG5cblx0cmV0dXJuIHRoaXM7XG59O1xuXG4vLyBPdmVycmlkZVxuU29ja2V0SU9DbGllbnRzTWFuYWdlci5wcm90b3R5cGUuZ2V0TnVtYmVyT2ZDbGllbnRzID0gZnVuY3Rpb24gKHJvb20pIHtcblx0cmV0dXJuIE9iamVjdC5rZXlzKHRoaXMuaW8uc29ja2V0cy5hZGFwdGVyLnJvb21zW3Jvb21dLnNvY2tldHMpLmxlbmd0aDtcbn07XG5cbi8vIE92ZXJyaWRlXG5Tb2NrZXRJT0NsaWVudHNNYW5hZ2VyLnByb3RvdHlwZS5nZXQgPSBmdW5jdGlvbiAoaWQpIHtcblx0cmV0dXJuIHRoaXMuX2NsaWVudHNbaWRdO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBTb2NrZXRJT0NsaWVudHNNYW5hZ2VyO1xuIiwiLyoqXG4gKiBAYXV0aG9yIGx1aXpzc2JcbiAqL1xuZnVuY3Rpb24gZGVmYXVsdFBhcmFtcyAoc2VuZGVyLCBkYXRhKSB7XG5cdGRhdGEgPSBkYXRhIHx8IHt9O1xuXHRkYXRhLnNlbmRlciA9IHNlbmRlci5pZDtcblxuXHRpZiAoc2VuZGVyLnJvb20pIHtcblx0XHRkYXRhLnJvb20gPSBzZW5kZXIucm9vbTtcblx0XHRkYXRhLm51bWJlck9mUGFydGljaXBhbnRzID1cblx0XHRcdHNlbmRlci5tYW5hZ2VyLmdldE51bWJlck9mQ2xpZW50cyhzZW5kZXIucm9vbSk7XG5cdH1cblxuXHRyZXR1cm4gZGF0YTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gZGVmYXVsdFBhcmFtcztcbiIsImNvbnN0IEludGVyZmFjZXNGYWN0b3J5ID0gcmVxdWlyZSgnLi9JbnRlcmZhY2VzRmFjdG9yeScpO1xuY29uc3QgRGV2aWNlSW50ZXJmYWNlID0gcmVxdWlyZSgnLi4vLi4vY29uc3RhbnRzL0RldmljZUludGVyZmFjZScpO1xuXG4vKipcbiAqIEBhdXRob3IgbHVpenNzYlxuICovXG5mdW5jdGlvbiBEZWZhdWx0SW50ZXJmYWNlc0ZhY3RvcnkgKG5hdGl2ZUxpYikge1xuXHRJbnRlcmZhY2VzRmFjdG9yeS5jYWxsKHRoaXMpO1xuXG5cdHRoaXMubmF0aXZlTGliID0gbmF0aXZlTGliO1xuXG5cdHRoaXMuX2RhdGEgPSB7fTtcblx0Zm9yICh2YXIgZGV2aWNlSW50ZXJmYWNlIGluIERldmljZUludGVyZmFjZSkge1xuXHRcdHRoaXMuX2RhdGFbRGV2aWNlSW50ZXJmYWNlW2RldmljZUludGVyZmFjZV1dID0ge1xuXHRcdFx0Y2xhc3M6IERldmljZUludGVyZmFjZVtkZXZpY2VJbnRlcmZhY2VdWzBdLnRvVXBwZXJDYXNlKCkgK1xuXHRcdFx0XHREZXZpY2VJbnRlcmZhY2VbZGV2aWNlSW50ZXJmYWNlXS5zdWJzdHJpbmcoMSkgKyAnUmVtb3RlRGV2aWNlJ1xuXHRcdH07XG5cdH1cbn1cblxuLy8gT3ZlcnJpZGVcbkRlZmF1bHRJbnRlcmZhY2VzRmFjdG9yeS5wcm90b3R5cGUubWFrZUludGVyZmFjZSA9IGZ1bmN0aW9uIChcblx0bmFtZSwgYWRkcmVzcywgZGV2aWNlSW50ZXJmYWNlXG4pIHtcblx0cmV0dXJuIG5ldyB0aGlzLm5hdGl2ZUxpYlt0aGlzLl9kYXRhW2RldmljZUludGVyZmFjZV0uY2xhc3NdKG5hbWUsIGFkZHJlc3MpO1xufTtcblxuLy8gT3ZlcnJpZGVcbkRlZmF1bHRJbnRlcmZhY2VzRmFjdG9yeS5wcm90b3R5cGUubWFrZUxvb3BlciA9IGZ1bmN0aW9uIChkZXZpY2VCaW5kaW5nKSB7XG5cdHJldHVybiBuZXcgdGhpcy5uYXRpdmVMaWIuQXN5bmNXb3JrZXJEZXZpY2VMb29wZXIoZGV2aWNlQmluZGluZyk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IERlZmF1bHRJbnRlcmZhY2VzRmFjdG9yeTtcbiIsImNvbnN0IENsaWVudEV2ZW50c0hhbmRsZXIgPSByZXF1aXJlKCcuLi9ldmVudHMvQ2xpZW50RXZlbnRzSGFuZGxlcicpO1xuY29uc3QgRGV2aWNlRXZlbnRzID0gcmVxdWlyZSgnLi4vLi4vY29uc3RhbnRzL0RldmljZUV2ZW50cycpO1xuXG5mdW5jdGlvbiBtYWtlRGV2aWNlSWQgKG5hbWUsIGFkZHJlc3MsIGRldmljZUludGVyZmFjZSkge1xuXHRyZXR1cm4gW25hbWUsIGFkZHJlc3MsIGRldmljZUludGVyZmFjZV0uam9pbignOicpO1xufTtcblxuZnVuY3Rpb24gZXZlbnRHZXR0ZXJOYW1lIChldmVudE5hbWUpIHtcblx0cmV0dXJuICdnZXRPbicgKyBldmVudE5hbWVbMF0udG9VcHBlckNhc2UoKSArIGV2ZW50TmFtZS5zdWJzdHIoMSk7XG59O1xuXG5mdW5jdGlvbiB0dXJuU2VyaWFsaXphYmxlIChvYmopIHtcblx0aWYgKG9iaiBpbnN0YW5jZW9mIEFycmF5KSB7XG5cdFx0cmV0dXJuIG9iajtcblx0fVxuXG5cdGNvbnN0IHNlcmlhbGl6YWJsZSA9IHt9O1xuXG5cdGZvciAodmFyIGtleSBpbiBvYmopIHtcblx0XHRpZiAoa2V5LmluZGV4T2YoJ2dldCcpID09PSAwKSB7XG5cdFx0XHRzZXJpYWxpemFibGVbYXR0ck5hbWUoa2V5KV0gPSBvYmpba2V5XSgpO1xuXHRcdH1cblx0fVxuXG5cdHJldHVybiBzZXJpYWxpemFibGU7XG5cblx0ZnVuY3Rpb24gYXR0ck5hbWUgKGdldHRlck5hbWUpIHtcblx0XHRnZXR0ZXJOYW1lID0gZ2V0dGVyTmFtZS5zdWJzdHIoMyk7XG5cdFx0cmV0dXJuIGdldHRlck5hbWUuY2hhckF0KDApLnRvTG93ZXJDYXNlKCkgKyBnZXR0ZXJOYW1lLnNsaWNlKDEpO1xuXHR9XG59XG5cbi8qKlxuICogQGF1dGhvciBsdWl6c3NiXG4gKi9cbmZ1bmN0aW9uIERldmljZUV2ZW50c0hhbmRsZXIgKGludGVyZmFjZUZhY3RvcnkpIHtcblx0Q2xpZW50RXZlbnRzSGFuZGxlci5jYWxsKHRoaXMpO1xuXG5cdHRoaXMuX2ludGVyZmFjZUZhY3RvcnkgPSBpbnRlcmZhY2VGYWN0b3J5O1xuXHR0aGlzLl9kZXZpY2VzUGVyQ2xpZW50ID0ge307XG59XG5cbkRldmljZUV2ZW50c0hhbmRsZXIucHJvdG90eXBlID0gT2JqZWN0LmFzc2lnbihcblx0T2JqZWN0LmNyZWF0ZShDbGllbnRFdmVudHNIYW5kbGVyLnByb3RvdHlwZSksXG5cdHsgY29uc3RydWN0b3I6IERldmljZUV2ZW50c0hhbmRsZXIgfVxuKTtcblxuLy8gT3ZlcnJpZGVcbkRldmljZUV2ZW50c0hhbmRsZXIucHJvdG90eXBlLmhhbmRsZU5ld0NsaWVudCA9IGZ1bmN0aW9uIChjbGllbnQpIHtcblx0Y29uc3QgZGV2aWNlc1BlckNsaWVudCA9IHRoaXMuX2RldmljZXNQZXJDbGllbnQ7XG5cdGRldmljZXNQZXJDbGllbnRbY2xpZW50LmlkXSA9IHt9O1xuXG5cdGNvbnN0IGZhY3RvcnkgPSB0aGlzLl9pbnRlcmZhY2VGYWN0b3J5O1xuXG5cdGNsaWVudC5vbihcblx0XHREZXZpY2VFdmVudHMuVG9TZXJ2ZXIuQ09OTkVDVCxcblx0XHRmdW5jdGlvbiAoZGF0YSwgbmFtZSwgYWRkcmVzcywgZGV2aWNlSW50ZXJmYWNlKSB7XG5cdFx0XHRjb25zb2xlLmxvZyhcblx0XHRcdFx0RGV2aWNlRXZlbnRzLlRvU2VydmVyLkNPTk5FQ1QsIG5hbWUsIGFkZHJlc3MsIGRldmljZUludGVyZmFjZVxuXHRcdFx0KTtcblxuXHRcdFx0Y29uc3QgZGV2aWNlSWQgPSBtYWtlRGV2aWNlSWQobmFtZSwgYWRkcmVzcywgZGV2aWNlSW50ZXJmYWNlKTtcblx0XHRcdGNvbnN0IGhhbmRsZXJFdmVudHMgPVxuXHRcdFx0XHREZXZpY2VFdmVudHMuZm9yRGV2aWNlKG5hbWUsIGFkZHJlc3MsIGRldmljZUludGVyZmFjZSk7XG5cblx0XHRcdGlmIChkZXZpY2VzUGVyQ2xpZW50W2NsaWVudC5pZF1bZGV2aWNlSWRdKSB7XG5cdFx0XHRcdGNsaWVudC5lbWl0KGhhbmRsZXJFdmVudHMuRnJvbVNlcnZlci5DT05ORUNUKTtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXG5cdFx0XHRjb25zdCBkZXZpY2UgPVxuXHRcdFx0XHRmYWN0b3J5Lm1ha2VJbnRlcmZhY2UobmFtZSwgYWRkcmVzcywgZGV2aWNlSW50ZXJmYWNlKTtcblx0XHRcdGNvbnN0IGxvb3BlciA9IGZhY3RvcnkubWFrZUxvb3BlcihkZXZpY2UpO1xuXG5cdFx0XHRjb25zdCBkZXZpY2VEYXRhID0gZGV2aWNlc1BlckNsaWVudFtjbGllbnQuaWRdW2RldmljZUlkXSA9IHtcblx0XHRcdFx0bmFtZTogbmFtZSxcblx0XHRcdFx0YWRkcmVzczogYWRkcmVzcyxcblx0XHRcdFx0ZGV2aWNlSW50ZXJmYWNlOiBkZXZpY2VJbnRlcmZhY2UsXG5cdFx0XHRcdGRldmljZTogZGV2aWNlLFxuXHRcdFx0XHRsb29wZXI6IGxvb3Blcixcblx0XHRcdFx0c3Vic2NyaWJlZEV2ZW50czoge30sXG5cdFx0XHRcdGhhbmRsZXJFdmVudHM6IGhhbmRsZXJFdmVudHNcblx0XHRcdH07XG5cblx0XHRcdGNsaWVudC5vbihcblx0XHRcdFx0aGFuZGxlckV2ZW50cy5Ub1NlcnZlci5TVUJTQ1JJQkVfVE9fRVZFTlQsXG5cdFx0XHRcdGZ1bmN0aW9uIChzdWJzY3JpcHRpb25EYXRhLCBldmVudE5hbWUsIGJyb2FkY2FzdHNEYXRhKSB7XG5cdFx0XHRcdFx0Y29uc29sZS5sb2coXG5cdFx0XHRcdFx0XHRoYW5kbGVyRXZlbnRzLlRvU2VydmVyLlNVQlNDUklCRV9UT19FVkVOVCxcblx0XHRcdFx0XHRcdGV2ZW50TmFtZSwgYnJvYWRjYXN0c0RhdGFcblx0XHRcdFx0XHQpO1xuXG5cdFx0XHRcdFx0aWYgKGRldmljZURhdGEuc3Vic2NyaWJlZEV2ZW50c1tldmVudE5hbWVdKSB7XG5cdFx0XHRcdFx0XHRjbGllbnQuZW1pdChcblx0XHRcdFx0XHRcdFx0aGFuZGxlckV2ZW50cy5Gcm9tU2VydmVyLlNVQlNDUklCRSwgZXZlbnROYW1lXG5cdFx0XHRcdFx0XHQpO1xuXHRcdFx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdGlmICghZGV2aWNlW2V2ZW50R2V0dGVyTmFtZShldmVudE5hbWUpXSkge1xuXHRcdFx0XHRcdFx0Y2xpZW50LmVtaXQoXG5cdFx0XHRcdFx0XHRcdGhhbmRsZXJFdmVudHMuRnJvbVNlcnZlci5TVUJTQ1JJQkVfRVJST1IsXG5cdFx0XHRcdFx0XHRcdGV2ZW50TmFtZSxcblx0XHRcdFx0XHRcdFx0eyBtZXNzYWdlOiAnaW50ZXJmYWNlIGhhcyBubyBldmVudCAnICsgZXZlbnROYW1lIH1cblx0XHRcdFx0XHRcdCk7XG5cdFx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0Y29uc3QgZXZlbnQgPSBkZXZpY2VbZXZlbnRHZXR0ZXJOYW1lKGV2ZW50TmFtZSldKCk7XG5cdFx0XHRcdFx0Y29uc3QgcmVzdWx0ID0gZXZlbnQucmVnaXN0ZXJDYWxsYmFjayhcblx0XHRcdFx0XHRcdGNsaWVudC5pZCxcblx0XHRcdFx0XHRcdGZ1bmN0aW9uIChldmVudERhdGEpIHtcblx0XHRcdFx0XHRcdFx0ZXZlbnREYXRhID0gdHVyblNlcmlhbGl6YWJsZShldmVudERhdGEpO1xuXG5cdFx0XHRcdFx0XHRcdGlmIChicm9hZGNhc3RzRGF0YSAmJiBjbGllbnQucm9vbSkge1xuXHRcdFx0XHRcdFx0XHRcdGNsaWVudC5tYW5hZ2VyLmJyb2FkY2FzdChcblx0XHRcdFx0XHRcdFx0XHRcdGNsaWVudC5yb29tLFxuXHRcdFx0XHRcdFx0XHRcdFx0aGFuZGxlckV2ZW50cy5Gcm9tU2VydmVyLkRFVklDRV9FVkVOVCxcblx0XHRcdFx0XHRcdFx0XHRcdGV2ZW50TmFtZSwgZXZlbnREYXRhXG5cdFx0XHRcdFx0XHRcdFx0KTtcblx0XHRcdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdFx0XHRjbGllbnQuZW1pdChcblx0XHRcdFx0XHRcdFx0XHRcdGhhbmRsZXJFdmVudHMuRnJvbVNlcnZlci5ERVZJQ0VfRVZFTlQsXG5cdFx0XHRcdFx0XHRcdFx0XHRldmVudE5hbWUsIGV2ZW50RGF0YVxuXHRcdFx0XHRcdFx0XHRcdCk7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHQpO1xuXG5cdFx0XHRcdFx0aWYgKHJlc3VsdCA9PT0gMCkge1xuXHRcdFx0XHRcdFx0ZGV2aWNlRGF0YS5zdWJzY3JpYmVkRXZlbnRzW2V2ZW50TmFtZV0gPSB0cnVlO1xuXG5cdFx0XHRcdFx0XHRjbGllbnQuZW1pdChcblx0XHRcdFx0XHRcdFx0aGFuZGxlckV2ZW50cy5Gcm9tU2VydmVyLlNVQlNDUklCRSwgZXZlbnROYW1lXG5cdFx0XHRcdFx0XHQpO1xuXG5cdFx0XHRcdFx0XHRpZiAoIWxvb3Blci5pc1J1bm5pbmcoKSkge1xuXHRcdFx0XHRcdFx0XHRsb29wZXIuc3RhcnQoKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0Y2xpZW50LmVtaXQoXG5cdFx0XHRcdFx0XHRcdGhhbmRsZXJFdmVudHMuRnJvbVNlcnZlci5TVUJTQ1JJQkVfRVJST1IsXG5cdFx0XHRcdFx0XHRcdGV2ZW50TmFtZSxcblx0XHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHRcdG1lc3NhZ2U6ICdjb3VsZCBub3QgcmVnaXN0ZXIgY2FsbGJhY2sgZm9yIGV2ZW50Jyxcblx0XHRcdFx0XHRcdFx0XHRjb2RlOiByZXN1bHRcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0KTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdClcblx0XHRcdFx0Lm9uKFxuXHRcdFx0XHRcdGhhbmRsZXJFdmVudHMuVG9TZXJ2ZXIuVU5TVUJTQ1JJQkVfRlJPTV9FVkVOVCxcblx0XHRcdFx0XHRmdW5jdGlvbiAoZGF0YSwgZXZlbnROYW1lKSB7XG5cdFx0XHRcdFx0XHRjb25zb2xlLmxvZyhcblx0XHRcdFx0XHRcdFx0aGFuZGxlckV2ZW50cy5Ub1NlcnZlci5VTlNVQlNDUklCRV9GUk9NX0VWRU5ULCBldmVudE5hbWVcblx0XHRcdFx0XHRcdCk7XG5cblx0XHRcdFx0XHRcdGlmIChkZXZpY2VbZXZlbnRHZXR0ZXJOYW1lKGV2ZW50TmFtZSldKSB7XG5cdFx0XHRcdFx0XHRcdGRldmljZVtldmVudEdldHRlck5hbWUoZXZlbnROYW1lKV0oKVxuXHRcdFx0XHRcdFx0XHRcdC51bnJlZ2lzdGVyQ2FsbGJhY2soY2xpZW50LmlkKTtcblx0XHRcdFx0XHRcdFx0ZGVsZXRlIGRldmljZURhdGEuc3Vic2NyaWJlZEV2ZW50c1tldmVudE5hbWVdO1xuXHRcdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdFx0Y29uc29sZS5sb2coJ3RyaWVkIHRvIHVuc3Vic2NyaWJlIGZyb20gaW5leGlzdGVudCBldmVudCcpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0KVxuXHRcdFx0XHQuZW1pdChoYW5kbGVyRXZlbnRzLkZyb21TZXJ2ZXIuQ09OTkVDVClcblx0XHRcdFx0LmJyb2FkY2FzdChcblx0XHRcdFx0XHREZXZpY2VFdmVudHMuRnJvbVNlcnZlci5ORVdfREVWSUNFLFxuXHRcdFx0XHRcdG5hbWUsIGFkZHJlc3MsIGRldmljZUludGVyZmFjZVxuXHRcdFx0XHQpO1xuXHRcdH1cblx0KVxuXHRcdC5vbihcblx0XHRcdERldmljZUV2ZW50cy5Ub1NlcnZlci5ESVNDT05ORUNULFxuXHRcdFx0ZnVuY3Rpb24gKGRhdGEsIG5hbWUsIGFkZHJlc3MsIGRldmljZUludGVyZmFjZSkge1xuXHRcdFx0XHRjb25zb2xlLmxvZyhcblx0XHRcdFx0XHREZXZpY2VFdmVudHMuVG9TZXJ2ZXIuRElTQ09OTkVDVCxcblx0XHRcdFx0XHRuYW1lLCBhZGRyZXNzLCBkZXZpY2VJbnRlcmZhY2Vcblx0XHRcdFx0KTtcblxuXHRcdFx0XHR0aGlzLl9raWxsRGV2aWNlKFxuXHRcdFx0XHRcdGNsaWVudCwgbWFrZURldmljZUlkKG5hbWUsIGFkZHJlc3MsIGRldmljZUludGVyZmFjZSlcblx0XHRcdFx0KTtcblx0XHRcdH0uYmluZCh0aGlzKVxuXHRcdClcblx0XHQub24oXG5cdFx0XHREZXZpY2VFdmVudHMuVG9TZXJ2ZXIuUVVFUllfREVWSUNFUyxcblx0XHRcdGZ1bmN0aW9uIChkYXRhKSB7XG5cdFx0XHRcdGNvbnN0IHJlc3VsdCA9IFtdO1xuXG5cdFx0XHRcdGZvciAodmFyIGNsaWVudElkIGluIGRldmljZXNQZXJDbGllbnQpIHtcblx0XHRcdFx0XHRmb3IgKHZhciBkZXZpY2VJZCBpbiBkZXZpY2VzUGVyQ2xpZW50W2NsaWVudElkXSkge1xuXHRcdFx0XHRcdFx0Y29uc3QgZGV2aWNlRGF0YSA9IGRldmljZXNQZXJDbGllbnRbY2xpZW50SWRdW2RldmljZUlkXTtcblx0XHRcdFx0XHRcdHJlc3VsdC5wdXNoKHtcblx0XHRcdFx0XHRcdFx0bmFtZTogZGV2aWNlRGF0YS5uYW1lLFxuXHRcdFx0XHRcdFx0XHRhZGRyZXNzOiBkZXZpY2VEYXRhLmFkZHJlc3MsXG5cdFx0XHRcdFx0XHRcdGRldmljZUludGVyZmFjZTogZGV2aWNlRGF0YS5kZXZpY2VJbnRlcmZhY2Vcblx0XHRcdFx0XHRcdH0pO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXG5cdFx0XHRcdGNsaWVudC5lbWl0KERldmljZUV2ZW50cy5Gcm9tU2VydmVyLkRFVklDRVNfUVVFUlksIHJlc3VsdCk7XG5cdFx0XHR9XG5cdFx0KTtcbn07XG5cbi8vIE92ZXJyaWRlXG5EZXZpY2VFdmVudHNIYW5kbGVyLnByb3RvdHlwZS5oYW5kbGVDbGllbnRHb25lID0gZnVuY3Rpb24gKGNsaWVudCkge1xuXHRmb3IgKHZhciBpZGVudGlmaWVyIGluIHRoaXMuX2RldmljZXNQZXJDbGllbnRbY2xpZW50LmlkXSkge1xuXHRcdHRoaXMuX2tpbGxEZXZpY2UoY2xpZW50LCBpZGVudGlmaWVyKTtcblx0fVxuXHRkZWxldGUgdGhpcy5fZGV2aWNlc1BlckNsaWVudFtjbGllbnQuaWRdO1xufTtcblxuRGV2aWNlRXZlbnRzSGFuZGxlci5wcm90b3R5cGUuX2tpbGxEZXZpY2UgPSBmdW5jdGlvbiAoY2xpZW50LCBpZGVudGlmaWVyKSB7XG5cdGNvbnN0IGRldmljZURhdGEgPSB0aGlzLl9kZXZpY2VzUGVyQ2xpZW50W2NsaWVudC5pZF1baWRlbnRpZmllcl07XG5cblx0aWYgKCFkZXZpY2VEYXRhKSB7XG5cdFx0cmV0dXJuO1xuXHR9XG5cblx0ZGV2aWNlRGF0YS5sb29wZXIuc3RvcCgpO1xuXG5cdGlmIChjbGllbnQuY29ubmVjdGVkKSB7XG5cdFx0Y2xpZW50Lm9mZihkZXZpY2VEYXRhLmhhbmRsZXJFdmVudHMuVG9TZXJ2ZXIuU1VCU0NSSUJFX1RPX0VWRU5UKVxuXHRcdFx0Lm9mZihkZXZpY2VEYXRhLmhhbmRsZXJFdmVudHMuVG9TZXJ2ZXIuVU5TVUJTQ1JJQkVfRlJPTV9FVkVOVCk7XG5cdH1cblxuXHRmb3IgKHZhciBldmVudE5hbWUgaW4gZGV2aWNlRGF0YS5zdWJzY3JpYmVkRXZlbnRzKSB7XG5cdFx0ZGV2aWNlRGF0YVxuXHRcdFx0LmRldmljZVtldmVudEdldHRlck5hbWUoZXZlbnROYW1lKV0oKS51bnJlZ2lzdGVyQ2FsbGJhY2soY2xpZW50LmlkKTtcblx0XHRkZWxldGUgZGV2aWNlRGF0YS5zdWJzY3JpYmVkRXZlbnRzW2V2ZW50TmFtZV07XG5cdH1cblxuXHRkZXZpY2VEYXRhLmRldmljZS5kaXNjb25uZWN0KCk7XG5cdGNsaWVudC5tYW5hZ2VyXG5cdFx0LmJyb2FkY2FzdChjbGllbnQucm9vbSwgZGV2aWNlRGF0YS5oYW5kbGVyRXZlbnRzLkZyb21TZXJ2ZXIuTE9TVCk7XG5cdGRlbGV0ZSB0aGlzLl9kZXZpY2VzUGVyQ2xpZW50W2NsaWVudC5pZF1baWRlbnRpZmllcl07XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IERldmljZUV2ZW50c0hhbmRsZXI7XG4iLCJjb25zdCBPYmplY3RFeCA9IHJlcXVpcmUoJy4uLy4uL3V0aWxzL09iamVjdEV4Jyk7XG5cbi8qKlxuICogQGF1dGhvciBsdWl6c3NiXG4gKi9cbmZ1bmN0aW9uIEludGVyZmFjZXNGYWN0b3J5ICgpIHt9XG5cbi8qKlxuICogQ3JlYXRlcyBhbiBpbnN0YW5jZSBvZiBhIGRldmljZSBpbnRlcmZhY2UuXG4gKiBAcGFyYW0ge3N0cmluZ30gbmFtZSBOYW1lIG9mIHRoZSBkZXZpY2UuXG4gKiBAcGFyYW0ge3N0cmluZ30gYWRkcmVzcyBBZGRyZXNzIG9mIHRoZSBkZXZpY2UuXG4gKiBAcGFyYW0ge3N0cmluZ30gZGV2aWNlSW50ZXJmYWNlIEludGVyZmFjZSBvZiB0aGUgZGV2aWNlLlxuICogQHJldHVybiB7SVZSUE5EZXZpY2VCaW5kaW5nfSBEZXZpY2UgaW50ZXJmYWNlIGJpbmRpbmcuXG4gKi9cbkludGVyZmFjZXNGYWN0b3J5LnByb3RvdHlwZS5tYWtlSW50ZXJmYWNlID0gT2JqZWN0RXguYWJzdHJhY3RNZXRob2Q7XG5cbi8qKlxuICogQ3JlYXRlIGEgbG9vcGVyIGZvciBhIGRldmljZSBpbnRlcmZhY2UuXG4gKiBAcGFyYW0ge0lWUlBORGV2aWNlQmluZGluZ30gYmluZGluZyBEZXZpY2UgaW50ZXJmYWNlIGJpbmRpbmcgdG8gYmUgbG9vcGVkLlxuICogQHJldHVybiB7SVZSUE5EZXZpY2VMb29wZXJ9IExvb3BlciBmb3IgdGhlIGRldmljZS5cbiAqL1xuSW50ZXJmYWNlc0ZhY3RvcnkucHJvdG90eXBlLm1ha2VMb29wZXIgPSBPYmplY3RFeC5hYnN0cmFjdE1ldGhvZDtcblxubW9kdWxlLmV4cG9ydHMgPSBJbnRlcmZhY2VzRmFjdG9yeTtcbiIsImNvbnN0IE9iamVjdEV4ID0gcmVxdWlyZSgnLi4vLi4vdXRpbHMvT2JqZWN0RXgnKTtcbmNvbnN0IGRlZmF1bHRQYXJhbXMgPSByZXF1aXJlKCcuLi9kZWZhdWx0UGFyYW1zJyk7XG5cbi8qKlxuICogQGF1dGhvciBsdWl6c3NiXG4gKi9cbmZ1bmN0aW9uIENsaWVudEV2ZW50c0hhbmRsZXIgKCkge31cblxuLyoqXG4gKiBIYW5kbGVzIGEgbmV3IGNsaWVudCBjb25uZWN0aW9uLlxuICogU2hvdWxkIHJlZ2lzdGVyIGZvciB0aGUgY2xpZW50J3MgZXZlbnRzIGFuZCBoYW5kbGUgdGhlbS5cbiAqIEBwYXJhbSB7Q2xpZW50Q29ubmVjdGlvbn0gY2xpZW50IFRoZSBuZXcgY29ubmVjdGlvbi5cbiAqL1xuQ2xpZW50RXZlbnRzSGFuZGxlci5wcm90b3R5cGUuaGFuZGxlTmV3Q2xpZW50ID0gT2JqZWN0RXguYWJzdHJhY3RNZXRob2Q7XG5cbi8qKlxuICogSGFuZGxlcyBhIGNsaWVudCBkaXNjb25uZWN0aW9uLlxuICogU2hvdWxkIHVuZG8gZXZlcnl0aGluZyBkb25lIGJ5IGhhbmRsZU5ld0NsaWVudCgpLlxuICogQHBhcmFtIHtDbGllbnRlQ29ubmVjdGlvbn0gY2xpZW50IFRoZSBnb25lIGNsaWVudC5cbiAqL1xuQ2xpZW50RXZlbnRzSGFuZGxlci5wcm90b3R5cGUuaGFuZGxlQ2xpZW50R29uZSA9IGZ1bmN0aW9uICgpIHt9O1xuXG5DbGllbnRFdmVudHNIYW5kbGVyLnByb3RvdHlwZS5zZW5kTWVzc2FnZSA9IGZ1bmN0aW9uIChmcm9tQ2xpZW50LCB0b0NsaWVudElkLCBldmVudCwgcGFyYW1zKSB7XG5cdGNvbnN0IHRvQ2xpZW50ID0gZnJvbUNsaWVudC5tYW5hZ2VyLmdldCh0b0NsaWVudElkKTtcblxuXHR0b0NsaWVudC5lbWl0V2l0aFBhcmFtcy5hcHBseShcblx0XHR0b0NsaWVudCxcblx0XHRbZXZlbnQsIE9iamVjdC5hc3NpZ24oZGVmYXVsdFBhcmFtcyhmcm9tQ2xpZW50LCB7fSksIHBhcmFtcyldXG5cdFx0XHQuY29uY2F0KEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cywgNCkpXG5cdCk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IENsaWVudEV2ZW50c0hhbmRsZXI7XG4iLCJjb25zdCBDbGllbnRFdmVudHNIYW5kbGVyID0gcmVxdWlyZSgnLi9DbGllbnRFdmVudHNIYW5kbGVyJyk7XG5jb25zdCBNYXN0ZXJTbGF2ZUluZm8gPSByZXF1aXJlKCcuL01hc3RlclNsYXZlSW5mbycpO1xuY29uc3QgZGVmYXVsdFBhcmFtcyA9IHJlcXVpcmUoJy4uL2RlZmF1bHRQYXJhbXMnKTtcbmNvbnN0IENsdXN0ZXJFdmVudHMgPSByZXF1aXJlKCcuLi8uLi9jb25zdGFudHMvQ2x1c3RlckV2ZW50cycpO1xuY29uc3QgQ2x1c3RlckpvaW5pbmdNb2RlID0gcmVxdWlyZSgnLi4vLi4vY29uc3RhbnRzL0NsdXN0ZXJKb2luaW5nTW9kZScpO1xuY29uc3QgTm9kZUNvbm5lY3Rpb25FdmVudHMgPSByZXF1aXJlKCcuLi8uLi9jb25zdGFudHMvTm9kZUNvbm5lY3Rpb25FdmVudHMnKTtcblxuLyoqXG4gKiBAYXV0aG9yIGx1aXpzc2JcbiAqL1xuZnVuY3Rpb24gQ2x1c3RlckV2ZW50c0hhbmRsZXIgKCkge1xuXHRDbGllbnRFdmVudHNIYW5kbGVyLmNhbGwodGhpcyk7XG5cdHRoaXMuX2NsdXN0ZXJzID0ge307XG5cblx0dGhpcy5fYm91bmRfbGVhdmVDbHVzdGVyID0gdGhpcy5fbGVhdmVDbHVzdGVyLmJpbmQodGhpcyk7XG59XG5cbkNsdXN0ZXJFdmVudHNIYW5kbGVyLnByb3RvdHlwZSA9IE9iamVjdC5hc3NpZ24oXG5cdE9iamVjdC5jcmVhdGUoQ2xpZW50RXZlbnRzSGFuZGxlci5wcm90b3R5cGUpLFxuXHR7IGNvbnN0cnVjdG9yOiBDbHVzdGVyRXZlbnRzSGFuZGxlciB9XG4pO1xuXG4vLyBPdmVycmlkZVxuQ2x1c3RlckV2ZW50c0hhbmRsZXIucHJvdG90eXBlLmhhbmRsZU5ld0NsaWVudCA9IGZ1bmN0aW9uIChjbGllbnQpIHtcblx0Y29uc3QgbGVhdmVDbHVzdGVyID0gdGhpcy5fYm91bmRfbGVhdmVDbHVzdGVyO1xuXHRjb25zdCBjbHVzdGVycyA9IHRoaXMuX2NsdXN0ZXJzO1xuXHRjb25zdCBtZXNzYWdlID0gZnVuY3Rpb24gKHRvUGVlcklkLCBldmVudCwgZGF0YSkge1xuXHRcdHRoaXMuc2VuZE1lc3NhZ2UoXG5cdFx0XHRjbGllbnQsIHRvUGVlcklkLCBldmVudCwgdGhpcy5fZGVmYXVsdFBhcmFtcyhjbGllbnQsIHt9KSwgZGF0YVxuXHRcdCk7XG5cdH0uYmluZCh0aGlzKTtcblxuXHRjbGllbnQub24oXG5cdFx0Q2x1c3RlckV2ZW50cy5Ub1NlcnZlci5KT0lOX0NMVVNURVIsXG5cdFx0ZnVuY3Rpb24gKGRhdGEsIHJvb20sIHByZWZlcnJlZENvbm5lY3Rpb24sIGpvaW5pbmdNb2RlKSB7XG5cdFx0XHRjb25zb2xlLmxvZyhDbHVzdGVyRXZlbnRzLlRvU2VydmVyLkpPSU5fQ0xVU1RFUik7XG5cblx0XHRcdGlmIChjbHVzdGVyc1tyb29tXSAmJiBjbHVzdGVyc1tyb29tXS5nZXQoY2xpZW50LmlkKSkge1xuXHRcdFx0XHRyZXR1cm4gZmFpbCgnbm9kZSBhbHJlYWR5IGpvaW5lZCBjbHVzdGVyJyk7XG5cdFx0XHR9XG5cblx0XHRcdGlmICghY2xpZW50LnJvb20pIHtcblx0XHRcdFx0Y2xpZW50LmpvaW4ocm9vbSk7XG5cdFx0XHR9XG5cblx0XHRcdGNvbnN0IG5vZGVEYXRhID0ge1xuXHRcdFx0XHRpZDogY2xpZW50LmlkLCBjb25uZWN0aW9uTWV0aG9kOiBwcmVmZXJyZWRDb25uZWN0aW9uXG5cdFx0XHR9O1xuXG5cdFx0XHRqb2luaW5nTW9kZSA9IGpvaW5pbmdNb2RlIHx8IENsdXN0ZXJKb2luaW5nTW9kZS5BTlk7XG5cdFx0XHRjbHVzdGVyc1tyb29tXSA9IGNsdXN0ZXJzW3Jvb21dIHx8IG5ldyBNYXN0ZXJTbGF2ZUluZm8oKTtcblxuXHRcdFx0dmFyIHRlbGxTbGF2ZXMgPSBmYWxzZTtcblxuXHRcdFx0aWYgKGNsdXN0ZXJzW3Jvb21dLm1hc3Rlcikge1xuXHRcdFx0XHRpZiAoam9pbmluZ01vZGUgPT09IENsdXN0ZXJKb2luaW5nTW9kZS5NQVNURVIpIHtcblx0XHRcdFx0XHRyZXR1cm4gZmFpbCgnY2x1c3RlciBhbHJlYWR5IGhhcyBtYXN0ZXInKTtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRjbHVzdGVyc1tyb29tXS5hZGRTbGF2ZShub2RlRGF0YSk7XG5cdFx0XHRcdH1cblx0XHRcdH0gZWxzZSBpZiAoam9pbmluZ01vZGUgJiBDbHVzdGVySm9pbmluZ01vZGUuTUFTVEVSKSB7XG5cdFx0XHRcdGNsdXN0ZXJzW3Jvb21dLnNldE1hc3Rlcihub2RlRGF0YSk7XG5cdFx0XHRcdHRlbGxTbGF2ZXMgPSB0cnVlO1xuXHRcdFx0XHRjb25zb2xlLmxvZyhgUm9vbSAnJHtyb29tfScgaGFzIG1hc3RlcmApO1xuXHRcdFx0fSBlbHNlIGlmIChqb2luaW5nTW9kZSAmIENsdXN0ZXJKb2luaW5nTW9kZS5TTEFWRV9XQUlUKSB7XG5cdFx0XHRcdGNsdXN0ZXJzW3Jvb21dLmFkZFNsYXZlKG5vZGVEYXRhKTtcblx0XHRcdFx0Y29uc29sZS5sb2coYFJvb20gJyR7cm9vbX0nIGhhcyAke2NsdXN0ZXJzW3Jvb21dLmNvdW50fSBwYXJ0aWNpcGFudHNgKTtcblx0XHRcdH0gZWxzZSBpZiAoam9pbmluZ01vZGUgJiBDbHVzdGVySm9pbmluZ01vZGUuU0xBVkVfTk9XKSB7XG5cdFx0XHRcdHJldHVybiBmYWlsKCdjbHVzdGVyIGhhcyBubyBtYXN0ZXInKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHJldHVybiBmYWlsKCd1bmtub3duIGpvaW5pbmcgbW9kZScpO1xuXHRcdFx0fVxuXG5cdFx0XHRjbGllbnQuZW1pdChcblx0XHRcdFx0Q2x1c3RlckV2ZW50cy5Gcm9tU2VydmVyLkpPSU5FRF9DTFVTVEVSLCBjbHVzdGVyc1tyb29tXS5tYXN0ZXJcblx0XHRcdCk7XG5cblx0XHRcdGlmICh0ZWxsU2xhdmVzKSB7XG5cdFx0XHRcdGNsaWVudC5icm9hZGNhc3QoQ2x1c3RlckV2ZW50cy5Gcm9tU2VydmVyLk1BU1RFUl9KT0lORUQsIG5vZGVEYXRhKTtcblx0XHRcdH1cblxuXHRcdFx0ZnVuY3Rpb24gZmFpbCAobWVzc2FnZSkge1xuXHRcdFx0XHRjbGllbnQuZW1pdChcblx0XHRcdFx0XHRDbHVzdGVyRXZlbnRzLkZyb21TZXJ2ZXIuSk9JTl9GQUlMRUQsIHsgbWVzc2FnZTogbWVzc2FnZSB9XG5cdFx0XHRcdCk7XG5cdFx0XHR9XG5cdFx0fVxuXHQpXG5cdFx0Lm9uKFxuXHRcdFx0Tm9kZUNvbm5lY3Rpb25FdmVudHMuVG9TZXJ2ZXIuUEVFS19DTFVTVEVSLFxuXHRcdFx0ZnVuY3Rpb24gKGRhdGEsIHJvb20pIHtcblx0XHRcdFx0Y29uc3QgY2x1c3RlckRhdGEgPSB7fTtcblx0XHRcdFx0aWYgKGNsdXN0ZXJzW3Jvb21dKSB7XG5cdFx0XHRcdFx0Y2x1c3RlckRhdGEucGFydGljaXBhbnRzID0gY2x1c3RlcnMuY291bnQ7XG5cdFx0XHRcdFx0Y2x1c3RlckRhdGEubm9kZXMgPSBjbHVzdGVyc1tyb29tXTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGNsaWVudC5lbWl0KENsdXN0ZXJFdmVudHMuRnJvbVNlcnZlci5QRUVLRUQsIGNsdXN0ZXJEYXRhKTtcblx0XHRcdH1cblx0XHQpXG5cdFx0Lm9uKFxuXHRcdFx0Tm9kZUNvbm5lY3Rpb25FdmVudHMuVG9TZXJ2ZXIuT0ZGRVJfQ09OTkVDVElPTixcblx0XHRcdGZ1bmN0aW9uIChkYXRhLCB0b1BlZXJJZCwgb2ZmZXIpIHtcblx0XHRcdFx0Y29uc29sZS5sb2coXG5cdFx0XHRcdFx0Tm9kZUNvbm5lY3Rpb25FdmVudHMuVG9TZXJ2ZXIuT0ZGRVJfQ09OTkVDVElPTixcblx0XHRcdFx0XHRkYXRhLCB0b1BlZXJJZFxuXHRcdFx0XHQpO1xuXHRcdFx0XHRtZXNzYWdlKFxuXHRcdFx0XHRcdHRvUGVlcklkLCBOb2RlQ29ubmVjdGlvbkV2ZW50cy5Gcm9tU2VydmVyLlJFQ0VJVkVEX09GRkVSLFxuXHRcdFx0XHRcdG9mZmVyXG5cdFx0XHRcdCk7XG5cdFx0XHR9XG5cdFx0KVxuXHRcdC5vbihcblx0XHRcdE5vZGVDb25uZWN0aW9uRXZlbnRzLlRvU2VydmVyLkFOU1dFUl9PRkZFUixcblx0XHRcdGZ1bmN0aW9uIChkYXRhLCB0b1BlZXJJZCwgYW5zd2VyKSB7XG5cdFx0XHRcdGNvbnNvbGUubG9nKFxuXHRcdFx0XHRcdE5vZGVDb25uZWN0aW9uRXZlbnRzLlRvU2VydmVyLkFOU1dFUl9PRkZFUiwgZGF0YSwgdG9QZWVySWRcblx0XHRcdFx0KTtcblx0XHRcdFx0bWVzc2FnZShcblx0XHRcdFx0XHR0b1BlZXJJZCwgTm9kZUNvbm5lY3Rpb25FdmVudHMuRnJvbVNlcnZlci5SRUNFSVZFRF9BTlNXRVIsXG5cdFx0XHRcdFx0YW5zd2VyXG5cdFx0XHRcdCk7XG5cdFx0XHR9XG5cdFx0KVxuXHRcdC5vbihcblx0XHRcdE5vZGVDb25uZWN0aW9uRXZlbnRzLlRvU2VydmVyLkZBSUxfQU5TV0VSLFxuXHRcdFx0ZnVuY3Rpb24gKGRhdGEsIHRvUGVlcklkLCBlcnJvcikge1xuXHRcdFx0XHRjb25zb2xlLmxvZyhcblx0XHRcdFx0XHROb2RlQ29ubmVjdGlvbkV2ZW50cy5Ub1NlcnZlci5GQUlMX0FOU1dFUiwgZGF0YSwgdG9QZWVySWRcblx0XHRcdFx0KTtcblx0XHRcdFx0bWVzc2FnZShcblx0XHRcdFx0XHR0b1BlZXJJZCwgTm9kZUNvbm5lY3Rpb25FdmVudHMuRnJvbVNlcnZlci5BTlNXRVJfRkFJTEVELFxuXHRcdFx0XHRcdGVycm9yXG5cdFx0XHRcdCk7XG5cdFx0XHR9XG5cdFx0KVxuXHRcdC5vbihcblx0XHRcdE5vZGVDb25uZWN0aW9uRXZlbnRzLlRvU2VydmVyLkNBTkRJREFURV9QRUVSLFxuXHRcdFx0ZnVuY3Rpb24gKGRhdGEsIHRvUGVlcklkLCBjYW5kaWRhdGUpIHtcblx0XHRcdFx0Y29uc29sZS5sb2coXG5cdFx0XHRcdFx0Tm9kZUNvbm5lY3Rpb25FdmVudHMuVG9TZXJ2ZXIuQ0FORElEQVRFX1BFRVIsIGRhdGEsIHRvUGVlcklkXG5cdFx0XHRcdCk7XG5cdFx0XHRcdG1lc3NhZ2UoXG5cdFx0XHRcdFx0dG9QZWVySWQsIE5vZGVDb25uZWN0aW9uRXZlbnRzLkZyb21TZXJ2ZXIuUkVDRUlWRURfQ0FORElEQVRFLFxuXHRcdFx0XHRcdGNhbmRpZGF0ZVxuXHRcdFx0XHQpO1xuXHRcdFx0fVxuXHRcdClcblx0XHQub24oQ2x1c3RlckV2ZW50cy5Ub1NlcnZlci5RVUlUX0NMVVNURVIsIGZ1bmN0aW9uIChkYXRhKSB7XG5cdFx0XHRjb25zb2xlLmxvZyhDbHVzdGVyRXZlbnRzLlRvU2VydmVyLlFVSVRfQ0xVU1RFUiwgZGF0YSk7XG5cdFx0XHRsZWF2ZUNsdXN0ZXIoY2xpZW50KTtcblx0XHR9KTtcblxuXHRjbGllbnQub24ud2lsbEVtaXQuYWRkKHRoaXMuX2RlZmF1bHRQYXJhbXMsIHRoaXMpO1xufTtcblxuLy8gT3ZlcnJpZGVcbkNsdXN0ZXJFdmVudHNIYW5kbGVyLnByb3RvdHlwZS5oYW5kbGVDbGllbnRHb25lID0gZnVuY3Rpb24gKGNsaWVudCkge1xuXHRjb25zb2xlLmxvZygnZGlzY29ubmVjdCcpO1xuXG5cdGNvbnN0IHJvb20gPSBjbGllbnQucm9vbTtcblxuXHRpZiAocm9vbSkge1xuXHRcdHRoaXMuX2xlYXZlQ2x1c3RlcihjbGllbnQpO1xuXHR9XG59O1xuXG5DbHVzdGVyRXZlbnRzSGFuZGxlci5wcm90b3R5cGUuX2dlbmVyYWxCcm9hZGNhc3QgPSBmdW5jdGlvbiAoY2xpZW50KSB7XG5cdGNvbnN0IGJpbmRpbmcgPSBjbGllbnQubWFuYWdlci5vbi53aWxsQnJvYWRjYXN0LmFkZChmdW5jdGlvbiAoZGF0YSkge1xuXHRcdGRlZmF1bHRQYXJhbXMoY2xpZW50LCBkYXRhKTtcblx0XHR0aGlzLl9kZWZhdWx0UGFyYW1zKGNsaWVudCwgZGF0YSk7XG5cdFx0YmluZGluZy5kZXRhY2goKTtcblx0fS5iaW5kKHRoaXMpKTtcblxuXHRjbGllbnQubWFuYWdlci5icm9hZGNhc3QuYXBwbHkoXG5cdFx0Y2xpZW50Lm1hbmFnZXIsIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSlcblx0KTtcbn07XG5cbkNsdXN0ZXJFdmVudHNIYW5kbGVyLnByb3RvdHlwZS5fbGVhdmVDbHVzdGVyID0gZnVuY3Rpb24gKGNsaWVudCkge1xuXHRpZiAoIXRoaXMuX2NsdXN0ZXJzW2NsaWVudC5yb29tXSkge1xuXHRcdHJldHVybjtcblx0fVxuXG5cdGNvbnNvbGUubG9nKCdsZWF2ZUNsdXN0ZXInLCBjbGllbnQucm9vbSwgdGhpcy5fY2x1c3RlcnNbY2xpZW50LnJvb21dLmNvdW50KTtcblxuXHRjb25zdCByb29tID0gY2xpZW50LnJvb207XG5cblx0dHJ5IHtcblx0XHRjbGllbnQubGVhdmUoKTtcblx0fSBjYXRjaCAoZXgpIHt9XG5cblx0Y29uc3Qgd2FzTWFzdGVyID0gdGhpcy5fY2x1c3RlcnNbcm9vbV0ubWFzdGVyICYmXG5cdFx0dGhpcy5fY2x1c3RlcnNbcm9vbV0ubWFzdGVyLmlkID09PSBjbGllbnQuaWQ7XG5cblx0dGhpcy5fZ2VuZXJhbEJyb2FkY2FzdChcblx0XHRjbGllbnQsIHJvb20sIENsdXN0ZXJFdmVudHMuRnJvbVNlcnZlci5QRUVSX0dPTkUsIHdhc01hc3RlclxuXHQpO1xuXG5cdGlmICh3YXNNYXN0ZXIpIHtcblx0XHRjb25zb2xlLmxvZyhgUm9vbSAnJHtyb29tfScgZGVhZGApO1xuXG5cdFx0Zm9yIChsZXQgY2xpZW50SWQgaW4gdGhpcy5fY2x1c3RlcnNbcm9vbV0uc2xhdmVzKSB7XG5cdFx0XHRjbGllbnQubWFuYWdlci5nZXQoY2xpZW50SWQpLmxlYXZlKCk7XG5cdFx0fVxuXG5cdFx0ZGVsZXRlIHRoaXMuX2NsdXN0ZXJzW3Jvb21dO1xuXHR9XG59O1xuXG5DbHVzdGVyRXZlbnRzSGFuZGxlci5wcm90b3R5cGUuX2RlZmF1bHRQYXJhbXMgPSBmdW5jdGlvbiAoc2VuZGVyLCBkYXRhKSB7XG5cdGRhdGEgPSBkYXRhIHx8IHt9O1xuXG5cdGlmIChzZW5kZXIucm9vbSAmJiB0aGlzLl9jbHVzdGVyc1tzZW5kZXIucm9vbV0pIHtcblx0XHRkYXRhLm5vZGVEYXRhID0gdGhpcy5fY2x1c3RlcnNbc2VuZGVyLnJvb21dLmdldChzZW5kZXIuaWQpO1xuXHR9XG5cblx0cmV0dXJuIGRhdGE7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IENsdXN0ZXJFdmVudHNIYW5kbGVyO1xuIiwiY29uc3QgT2JqZWN0RXggPSByZXF1aXJlKCcuLi8uLi91dGlscy9PYmplY3RFeCcpO1xuXG4vKipcbiAqIEBhdXRob3IgbHVpenNzYlxuICovXG5mdW5jdGlvbiBNYXN0ZXJTbGF2ZUluZm8gKCkge1xuXHR0aGlzLm1hc3RlciA9IHVuZGVmaW5lZDtcblx0dGhpcy5zbGF2ZXMgPSB7fTtcbn1cblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KFxuXHRNYXN0ZXJTbGF2ZUluZm8ucHJvdG90eXBlLCAnY291bnQnLCBPYmplY3RFeC5yZWFkT25seShmdW5jdGlvbiAoKSB7XG5cdFx0cmV0dXJuICEhdGhpcy5tYXN0ZXIgKyBPYmplY3Qua2V5cyh0aGlzLnNsYXZlcykubGVuZ3RoO1xuXHR9KVxuKTtcblxuTWFzdGVyU2xhdmVJbmZvLnByb3RvdHlwZS5nZXQgPSBmdW5jdGlvbiAobm9kZUlkKSB7XG5cdHJldHVybiB0aGlzLm1hc3RlciAmJiBub2RlSWQgPT09IHRoaXMubWFzdGVyLmlkXG5cdFx0PyB0aGlzLm1hc3RlciA6IHRoaXMuc2xhdmVzW25vZGVJZF07XG59O1xuXG5NYXN0ZXJTbGF2ZUluZm8ucHJvdG90eXBlLnNldE1hc3RlciA9IGZ1bmN0aW9uIChub2RlRGF0YSkge1xuXHR0aGlzLm1hc3RlciA9IG5vZGVEYXRhO1xufTtcblxuTWFzdGVyU2xhdmVJbmZvLnByb3RvdHlwZS5hZGRTbGF2ZSA9IGZ1bmN0aW9uIChub2RlRGF0YSkge1xuXHR0aGlzLnNsYXZlc1tub2RlRGF0YS5pZF0gPSBub2RlRGF0YTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gTWFzdGVyU2xhdmVJbmZvO1xuIiwiY29uc3QgQ2xpZW50RXZlbnRzSGFuZGxlciA9IHJlcXVpcmUoJy4vQ2xpZW50RXZlbnRzSGFuZGxlcicpO1xuY29uc3QgTm9kZUV2ZW50cyA9IHJlcXVpcmUoJy4uLy4uL2NvbnN0YW50cy9Ob2RlRXZlbnRzJyk7XG5cbmZ1bmN0aW9uIE5vZGVFdmVudHNIYW5kbGVyICgpIHtcblx0Q2xpZW50RXZlbnRzSGFuZGxlci5jYWxsKHRoaXMpO1xuXHR0aGlzLl9jbGllbnRzUm9vbXMgPSB7fTtcbn1cblxuTm9kZUV2ZW50c0hhbmRsZXIucHJvdG90eXBlID0gT2JqZWN0LmFzc2lnbihcblx0T2JqZWN0LmNyZWF0ZShDbGllbnRFdmVudHNIYW5kbGVyLnByb3RvdHlwZSksXG5cdHsgY29uc3RydWN0b3I6IE5vZGVFdmVudHNIYW5kbGVyIH1cbik7XG5cbi8vIE92ZXJyaWRlXG5Ob2RlRXZlbnRzSGFuZGxlci5wcm90b3R5cGUuaGFuZGxlTmV3Q2xpZW50ID0gZnVuY3Rpb24gKGNsaWVudCkge1xuXHRjb25zdCAkdGhpcyA9IHRoaXM7XG5cblx0Y2xpZW50XG5cdFx0Lm9uKE5vZGVFdmVudHMuVG9TZXJ2ZXIuU0VORF9EQVRBLCBmdW5jdGlvbiAoZGF0YSwgZGF0YVBhY2thZ2UpIHtcblx0XHRcdGlmIChkYXRhUGFja2FnZS5yZWNpcGllbnQpIHtcblx0XHRcdFx0JHRoaXMuc2VuZE1lc3NhZ2UoXG5cdFx0XHRcdFx0Y2xpZW50LFxuXHRcdFx0XHRcdGRhdGFQYWNrYWdlLnJlY2lwaWVudCxcblx0XHRcdFx0XHROb2RlRXZlbnRzLkZyb21TZXJ2ZXIuUkVDRUlWRURfREFUQSwge30sIGRhdGFQYWNrYWdlXG5cdFx0XHRcdCk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRjbGllbnQuYnJvYWRjYXN0KFxuXHRcdFx0XHRcdE5vZGVFdmVudHMuRnJvbVNlcnZlci5SRUNFSVZFRF9EQVRBLCBkYXRhUGFja2FnZVxuXHRcdFx0XHQpO1xuXHRcdFx0fVxuXHRcdH0pXG5cdFx0Lm9uKE5vZGVFdmVudHMuVG9TZXJ2ZXIuUVVJVCwgZnVuY3Rpb24gKGRhdGEpIHtcblx0XHRcdGNsaWVudC5icm9hZGNhc3QoTm9kZUV2ZW50cy5Gcm9tU2VydmVyLk5PREVfR09ORSwgY2xpZW50LmlkKTtcblx0XHR9KTtcblxuXHR0aGlzLl9iaW5kaW5nID0gY2xpZW50Lm9uLmpvaW5lZFJvb20uYWRkKGZ1bmN0aW9uIChzZW5kZXIpIHtcblx0XHQkdGhpcy5fY2xpZW50c1Jvb21zW3NlbmRlci5pZF0gPSBzZW5kZXIucm9vbTtcblx0fSk7XG5cblx0dGhpcy5fY2xpZW50c1Jvb21zW2NsaWVudC5pZF0gPSBjbGllbnQucm9vbTtcbn07XG5cbi8vIE92ZXJyaWRlXG5Ob2RlRXZlbnRzSGFuZGxlci5wcm90b3R5cGUuaGFuZGxlQ2xpZW50R29uZSA9IGZ1bmN0aW9uIChjbGllbnQpIHtcblx0Y2xpZW50Lm1hbmFnZXIuYnJvYWRjYXN0KFxuXHRcdHRoaXMuX2NsaWVudHNSb29tc1tjbGllbnQuaWRdLFxuXHRcdE5vZGVFdmVudHMuRnJvbVNlcnZlci5OT0RFX0dPTkUsIGNsaWVudC5pZFxuXHQpO1xuXG5cdHRoaXMuX2JpbmRpbmcuZGV0YWNoKCk7XG5cblx0ZGVsZXRlIHRoaXMuX2NsaWVudHNSb29tc1tjbGllbnQuaWRdO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBOb2RlRXZlbnRzSGFuZGxlcjtcbiIsImNvbnN0IENsaWVudEV2ZW50c0hhbmRsZXIgPSByZXF1aXJlKCcuL0NsaWVudEV2ZW50c0hhbmRsZXInKTtcbmNvbnN0IFJvb21FdmVudHMgPSByZXF1aXJlKCcuLi8uLi9jb25zdGFudHMvUm9vbUV2ZW50cycpO1xuXG5mdW5jdGlvbiBSb29tRXZlbnRzSGFuZGxlciAoKSB7XG5cdENsaWVudEV2ZW50c0hhbmRsZXIuY2FsbCh0aGlzKTtcbn1cblxuUm9vbUV2ZW50c0hhbmRsZXIucHJvdG90eXBlID0gT2JqZWN0LmFzc2lnbihcblx0T2JqZWN0LmNyZWF0ZShDbGllbnRFdmVudHNIYW5kbGVyLnByb3RvdHlwZSksXG5cdHsgY29uc3RydWN0b3I6IFJvb21FdmVudHNIYW5kbGVyIH1cbik7XG5cbi8vIE92ZXJyaWRlXG5Sb29tRXZlbnRzSGFuZGxlci5wcm90b3R5cGUuaGFuZGxlTmV3Q2xpZW50ID0gZnVuY3Rpb24gKGNsaWVudCkge1xuXHRjbGllbnRcblx0XHQub24oUm9vbUV2ZW50cy5Ub1NlcnZlci5KT0lOLCBmdW5jdGlvbiAoZGF0YSwgcm9vbSkge1xuXHRcdFx0Y2xpZW50LmpvaW4ocm9vbSk7XG5cdFx0fSlcblx0XHQub24oUm9vbUV2ZW50cy5Ub1NlcnZlci5MRUFWRSwgZnVuY3Rpb24gKGRhdGEpIHtcblx0XHRcdGNsaWVudC5sZWF2ZSgpO1xuXHRcdH0pO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBSb29tRXZlbnRzSGFuZGxlcjtcbiIsIm1vZHVsZS5leHBvcnRzID0ge1xuXHRDbGllbnRDb25uZWN0aW9uOiByZXF1aXJlKCcuL2Nvbm5lY3Rpb25zL0NsaWVudENvbm5lY3Rpb24nKSxcblx0U29ja2V0SU9TZXJ2ZXJDb25uZWN0aW9uOiByZXF1aXJlKCcuL2Nvbm5lY3Rpb25zL1NvY2tldElPQ2xpZW50Q29ubmVjdGlvbicpLFxuXG5cdFJvb21FdmVudHNIYW5kbGVyOiByZXF1aXJlKCcuL2V2ZW50cy9Sb29tRXZlbnRzSGFuZGxlcicpLFxuXHRSb29tRXZlbnRzOiByZXF1aXJlKCcuLi9jb25zdGFudHMvUm9vbUV2ZW50cycpLFxuXG5cdENsaWVudHNNYW5hZ2VyOiByZXF1aXJlKCcuL2Nvbm5lY3Rpb25zL0NsaWVudHNNYW5hZ2VyJyksXG5cdFNvY2tldElPQ2xpZW50c01hbmFnZXI6IHJlcXVpcmUoJy4vY29ubmVjdGlvbnMvU29ja2V0SU9DbGllbnRzTWFuYWdlcicpLFxuXG5cdENsaWVudEV2ZW50c0hhbmRsZXI6IHJlcXVpcmUoJy4vZXZlbnRzL0NsaWVudEV2ZW50c0hhbmRsZXInKSxcblx0Q2x1c3RlckV2ZW50c0hhbmRsZXI6IHJlcXVpcmUoJy4vZXZlbnRzL0NsdXN0ZXJFdmVudHNIYW5kbGVyJyksXG5cdE5vZGVFdmVudHNIYW5kbGVyOiByZXF1aXJlKCcuL2V2ZW50cy9Ob2RlRXZlbnRzSGFuZGxlcicpLFxuXG5cdFdlYkNsdXN0ZXJTdXBwb3J0OiByZXF1aXJlKCcuL1dlYkNsdXN0ZXJTdXBwb3J0JyksXG5cblx0T2JqZWN0RXg6IHJlcXVpcmUoJy4uL3V0aWxzL09iamVjdEV4JyksXG5cdFJhbmRvbVV0aWxzOiByZXF1aXJlKCcuLi91dGlscy9SYW5kb21VdGlscycpLFxuXG5cdGNvbXB1dGVUaWx0OiByZXF1aXJlKCcuLi91dGlscy9jb21wdXRlVGlsdCcpLFxuXHR0b0RlZ3JlZXM6IHJlcXVpcmUoJy4uL3V0aWxzL3RvRGVncmVlcycpLFxuXG5cdERldmljZUV2ZW50c0hhbmRsZXI6IHJlcXVpcmUoJy4vZGV2aWNlcy9EZXZpY2VFdmVudHNIYW5kbGVyJyksXG5cdERldmljZUV2ZW50czogcmVxdWlyZSgnLi4vY29uc3RhbnRzL0RldmljZUV2ZW50cycpLFxuXHRJbnRlcmZhY2VzRmFjdG9yeTogcmVxdWlyZSgnLi9kZXZpY2VzL0ludGVyZmFjZXNGYWN0b3J5JyksXG5cdERlZmF1bHRJbnRlcmZhY2VzRmFjdG9yeTogcmVxdWlyZSgnLi9kZXZpY2VzL0RlZmF1bHRJbnRlcmZhY2VzRmFjdG9yeScpXG59O1xuIiwiY29uc3QgTGl2dmNsaWJFcnJvciA9IHJlcXVpcmUoJy4uL2V4Y2VwdGlvbnMvTGl2dmNsaWJFcnJvci5qcycpO1xuY29uc3QgU2lnbmFsID0gcmVxdWlyZSgnc2lnbmFscycpO1xuXG4vKipcbiAqIEBhdXRob3IgbHVpenNzYlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IHtcblx0Y2FsbERlbGVnYXRlOiBmdW5jdGlvbiAob2JqLCBkZWxlZ2F0ZU5hbWUpIHtcblx0XHRpZiAob2JqW2RlbGVnYXRlTmFtZV0pIHtcblx0XHRcdG9ialtkZWxlZ2F0ZU5hbWVdLmFwcGx5KFxuXHRcdFx0XHRudWxsLFxuXHRcdFx0XHRbb2JqXS5jb25jYXQoQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAyKSlcblx0XHRcdCk7XG5cdFx0fVxuXHR9LFxuXHRhYnN0cmFjdE1ldGhvZDogZnVuY3Rpb24gKCkge1xuXHRcdHRocm93IG5ldyBMaXZ2Y2xpYkVycm9yKCdhYnN0cmFjdCBtZXRob2Qgbm90IGltcGxlbWVudGVkJywgOTIpO1xuXHR9LFxuXHRub3RJbXBsZW1lbnRlZDogZnVuY3Rpb24gKCkge1xuXHRcdHRocm93IG5ldyBMaXZ2Y2xpYkVycm9yKCdub3QgaW1wbGVtZW50ZWQnLCAzOSk7XG5cdH0sXG5cdGlzRnVuY3Rpb246IGZ1bmN0aW9uIChvYmopIHtcblx0XHRyZXR1cm4gb2JqICYmIHt9LnRvU3RyaW5nLmNhbGwob2JqKSA9PT0gJ1tvYmplY3QgRnVuY3Rpb25dJztcblx0fSxcblx0aXNPYmplY3Q6IGZ1bmN0aW9uIChvYmopIHtcblx0XHRyZXR1cm4gb2JqICYmIG9iai50b1N0cmluZygpLmluZGV4T2YoJ1tvYmplY3QnKSA9PT0gMDtcblx0fSxcblx0aXNCb29sZWFuOiBmdW5jdGlvbiAob2JqKSB7XG5cdFx0cmV0dXJuIHR5cGVvZiBvYmogPT09ICdib29sZWFuJztcblx0fSxcblx0cmVhZE9ubHk6IGZ1bmN0aW9uIChnZXQsIGNvbmZpZ3VyYWJsZSwgZW51bWVyYWJsZSkge1xuXHRcdGlmIChjb25maWd1cmFibGUgPT09IHVuZGVmaW5lZCkge1xuXHRcdFx0Y29uZmlndXJhYmxlID0gdHJ1ZTtcblx0XHR9XG5cblx0XHRpZiAoZW51bWVyYWJsZSA9PT0gdW5kZWZpbmVkKSB7XG5cdFx0XHRlbnVtZXJhYmxlID0gdHJ1ZTtcblx0XHR9XG5cblx0XHR2YXIgZGVmaW5pdGlvbiA9IHsgY29uZmlndXJhYmxlOiBjb25maWd1cmFibGUsIGVudW1lcmFibGU6IGVudW1lcmFibGUgfTtcblxuXHRcdGlmICh0aGlzLmlzRnVuY3Rpb24oZ2V0KSkge1xuXHRcdFx0ZGVmaW5pdGlvbi5nZXQgPSBnZXQ7XG5cdFx0fSBlbHNlIHtcblx0XHRcdGRlZmluaXRpb24udmFsdWUgPSBnZXQ7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIGRlZmluaXRpb247XG5cdH0sXG5cdGRlZmluZVByb3BlcnRpZXM6IGZ1bmN0aW9uIChvYmosIHByb3BlcnRpZXMpIHtcblx0XHRmb3IgKHZhciBwcm9wZXJ0eSBpbiBwcm9wZXJ0aWVzKSB7XG5cdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkob2JqLCBwcm9wZXJ0eSwgcHJvcGVydGllc1twcm9wZXJ0eV0pO1xuXHRcdH1cblx0fSxcblx0ZGVmaW5lRXZlbnRzOiBmdW5jdGlvbiAob2JqLCBldmVudHNOYW1lcykge1xuXHRcdG9iai5vbiA9IG9iai5vbiB8fCB7fTtcblxuXHRcdGV2ZW50c05hbWVzLmZvckVhY2goZnVuY3Rpb24gKGVsZW1lbnQpIHtcblx0XHRcdG9iai5vbltlbGVtZW50XSA9IG5ldyBTaWduYWwoKTtcblx0XHR9KTtcblx0fSxcblx0ZXZhbHVhdGVEZWxlZ2F0ZTogZnVuY3Rpb24gKHNlbmRlciwgZGVsZWdhdGUsIGFyZ3MpIHtcblx0XHRyZXR1cm4gdGhpcy5pc0Z1bmN0aW9uKGRlbGVnYXRlKVxuXHRcdFx0PyBkZWxlZ2F0ZS5hcHBseShcblx0XHRcdFx0bnVsbCwgW3NlbmRlcl0uY29uY2F0KEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMikpXG5cdFx0XHQpXG5cdFx0XHQ6IGRlbGVnYXRlO1xuXHR9LFxuXHRlbnVtVG9TdHJpbmc6IGZ1bmN0aW9uIChlbnVtVHlwZSwgZW51bVZhbHVlKSB7XG5cdFx0Zm9yICh2YXIga2V5IGluIGVudW1UeXBlKSB7XG5cdFx0XHRpZiAoZW51bVR5cGVba2V5XSA9PT0gZW51bVZhbHVlKSB7XG5cdFx0XHRcdHJldHVybiBrZXk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0cmV0dXJuICc8dW5rbm93bj4nO1xuXHR9XG59O1xuIiwiLyoqXG4gKiBAYXV0aG9yIGx1aXpzc2JcbiAqL1xubW9kdWxlLmV4cG9ydHMgPSB7XG5cdHN0cmluZzogZnVuY3Rpb24gKGxlbmd0aCkge1xuXHRcdGlmICghbGVuZ3RoKSB7XG5cdFx0XHRsZW5ndGggPSAxNjtcblx0XHR9XG5cblx0XHR2YXIgYWxwaGFiZXQgPVxuXHRcdFx0J2FiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6JyArICdBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZJyArXG5cdFx0XHQnWjAxMjM0NTY3ODknO1xuXG5cdFx0cmV0dXJuIEFycmF5KGxlbmd0aClcblx0XHRcdC5qb2luKClcblx0XHRcdC5zcGxpdCgnLCcpXG5cdFx0XHQubWFwKFxuXHRcdFx0XHRmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdFx0cmV0dXJuIGFscGhhYmV0LmNoYXJBdChcblx0XHRcdFx0XHRcdE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIGFscGhhYmV0Lmxlbmd0aClcblx0XHRcdFx0XHQpO1xuXHRcdFx0XHR9XG5cdFx0XHQpXG5cdFx0XHQuam9pbignJyk7XG5cdH1cbn07XG4iLCIvKipcbiAqIFRha2VuIGZyb20gVUlWQSdzIChVbml0eSBJbmRpZSBWUlBOIEFkYXB0ZXIpIHNvdXJjZSBjb2RlLlxuICogaHR0cDovL3dlYi5jcy53cGkuZWR1L35nb2dvL2hpdmUvVUlWQS9cbiAqIFxuICogQGF1dGhvciBsdWl6c3NiXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKHhUb1JpZ2h0LCB5QmFja3dhcmRzLCB6VXB3YXJkcykge1xuXHRjb25zdCBncmF2cyA9IHtcblx0XHR4OiB4VG9SaWdodCxcblx0XHR5OiB5QmFja3dhcmRzLFxuXHRcdHo6IHpVcHdhcmRzXG5cdH07XG5cblx0dmFyIHJvbGwsIHBpdGNoO1xuXG5cdC8vIENvbXB1dGUgcm9sbCBhbmdsZVxuXHRjb25zdCBzcXJ0WTJaMiA9IE1hdGguc3FydChNYXRoLnBvdyhncmF2cy55LCAyKSArIE1hdGgucG93KGdyYXZzLnosIDIpKTtcblxuXHQvLyBQcmV2ZW50IGRpdmlkaW5nIGJ5IHplcm9cblx0aWYgKHNxcnRZMloyID09PSAwKSB7XG5cdFx0cm9sbCA9IDkwO1xuXHRcdGlmIChncmF2cy54ID49IDApIHtcblx0XHRcdHJvbGwgKj0gLTE7XG5cdFx0fVxuXHR9IGVsc2Uge1xuXHRcdC8vIFRoZSBtYXRoIHRoYXQgd29ya3MsIGNvbXB1dGUgcm9sbCBhbmdsZVxuXHRcdHJvbGwgPSBNYXRoLmF0YW4oZ3JhdnMueCAvIHNxcnRZMloyKTtcblx0fVxuXG5cdC8vIENvbXB1dGUgcGl0Y2ggYW5nbGVcblx0Y29uc3Qgc3FydFgyWjIgPSBNYXRoLnNxcnQoTWF0aC5wb3coZ3JhdnMueCwgMikgKyBNYXRoLnBvdyhncmF2cy56LCAyKSk7XG5cblx0Ly8gUHJldmVudCBkaXZpZGluZyBieSB6ZXJvXG5cdGlmIChzcXJ0WDJaMiA9PT0gMCkge1xuXHRcdHBpdGNoID0gOTA7XG5cdFx0aWYgKGdyYXZzLnkgPj0gMCkge1xuXHRcdFx0cGl0Y2ggKj0gLTE7XG5cdFx0fVxuXHR9IGVsc2Uge1xuXHRcdC8vIFRoZSBtYXRoIHRoYXQgd29ya3MsIGNvbXB1dGUgcGl0Y2ggYW5nbGVcblx0XHRwaXRjaCA9IC1NYXRoLmF0YW4oZ3JhdnMueSAvIHNxcnRYMloyKTtcblx0fVxuXG5cdHJldHVybiB7XG5cdFx0cm9sbDogcm9sbCxcblx0XHRwaXRjaDogcGl0Y2hcblx0fTtcbn07XG4iLCIvKipcbiAqIEBhdXRob3IgbHVpenNzYlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChyYWQpIHtcblx0cmV0dXJuIHJhZCAqIDE4MCAvIE1hdGguUEk7XG59O1xuIl19
